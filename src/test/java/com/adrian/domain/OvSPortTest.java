package com.adrian.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adrian.web.rest.TestUtil;

public class OvSPortTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OvSPort.class);
        OvSPort ovSPort1 = new OvSPort();
        ovSPort1.setId(1L);
        OvSPort ovSPort2 = new OvSPort();
        ovSPort2.setId(ovSPort1.getId());
        assertThat(ovSPort1).isEqualTo(ovSPort2);
        ovSPort2.setId(2L);
        assertThat(ovSPort1).isNotEqualTo(ovSPort2);
        ovSPort1.setId(null);
        assertThat(ovSPort1).isNotEqualTo(ovSPort2);
    }
}
