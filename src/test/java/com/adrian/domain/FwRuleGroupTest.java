package com.adrian.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adrian.web.rest.TestUtil;

public class FwRuleGroupTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FwRuleGroup.class);
        FwRuleGroup fwRuleGroup1 = new FwRuleGroup();
        fwRuleGroup1.setId(1L);
        FwRuleGroup fwRuleGroup2 = new FwRuleGroup();
        fwRuleGroup2.setId(fwRuleGroup1.getId());
        assertThat(fwRuleGroup1).isEqualTo(fwRuleGroup2);
        fwRuleGroup2.setId(2L);
        assertThat(fwRuleGroup1).isNotEqualTo(fwRuleGroup2);
        fwRuleGroup1.setId(null);
        assertThat(fwRuleGroup1).isNotEqualTo(fwRuleGroup2);
    }
}
