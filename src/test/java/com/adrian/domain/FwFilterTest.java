package com.adrian.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adrian.web.rest.TestUtil;

public class FwFilterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FwFilter.class);
        FwFilter fwFilter1 = new FwFilter();
        fwFilter1.setId(1L);
        FwFilter fwFilter2 = new FwFilter();
        fwFilter2.setId(fwFilter1.getId());
        assertThat(fwFilter1).isEqualTo(fwFilter2);
        fwFilter2.setId(2L);
        assertThat(fwFilter1).isNotEqualTo(fwFilter2);
        fwFilter1.setId(null);
        assertThat(fwFilter1).isNotEqualTo(fwFilter2);
    }
}
