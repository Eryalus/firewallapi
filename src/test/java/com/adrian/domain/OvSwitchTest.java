package com.adrian.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adrian.web.rest.TestUtil;

public class OvSwitchTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OvSwitch.class);
        OvSwitch ovSwitch1 = new OvSwitch();
        ovSwitch1.setId(1L);
        OvSwitch ovSwitch2 = new OvSwitch();
        ovSwitch2.setId(ovSwitch1.getId());
        assertThat(ovSwitch1).isEqualTo(ovSwitch2);
        ovSwitch2.setId(2L);
        assertThat(ovSwitch1).isNotEqualTo(ovSwitch2);
        ovSwitch1.setId(null);
        assertThat(ovSwitch1).isNotEqualTo(ovSwitch2);
    }
}
