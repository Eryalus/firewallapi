package com.adrian.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adrian.web.rest.TestUtil;

public class FwNetworkTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FwNetwork.class);
        FwNetwork fwNetwork1 = new FwNetwork();
        fwNetwork1.setId(1L);
        FwNetwork fwNetwork2 = new FwNetwork();
        fwNetwork2.setId(fwNetwork1.getId());
        assertThat(fwNetwork1).isEqualTo(fwNetwork2);
        fwNetwork2.setId(2L);
        assertThat(fwNetwork1).isNotEqualTo(fwNetwork2);
        fwNetwork1.setId(null);
        assertThat(fwNetwork1).isNotEqualTo(fwNetwork2);
    }
}
