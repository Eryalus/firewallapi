package com.adrian.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adrian.web.rest.TestUtil;

public class FwRuleTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FwRule.class);
        FwRule fwRule1 = new FwRule();
        fwRule1.setId(1L);
        FwRule fwRule2 = new FwRule();
        fwRule2.setId(fwRule1.getId());
        assertThat(fwRule1).isEqualTo(fwRule2);
        fwRule2.setId(2L);
        assertThat(fwRule1).isNotEqualTo(fwRule2);
        fwRule1.setId(null);
        assertThat(fwRule1).isNotEqualTo(fwRule2);
    }
}
