package com.adrian.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adrian.web.rest.TestUtil;

public class OnosControllerTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OnosController.class);
        OnosController onosController1 = new OnosController();
        onosController1.setId(1L);
        OnosController onosController2 = new OnosController();
        onosController2.setId(onosController1.getId());
        assertThat(onosController1).isEqualTo(onosController2);
        onosController2.setId(2L);
        assertThat(onosController1).isNotEqualTo(onosController2);
        onosController1.setId(null);
        assertThat(onosController1).isNotEqualTo(onosController2);
    }
}
