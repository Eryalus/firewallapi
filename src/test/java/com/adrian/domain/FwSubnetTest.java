package com.adrian.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.adrian.web.rest.TestUtil;

public class FwSubnetTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FwSubnet.class);
        FwSubnet fwSubnet1 = new FwSubnet();
        fwSubnet1.setId(1L);
        FwSubnet fwSubnet2 = new FwSubnet();
        fwSubnet2.setId(fwSubnet1.getId());
        assertThat(fwSubnet1).isEqualTo(fwSubnet2);
        fwSubnet2.setId(2L);
        assertThat(fwSubnet1).isNotEqualTo(fwSubnet2);
        fwSubnet1.setId(null);
        assertThat(fwSubnet1).isNotEqualTo(fwSubnet2);
    }
}
