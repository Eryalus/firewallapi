package com.adrian.web.rest;

import com.adrian.FirewallapiApp;
import com.adrian.domain.FwNetwork;
import com.adrian.repository.FwNetworkRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FwNetworkResource} REST controller.
 */
@SpringBootTest(classes = FirewallapiApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FwNetworkResourceIT {

    private static final String DEFAULT_HUMAN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_HUMAN_NAME = "BBBBBBBBBB";

    @Autowired
    private FwNetworkRepository fwNetworkRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFwNetworkMockMvc;

    private FwNetwork fwNetwork;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwNetwork createEntity(EntityManager em) {
        FwNetwork fwNetwork = new FwNetwork()
            .humanName(DEFAULT_HUMAN_NAME);
        return fwNetwork;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwNetwork createUpdatedEntity(EntityManager em) {
        FwNetwork fwNetwork = new FwNetwork()
            .humanName(UPDATED_HUMAN_NAME);
        return fwNetwork;
    }

    @BeforeEach
    public void initTest() {
        fwNetwork = createEntity(em);
    }

    @Test
    @Transactional
    public void createFwNetwork() throws Exception {
        int databaseSizeBeforeCreate = fwNetworkRepository.findAll().size();
        // Create the FwNetwork
        restFwNetworkMockMvc.perform(post("/api/fw-networks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwNetwork)))
            .andExpect(status().isCreated());

        // Validate the FwNetwork in the database
        List<FwNetwork> fwNetworkList = fwNetworkRepository.findAll();
        assertThat(fwNetworkList).hasSize(databaseSizeBeforeCreate + 1);
        FwNetwork testFwNetwork = fwNetworkList.get(fwNetworkList.size() - 1);
        assertThat(testFwNetwork.getHumanName()).isEqualTo(DEFAULT_HUMAN_NAME);
    }

    @Test
    @Transactional
    public void createFwNetworkWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fwNetworkRepository.findAll().size();

        // Create the FwNetwork with an existing ID
        fwNetwork.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFwNetworkMockMvc.perform(post("/api/fw-networks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwNetwork)))
            .andExpect(status().isBadRequest());

        // Validate the FwNetwork in the database
        List<FwNetwork> fwNetworkList = fwNetworkRepository.findAll();
        assertThat(fwNetworkList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFwNetworks() throws Exception {
        // Initialize the database
        fwNetworkRepository.saveAndFlush(fwNetwork);

        // Get all the fwNetworkList
        restFwNetworkMockMvc.perform(get("/api/fw-networks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fwNetwork.getId().intValue())))
            .andExpect(jsonPath("$.[*].humanName").value(hasItem(DEFAULT_HUMAN_NAME)));
    }
    
    @Test
    @Transactional
    public void getFwNetwork() throws Exception {
        // Initialize the database
        fwNetworkRepository.saveAndFlush(fwNetwork);

        // Get the fwNetwork
        restFwNetworkMockMvc.perform(get("/api/fw-networks/{id}", fwNetwork.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fwNetwork.getId().intValue()))
            .andExpect(jsonPath("$.humanName").value(DEFAULT_HUMAN_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingFwNetwork() throws Exception {
        // Get the fwNetwork
        restFwNetworkMockMvc.perform(get("/api/fw-networks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFwNetwork() throws Exception {
        // Initialize the database
        fwNetworkRepository.saveAndFlush(fwNetwork);

        int databaseSizeBeforeUpdate = fwNetworkRepository.findAll().size();

        // Update the fwNetwork
        FwNetwork updatedFwNetwork = fwNetworkRepository.findById(fwNetwork.getId()).get();
        // Disconnect from session so that the updates on updatedFwNetwork are not directly saved in db
        em.detach(updatedFwNetwork);
        updatedFwNetwork
            .humanName(UPDATED_HUMAN_NAME);

        restFwNetworkMockMvc.perform(put("/api/fw-networks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFwNetwork)))
            .andExpect(status().isOk());

        // Validate the FwNetwork in the database
        List<FwNetwork> fwNetworkList = fwNetworkRepository.findAll();
        assertThat(fwNetworkList).hasSize(databaseSizeBeforeUpdate);
        FwNetwork testFwNetwork = fwNetworkList.get(fwNetworkList.size() - 1);
        assertThat(testFwNetwork.getHumanName()).isEqualTo(UPDATED_HUMAN_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingFwNetwork() throws Exception {
        int databaseSizeBeforeUpdate = fwNetworkRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFwNetworkMockMvc.perform(put("/api/fw-networks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwNetwork)))
            .andExpect(status().isBadRequest());

        // Validate the FwNetwork in the database
        List<FwNetwork> fwNetworkList = fwNetworkRepository.findAll();
        assertThat(fwNetworkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFwNetwork() throws Exception {
        // Initialize the database
        fwNetworkRepository.saveAndFlush(fwNetwork);

        int databaseSizeBeforeDelete = fwNetworkRepository.findAll().size();

        // Delete the fwNetwork
        restFwNetworkMockMvc.perform(delete("/api/fw-networks/{id}", fwNetwork.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FwNetwork> fwNetworkList = fwNetworkRepository.findAll();
        assertThat(fwNetworkList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
