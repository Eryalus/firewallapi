package com.adrian.web.rest;

import com.adrian.FirewallapiApp;
import com.adrian.domain.FwFilter;
import com.adrian.repository.FwFilterRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FwFilterResource} REST controller.
 */
@SpringBootTest(classes = FirewallapiApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FwFilterResourceIT {

    private static final String DEFAULT_HUMAN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_HUMAN_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROTOCOL = "AAAAAAAAAA";
    private static final String UPDATED_PROTOCOL = "BBBBBBBBBB";

    private static final String DEFAULT_PROTOCOL_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_PROTOCOL_VALUE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ORIGIN = false;
    private static final Boolean UPDATED_ORIGIN = true;

    @Autowired
    private FwFilterRepository fwFilterRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFwFilterMockMvc;

    private FwFilter fwFilter;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwFilter createEntity(EntityManager em) {
        FwFilter fwFilter = new FwFilter()
            .humanName(DEFAULT_HUMAN_NAME)
            .protocol(DEFAULT_PROTOCOL)
            .protocolValue(DEFAULT_PROTOCOL_VALUE)
            .origin(DEFAULT_ORIGIN);
        return fwFilter;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwFilter createUpdatedEntity(EntityManager em) {
        FwFilter fwFilter = new FwFilter()
            .humanName(UPDATED_HUMAN_NAME)
            .protocol(UPDATED_PROTOCOL)
            .protocolValue(UPDATED_PROTOCOL_VALUE)
            .origin(UPDATED_ORIGIN);
        return fwFilter;
    }

    @BeforeEach
    public void initTest() {
        fwFilter = createEntity(em);
    }

    @Test
    @Transactional
    public void createFwFilter() throws Exception {
        int databaseSizeBeforeCreate = fwFilterRepository.findAll().size();
        // Create the FwFilter
        restFwFilterMockMvc.perform(post("/api/fw-filters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwFilter)))
            .andExpect(status().isCreated());

        // Validate the FwFilter in the database
        List<FwFilter> fwFilterList = fwFilterRepository.findAll();
        assertThat(fwFilterList).hasSize(databaseSizeBeforeCreate + 1);
        FwFilter testFwFilter = fwFilterList.get(fwFilterList.size() - 1);
        assertThat(testFwFilter.getHumanName()).isEqualTo(DEFAULT_HUMAN_NAME);
        assertThat(testFwFilter.getProtocol()).isEqualTo(DEFAULT_PROTOCOL);
        assertThat(testFwFilter.getProtocolValue()).isEqualTo(DEFAULT_PROTOCOL_VALUE);
        assertThat(testFwFilter.isOrigin()).isEqualTo(DEFAULT_ORIGIN);
    }

    @Test
    @Transactional
    public void createFwFilterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fwFilterRepository.findAll().size();

        // Create the FwFilter with an existing ID
        fwFilter.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFwFilterMockMvc.perform(post("/api/fw-filters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwFilter)))
            .andExpect(status().isBadRequest());

        // Validate the FwFilter in the database
        List<FwFilter> fwFilterList = fwFilterRepository.findAll();
        assertThat(fwFilterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkProtocolIsRequired() throws Exception {
        int databaseSizeBeforeTest = fwFilterRepository.findAll().size();
        // set the field null
        fwFilter.setProtocol(null);

        // Create the FwFilter, which fails.


        restFwFilterMockMvc.perform(post("/api/fw-filters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwFilter)))
            .andExpect(status().isBadRequest());

        List<FwFilter> fwFilterList = fwFilterRepository.findAll();
        assertThat(fwFilterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOriginIsRequired() throws Exception {
        int databaseSizeBeforeTest = fwFilterRepository.findAll().size();
        // set the field null
        fwFilter.setOrigin(null);

        // Create the FwFilter, which fails.


        restFwFilterMockMvc.perform(post("/api/fw-filters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwFilter)))
            .andExpect(status().isBadRequest());

        List<FwFilter> fwFilterList = fwFilterRepository.findAll();
        assertThat(fwFilterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFwFilters() throws Exception {
        // Initialize the database
        fwFilterRepository.saveAndFlush(fwFilter);

        // Get all the fwFilterList
        restFwFilterMockMvc.perform(get("/api/fw-filters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fwFilter.getId().intValue())))
            .andExpect(jsonPath("$.[*].humanName").value(hasItem(DEFAULT_HUMAN_NAME)))
            .andExpect(jsonPath("$.[*].protocol").value(hasItem(DEFAULT_PROTOCOL)))
            .andExpect(jsonPath("$.[*].protocolValue").value(hasItem(DEFAULT_PROTOCOL_VALUE)))
            .andExpect(jsonPath("$.[*].origin").value(hasItem(DEFAULT_ORIGIN.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getFwFilter() throws Exception {
        // Initialize the database
        fwFilterRepository.saveAndFlush(fwFilter);

        // Get the fwFilter
        restFwFilterMockMvc.perform(get("/api/fw-filters/{id}", fwFilter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fwFilter.getId().intValue()))
            .andExpect(jsonPath("$.humanName").value(DEFAULT_HUMAN_NAME))
            .andExpect(jsonPath("$.protocol").value(DEFAULT_PROTOCOL))
            .andExpect(jsonPath("$.protocolValue").value(DEFAULT_PROTOCOL_VALUE))
            .andExpect(jsonPath("$.origin").value(DEFAULT_ORIGIN.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFwFilter() throws Exception {
        // Get the fwFilter
        restFwFilterMockMvc.perform(get("/api/fw-filters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFwFilter() throws Exception {
        // Initialize the database
        fwFilterRepository.saveAndFlush(fwFilter);

        int databaseSizeBeforeUpdate = fwFilterRepository.findAll().size();

        // Update the fwFilter
        FwFilter updatedFwFilter = fwFilterRepository.findById(fwFilter.getId()).get();
        // Disconnect from session so that the updates on updatedFwFilter are not directly saved in db
        em.detach(updatedFwFilter);
        updatedFwFilter
            .humanName(UPDATED_HUMAN_NAME)
            .protocol(UPDATED_PROTOCOL)
            .protocolValue(UPDATED_PROTOCOL_VALUE)
            .origin(UPDATED_ORIGIN);

        restFwFilterMockMvc.perform(put("/api/fw-filters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFwFilter)))
            .andExpect(status().isOk());

        // Validate the FwFilter in the database
        List<FwFilter> fwFilterList = fwFilterRepository.findAll();
        assertThat(fwFilterList).hasSize(databaseSizeBeforeUpdate);
        FwFilter testFwFilter = fwFilterList.get(fwFilterList.size() - 1);
        assertThat(testFwFilter.getHumanName()).isEqualTo(UPDATED_HUMAN_NAME);
        assertThat(testFwFilter.getProtocol()).isEqualTo(UPDATED_PROTOCOL);
        assertThat(testFwFilter.getProtocolValue()).isEqualTo(UPDATED_PROTOCOL_VALUE);
        assertThat(testFwFilter.isOrigin()).isEqualTo(UPDATED_ORIGIN);
    }

    @Test
    @Transactional
    public void updateNonExistingFwFilter() throws Exception {
        int databaseSizeBeforeUpdate = fwFilterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFwFilterMockMvc.perform(put("/api/fw-filters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwFilter)))
            .andExpect(status().isBadRequest());

        // Validate the FwFilter in the database
        List<FwFilter> fwFilterList = fwFilterRepository.findAll();
        assertThat(fwFilterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFwFilter() throws Exception {
        // Initialize the database
        fwFilterRepository.saveAndFlush(fwFilter);

        int databaseSizeBeforeDelete = fwFilterRepository.findAll().size();

        // Delete the fwFilter
        restFwFilterMockMvc.perform(delete("/api/fw-filters/{id}", fwFilter.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FwFilter> fwFilterList = fwFilterRepository.findAll();
        assertThat(fwFilterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
