package com.adrian.web.rest;

import com.adrian.FirewallapiApp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the OnosActionsResource REST controller.
 *
 * @see OnosActionsResource
 */
@SpringBootTest(classes = FirewallapiApp.class)
public class OnosActionsResourceIT {

    private MockMvc restMockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        OnosActionsResource onosActionsResource = new OnosActionsResource();
        restMockMvc = MockMvcBuilders
            .standaloneSetup(onosActionsResource)
            .build();
    }

    /**
     * Test reloadRules
     */
    @Test
    public void testReloadRules() throws Exception {
        restMockMvc.perform(post("/api/onos-actions/reload-rules"))
            .andExpect(status().isOk());
    }
}
