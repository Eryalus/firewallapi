package com.adrian.web.rest;

import com.adrian.FirewallapiApp;
import com.adrian.domain.OvSPort;
import com.adrian.repository.OvSPortRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OvSPortResource} REST controller.
 */
@SpringBootTest(classes = FirewallapiApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class OvSPortResourceIT {

    private static final String DEFAULT_HUMAN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_HUMAN_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ONOS_PORT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ONOS_PORT_NAME = "BBBBBBBBBB";

    @Autowired
    private OvSPortRepository ovSPortRepository;

    @Mock
    private OvSPortRepository ovSPortRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOvSPortMockMvc;

    private OvSPort ovSPort;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OvSPort createEntity(EntityManager em) {
        OvSPort ovSPort = new OvSPort()
            .humanName(DEFAULT_HUMAN_NAME)
            .onosPortName(DEFAULT_ONOS_PORT_NAME);
        return ovSPort;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OvSPort createUpdatedEntity(EntityManager em) {
        OvSPort ovSPort = new OvSPort()
            .humanName(UPDATED_HUMAN_NAME)
            .onosPortName(UPDATED_ONOS_PORT_NAME);
        return ovSPort;
    }

    @BeforeEach
    public void initTest() {
        ovSPort = createEntity(em);
    }

    @Test
    @Transactional
    public void createOvSPort() throws Exception {
        int databaseSizeBeforeCreate = ovSPortRepository.findAll().size();
        // Create the OvSPort
        restOvSPortMockMvc.perform(post("/api/ov-s-ports")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ovSPort)))
            .andExpect(status().isCreated());

        // Validate the OvSPort in the database
        List<OvSPort> ovSPortList = ovSPortRepository.findAll();
        assertThat(ovSPortList).hasSize(databaseSizeBeforeCreate + 1);
        OvSPort testOvSPort = ovSPortList.get(ovSPortList.size() - 1);
        assertThat(testOvSPort.getHumanName()).isEqualTo(DEFAULT_HUMAN_NAME);
        assertThat(testOvSPort.getOnosPortName()).isEqualTo(DEFAULT_ONOS_PORT_NAME);
    }

    @Test
    @Transactional
    public void createOvSPortWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ovSPortRepository.findAll().size();

        // Create the OvSPort with an existing ID
        ovSPort.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOvSPortMockMvc.perform(post("/api/ov-s-ports")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ovSPort)))
            .andExpect(status().isBadRequest());

        // Validate the OvSPort in the database
        List<OvSPort> ovSPortList = ovSPortRepository.findAll();
        assertThat(ovSPortList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOvSPorts() throws Exception {
        // Initialize the database
        ovSPortRepository.saveAndFlush(ovSPort);

        // Get all the ovSPortList
        restOvSPortMockMvc.perform(get("/api/ov-s-ports?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ovSPort.getId().intValue())))
            .andExpect(jsonPath("$.[*].humanName").value(hasItem(DEFAULT_HUMAN_NAME)))
            .andExpect(jsonPath("$.[*].onosPortName").value(hasItem(DEFAULT_ONOS_PORT_NAME)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllOvSPortsWithEagerRelationshipsIsEnabled() throws Exception {
        when(ovSPortRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restOvSPortMockMvc.perform(get("/api/ov-s-ports?eagerload=true"))
            .andExpect(status().isOk());

        verify(ovSPortRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllOvSPortsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(ovSPortRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restOvSPortMockMvc.perform(get("/api/ov-s-ports?eagerload=true"))
            .andExpect(status().isOk());

        verify(ovSPortRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getOvSPort() throws Exception {
        // Initialize the database
        ovSPortRepository.saveAndFlush(ovSPort);

        // Get the ovSPort
        restOvSPortMockMvc.perform(get("/api/ov-s-ports/{id}", ovSPort.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ovSPort.getId().intValue()))
            .andExpect(jsonPath("$.humanName").value(DEFAULT_HUMAN_NAME))
            .andExpect(jsonPath("$.onosPortName").value(DEFAULT_ONOS_PORT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingOvSPort() throws Exception {
        // Get the ovSPort
        restOvSPortMockMvc.perform(get("/api/ov-s-ports/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOvSPort() throws Exception {
        // Initialize the database
        ovSPortRepository.saveAndFlush(ovSPort);

        int databaseSizeBeforeUpdate = ovSPortRepository.findAll().size();

        // Update the ovSPort
        OvSPort updatedOvSPort = ovSPortRepository.findById(ovSPort.getId()).get();
        // Disconnect from session so that the updates on updatedOvSPort are not directly saved in db
        em.detach(updatedOvSPort);
        updatedOvSPort
            .humanName(UPDATED_HUMAN_NAME)
            .onosPortName(UPDATED_ONOS_PORT_NAME);

        restOvSPortMockMvc.perform(put("/api/ov-s-ports")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOvSPort)))
            .andExpect(status().isOk());

        // Validate the OvSPort in the database
        List<OvSPort> ovSPortList = ovSPortRepository.findAll();
        assertThat(ovSPortList).hasSize(databaseSizeBeforeUpdate);
        OvSPort testOvSPort = ovSPortList.get(ovSPortList.size() - 1);
        assertThat(testOvSPort.getHumanName()).isEqualTo(UPDATED_HUMAN_NAME);
        assertThat(testOvSPort.getOnosPortName()).isEqualTo(UPDATED_ONOS_PORT_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingOvSPort() throws Exception {
        int databaseSizeBeforeUpdate = ovSPortRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOvSPortMockMvc.perform(put("/api/ov-s-ports")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ovSPort)))
            .andExpect(status().isBadRequest());

        // Validate the OvSPort in the database
        List<OvSPort> ovSPortList = ovSPortRepository.findAll();
        assertThat(ovSPortList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOvSPort() throws Exception {
        // Initialize the database
        ovSPortRepository.saveAndFlush(ovSPort);

        int databaseSizeBeforeDelete = ovSPortRepository.findAll().size();

        // Delete the ovSPort
        restOvSPortMockMvc.perform(delete("/api/ov-s-ports/{id}", ovSPort.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OvSPort> ovSPortList = ovSPortRepository.findAll();
        assertThat(ovSPortList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
