package com.adrian.web.rest;

import com.adrian.FirewallapiApp;
import com.adrian.domain.FwRuleGroup;
import com.adrian.repository.FwRuleGroupRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FwRuleGroupResource} REST controller.
 */
@SpringBootTest(classes = FirewallapiApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FwRuleGroupResourceIT {

    private static final String DEFAULT_HUMAN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_HUMAN_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_PRIORITY = 1;
    private static final Integer UPDATED_PRIORITY = 2;

    @Autowired
    private FwRuleGroupRepository fwRuleGroupRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFwRuleGroupMockMvc;

    private FwRuleGroup fwRuleGroup;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwRuleGroup createEntity(EntityManager em) {
        FwRuleGroup fwRuleGroup = new FwRuleGroup()
            .humanName(DEFAULT_HUMAN_NAME)
            .priority(DEFAULT_PRIORITY);
        return fwRuleGroup;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwRuleGroup createUpdatedEntity(EntityManager em) {
        FwRuleGroup fwRuleGroup = new FwRuleGroup()
            .humanName(UPDATED_HUMAN_NAME)
            .priority(UPDATED_PRIORITY);
        return fwRuleGroup;
    }

    @BeforeEach
    public void initTest() {
        fwRuleGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createFwRuleGroup() throws Exception {
        int databaseSizeBeforeCreate = fwRuleGroupRepository.findAll().size();
        // Create the FwRuleGroup
        restFwRuleGroupMockMvc.perform(post("/api/fw-rule-groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwRuleGroup)))
            .andExpect(status().isCreated());

        // Validate the FwRuleGroup in the database
        List<FwRuleGroup> fwRuleGroupList = fwRuleGroupRepository.findAll();
        assertThat(fwRuleGroupList).hasSize(databaseSizeBeforeCreate + 1);
        FwRuleGroup testFwRuleGroup = fwRuleGroupList.get(fwRuleGroupList.size() - 1);
        assertThat(testFwRuleGroup.getHumanName()).isEqualTo(DEFAULT_HUMAN_NAME);
        assertThat(testFwRuleGroup.getPriority()).isEqualTo(DEFAULT_PRIORITY);
    }

    @Test
    @Transactional
    public void createFwRuleGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fwRuleGroupRepository.findAll().size();

        // Create the FwRuleGroup with an existing ID
        fwRuleGroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFwRuleGroupMockMvc.perform(post("/api/fw-rule-groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwRuleGroup)))
            .andExpect(status().isBadRequest());

        // Validate the FwRuleGroup in the database
        List<FwRuleGroup> fwRuleGroupList = fwRuleGroupRepository.findAll();
        assertThat(fwRuleGroupList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkPriorityIsRequired() throws Exception {
        int databaseSizeBeforeTest = fwRuleGroupRepository.findAll().size();
        // set the field null
        fwRuleGroup.setPriority(null);

        // Create the FwRuleGroup, which fails.


        restFwRuleGroupMockMvc.perform(post("/api/fw-rule-groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwRuleGroup)))
            .andExpect(status().isBadRequest());

        List<FwRuleGroup> fwRuleGroupList = fwRuleGroupRepository.findAll();
        assertThat(fwRuleGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFwRuleGroups() throws Exception {
        // Initialize the database
        fwRuleGroupRepository.saveAndFlush(fwRuleGroup);

        // Get all the fwRuleGroupList
        restFwRuleGroupMockMvc.perform(get("/api/fw-rule-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fwRuleGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].humanName").value(hasItem(DEFAULT_HUMAN_NAME)))
            .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY)));
    }
    
    @Test
    @Transactional
    public void getFwRuleGroup() throws Exception {
        // Initialize the database
        fwRuleGroupRepository.saveAndFlush(fwRuleGroup);

        // Get the fwRuleGroup
        restFwRuleGroupMockMvc.perform(get("/api/fw-rule-groups/{id}", fwRuleGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fwRuleGroup.getId().intValue()))
            .andExpect(jsonPath("$.humanName").value(DEFAULT_HUMAN_NAME))
            .andExpect(jsonPath("$.priority").value(DEFAULT_PRIORITY));
    }
    @Test
    @Transactional
    public void getNonExistingFwRuleGroup() throws Exception {
        // Get the fwRuleGroup
        restFwRuleGroupMockMvc.perform(get("/api/fw-rule-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFwRuleGroup() throws Exception {
        // Initialize the database
        fwRuleGroupRepository.saveAndFlush(fwRuleGroup);

        int databaseSizeBeforeUpdate = fwRuleGroupRepository.findAll().size();

        // Update the fwRuleGroup
        FwRuleGroup updatedFwRuleGroup = fwRuleGroupRepository.findById(fwRuleGroup.getId()).get();
        // Disconnect from session so that the updates on updatedFwRuleGroup are not directly saved in db
        em.detach(updatedFwRuleGroup);
        updatedFwRuleGroup
            .humanName(UPDATED_HUMAN_NAME)
            .priority(UPDATED_PRIORITY);

        restFwRuleGroupMockMvc.perform(put("/api/fw-rule-groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFwRuleGroup)))
            .andExpect(status().isOk());

        // Validate the FwRuleGroup in the database
        List<FwRuleGroup> fwRuleGroupList = fwRuleGroupRepository.findAll();
        assertThat(fwRuleGroupList).hasSize(databaseSizeBeforeUpdate);
        FwRuleGroup testFwRuleGroup = fwRuleGroupList.get(fwRuleGroupList.size() - 1);
        assertThat(testFwRuleGroup.getHumanName()).isEqualTo(UPDATED_HUMAN_NAME);
        assertThat(testFwRuleGroup.getPriority()).isEqualTo(UPDATED_PRIORITY);
    }

    @Test
    @Transactional
    public void updateNonExistingFwRuleGroup() throws Exception {
        int databaseSizeBeforeUpdate = fwRuleGroupRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFwRuleGroupMockMvc.perform(put("/api/fw-rule-groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwRuleGroup)))
            .andExpect(status().isBadRequest());

        // Validate the FwRuleGroup in the database
        List<FwRuleGroup> fwRuleGroupList = fwRuleGroupRepository.findAll();
        assertThat(fwRuleGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFwRuleGroup() throws Exception {
        // Initialize the database
        fwRuleGroupRepository.saveAndFlush(fwRuleGroup);

        int databaseSizeBeforeDelete = fwRuleGroupRepository.findAll().size();

        // Delete the fwRuleGroup
        restFwRuleGroupMockMvc.perform(delete("/api/fw-rule-groups/{id}", fwRuleGroup.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FwRuleGroup> fwRuleGroupList = fwRuleGroupRepository.findAll();
        assertThat(fwRuleGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
