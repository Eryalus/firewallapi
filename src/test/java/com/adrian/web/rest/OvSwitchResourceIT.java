package com.adrian.web.rest;

import com.adrian.FirewallapiApp;
import com.adrian.domain.OvSwitch;
import com.adrian.repository.OvSwitchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OvSwitchResource} REST controller.
 */
@SpringBootTest(classes = FirewallapiApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OvSwitchResourceIT {

    private static final String DEFAULT_HUMAN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_HUMAN_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ONOS_DEVICE_ID = "AAAAAAAAAA";
    private static final String UPDATED_ONOS_DEVICE_ID = "BBBBBBBBBB";

    @Autowired
    private OvSwitchRepository ovSwitchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOvSwitchMockMvc;

    private OvSwitch ovSwitch;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OvSwitch createEntity(EntityManager em) {
        OvSwitch ovSwitch = new OvSwitch()
            .humanName(DEFAULT_HUMAN_NAME)
            .onosDeviceId(DEFAULT_ONOS_DEVICE_ID);
        return ovSwitch;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OvSwitch createUpdatedEntity(EntityManager em) {
        OvSwitch ovSwitch = new OvSwitch()
            .humanName(UPDATED_HUMAN_NAME)
            .onosDeviceId(UPDATED_ONOS_DEVICE_ID);
        return ovSwitch;
    }

    @BeforeEach
    public void initTest() {
        ovSwitch = createEntity(em);
    }

    @Test
    @Transactional
    public void createOvSwitch() throws Exception {
        int databaseSizeBeforeCreate = ovSwitchRepository.findAll().size();
        // Create the OvSwitch
        restOvSwitchMockMvc.perform(post("/api/ov-switches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ovSwitch)))
            .andExpect(status().isCreated());

        // Validate the OvSwitch in the database
        List<OvSwitch> ovSwitchList = ovSwitchRepository.findAll();
        assertThat(ovSwitchList).hasSize(databaseSizeBeforeCreate + 1);
        OvSwitch testOvSwitch = ovSwitchList.get(ovSwitchList.size() - 1);
        assertThat(testOvSwitch.getHumanName()).isEqualTo(DEFAULT_HUMAN_NAME);
        assertThat(testOvSwitch.getOnosDeviceId()).isEqualTo(DEFAULT_ONOS_DEVICE_ID);
    }

    @Test
    @Transactional
    public void createOvSwitchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ovSwitchRepository.findAll().size();

        // Create the OvSwitch with an existing ID
        ovSwitch.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOvSwitchMockMvc.perform(post("/api/ov-switches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ovSwitch)))
            .andExpect(status().isBadRequest());

        // Validate the OvSwitch in the database
        List<OvSwitch> ovSwitchList = ovSwitchRepository.findAll();
        assertThat(ovSwitchList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOvSwitches() throws Exception {
        // Initialize the database
        ovSwitchRepository.saveAndFlush(ovSwitch);

        // Get all the ovSwitchList
        restOvSwitchMockMvc.perform(get("/api/ov-switches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ovSwitch.getId().intValue())))
            .andExpect(jsonPath("$.[*].humanName").value(hasItem(DEFAULT_HUMAN_NAME)))
            .andExpect(jsonPath("$.[*].onosDeviceId").value(hasItem(DEFAULT_ONOS_DEVICE_ID)));
    }
    
    @Test
    @Transactional
    public void getOvSwitch() throws Exception {
        // Initialize the database
        ovSwitchRepository.saveAndFlush(ovSwitch);

        // Get the ovSwitch
        restOvSwitchMockMvc.perform(get("/api/ov-switches/{id}", ovSwitch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ovSwitch.getId().intValue()))
            .andExpect(jsonPath("$.humanName").value(DEFAULT_HUMAN_NAME))
            .andExpect(jsonPath("$.onosDeviceId").value(DEFAULT_ONOS_DEVICE_ID));
    }
    @Test
    @Transactional
    public void getNonExistingOvSwitch() throws Exception {
        // Get the ovSwitch
        restOvSwitchMockMvc.perform(get("/api/ov-switches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOvSwitch() throws Exception {
        // Initialize the database
        ovSwitchRepository.saveAndFlush(ovSwitch);

        int databaseSizeBeforeUpdate = ovSwitchRepository.findAll().size();

        // Update the ovSwitch
        OvSwitch updatedOvSwitch = ovSwitchRepository.findById(ovSwitch.getId()).get();
        // Disconnect from session so that the updates on updatedOvSwitch are not directly saved in db
        em.detach(updatedOvSwitch);
        updatedOvSwitch
            .humanName(UPDATED_HUMAN_NAME)
            .onosDeviceId(UPDATED_ONOS_DEVICE_ID);

        restOvSwitchMockMvc.perform(put("/api/ov-switches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOvSwitch)))
            .andExpect(status().isOk());

        // Validate the OvSwitch in the database
        List<OvSwitch> ovSwitchList = ovSwitchRepository.findAll();
        assertThat(ovSwitchList).hasSize(databaseSizeBeforeUpdate);
        OvSwitch testOvSwitch = ovSwitchList.get(ovSwitchList.size() - 1);
        assertThat(testOvSwitch.getHumanName()).isEqualTo(UPDATED_HUMAN_NAME);
        assertThat(testOvSwitch.getOnosDeviceId()).isEqualTo(UPDATED_ONOS_DEVICE_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingOvSwitch() throws Exception {
        int databaseSizeBeforeUpdate = ovSwitchRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOvSwitchMockMvc.perform(put("/api/ov-switches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ovSwitch)))
            .andExpect(status().isBadRequest());

        // Validate the OvSwitch in the database
        List<OvSwitch> ovSwitchList = ovSwitchRepository.findAll();
        assertThat(ovSwitchList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOvSwitch() throws Exception {
        // Initialize the database
        ovSwitchRepository.saveAndFlush(ovSwitch);

        int databaseSizeBeforeDelete = ovSwitchRepository.findAll().size();

        // Delete the ovSwitch
        restOvSwitchMockMvc.perform(delete("/api/ov-switches/{id}", ovSwitch.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OvSwitch> ovSwitchList = ovSwitchRepository.findAll();
        assertThat(ovSwitchList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
