package com.adrian.web.rest;

import com.adrian.FirewallapiApp;
import com.adrian.domain.FwRule;
import com.adrian.repository.FwRuleRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FwRuleResource} REST controller.
 */
@SpringBootTest(classes = FirewallapiApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FwRuleResourceIT {

    private static final String DEFAULT_HUMAN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_HUMAN_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_PRIORITY = 1;
    private static final Integer UPDATED_PRIORITY = 2;

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    @Autowired
    private FwRuleRepository fwRuleRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFwRuleMockMvc;

    private FwRule fwRule;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwRule createEntity(EntityManager em) {
        FwRule fwRule = new FwRule()
            .humanName(DEFAULT_HUMAN_NAME)
            .priority(DEFAULT_PRIORITY)
            .action(DEFAULT_ACTION);
        return fwRule;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwRule createUpdatedEntity(EntityManager em) {
        FwRule fwRule = new FwRule()
            .humanName(UPDATED_HUMAN_NAME)
            .priority(UPDATED_PRIORITY)
            .action(UPDATED_ACTION);
        return fwRule;
    }

    @BeforeEach
    public void initTest() {
        fwRule = createEntity(em);
    }

    @Test
    @Transactional
    public void createFwRule() throws Exception {
        int databaseSizeBeforeCreate = fwRuleRepository.findAll().size();
        // Create the FwRule
        restFwRuleMockMvc.perform(post("/api/fw-rules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwRule)))
            .andExpect(status().isCreated());

        // Validate the FwRule in the database
        List<FwRule> fwRuleList = fwRuleRepository.findAll();
        assertThat(fwRuleList).hasSize(databaseSizeBeforeCreate + 1);
        FwRule testFwRule = fwRuleList.get(fwRuleList.size() - 1);
        assertThat(testFwRule.getHumanName()).isEqualTo(DEFAULT_HUMAN_NAME);
        assertThat(testFwRule.getPriority()).isEqualTo(DEFAULT_PRIORITY);
        assertThat(testFwRule.getAction()).isEqualTo(DEFAULT_ACTION);
    }

    @Test
    @Transactional
    public void createFwRuleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fwRuleRepository.findAll().size();

        // Create the FwRule with an existing ID
        fwRule.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFwRuleMockMvc.perform(post("/api/fw-rules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwRule)))
            .andExpect(status().isBadRequest());

        // Validate the FwRule in the database
        List<FwRule> fwRuleList = fwRuleRepository.findAll();
        assertThat(fwRuleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkPriorityIsRequired() throws Exception {
        int databaseSizeBeforeTest = fwRuleRepository.findAll().size();
        // set the field null
        fwRule.setPriority(null);

        // Create the FwRule, which fails.


        restFwRuleMockMvc.perform(post("/api/fw-rules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwRule)))
            .andExpect(status().isBadRequest());

        List<FwRule> fwRuleList = fwRuleRepository.findAll();
        assertThat(fwRuleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFwRules() throws Exception {
        // Initialize the database
        fwRuleRepository.saveAndFlush(fwRule);

        // Get all the fwRuleList
        restFwRuleMockMvc.perform(get("/api/fw-rules?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fwRule.getId().intValue())))
            .andExpect(jsonPath("$.[*].humanName").value(hasItem(DEFAULT_HUMAN_NAME)))
            .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY)))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION)));
    }
    
    @Test
    @Transactional
    public void getFwRule() throws Exception {
        // Initialize the database
        fwRuleRepository.saveAndFlush(fwRule);

        // Get the fwRule
        restFwRuleMockMvc.perform(get("/api/fw-rules/{id}", fwRule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fwRule.getId().intValue()))
            .andExpect(jsonPath("$.humanName").value(DEFAULT_HUMAN_NAME))
            .andExpect(jsonPath("$.priority").value(DEFAULT_PRIORITY))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION));
    }
    @Test
    @Transactional
    public void getNonExistingFwRule() throws Exception {
        // Get the fwRule
        restFwRuleMockMvc.perform(get("/api/fw-rules/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFwRule() throws Exception {
        // Initialize the database
        fwRuleRepository.saveAndFlush(fwRule);

        int databaseSizeBeforeUpdate = fwRuleRepository.findAll().size();

        // Update the fwRule
        FwRule updatedFwRule = fwRuleRepository.findById(fwRule.getId()).get();
        // Disconnect from session so that the updates on updatedFwRule are not directly saved in db
        em.detach(updatedFwRule);
        updatedFwRule
            .humanName(UPDATED_HUMAN_NAME)
            .priority(UPDATED_PRIORITY)
            .action(UPDATED_ACTION);

        restFwRuleMockMvc.perform(put("/api/fw-rules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFwRule)))
            .andExpect(status().isOk());

        // Validate the FwRule in the database
        List<FwRule> fwRuleList = fwRuleRepository.findAll();
        assertThat(fwRuleList).hasSize(databaseSizeBeforeUpdate);
        FwRule testFwRule = fwRuleList.get(fwRuleList.size() - 1);
        assertThat(testFwRule.getHumanName()).isEqualTo(UPDATED_HUMAN_NAME);
        assertThat(testFwRule.getPriority()).isEqualTo(UPDATED_PRIORITY);
        assertThat(testFwRule.getAction()).isEqualTo(UPDATED_ACTION);
    }

    @Test
    @Transactional
    public void updateNonExistingFwRule() throws Exception {
        int databaseSizeBeforeUpdate = fwRuleRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFwRuleMockMvc.perform(put("/api/fw-rules")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwRule)))
            .andExpect(status().isBadRequest());

        // Validate the FwRule in the database
        List<FwRule> fwRuleList = fwRuleRepository.findAll();
        assertThat(fwRuleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFwRule() throws Exception {
        // Initialize the database
        fwRuleRepository.saveAndFlush(fwRule);

        int databaseSizeBeforeDelete = fwRuleRepository.findAll().size();

        // Delete the fwRule
        restFwRuleMockMvc.perform(delete("/api/fw-rules/{id}", fwRule.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FwRule> fwRuleList = fwRuleRepository.findAll();
        assertThat(fwRuleList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
