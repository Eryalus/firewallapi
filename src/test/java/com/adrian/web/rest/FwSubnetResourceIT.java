package com.adrian.web.rest;

import com.adrian.FirewallapiApp;
import com.adrian.domain.FwSubnet;
import com.adrian.repository.FwSubnetRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FwSubnetResource} REST controller.
 */
@SpringBootTest(classes = FirewallapiApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FwSubnetResourceIT {

    private static final String DEFAULT_IP_PREFIX = "AAAAAAAAAA";
    private static final String UPDATED_IP_PREFIX = "BBBBBBBBBB";

    private static final String DEFAULT_IP_MASK = "AAAAAAAAAA";
    private static final String UPDATED_IP_MASK = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ALLOW = false;
    private static final Boolean UPDATED_ALLOW = true;

    private static final Integer DEFAULT_PRIORITY = 1;
    private static final Integer UPDATED_PRIORITY = 2;

    @Autowired
    private FwSubnetRepository fwSubnetRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFwSubnetMockMvc;

    private FwSubnet fwSubnet;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwSubnet createEntity(EntityManager em) {
        FwSubnet fwSubnet = new FwSubnet()
            .ipPrefix(DEFAULT_IP_PREFIX)
            .ipMask(DEFAULT_IP_MASK)
            .allow(DEFAULT_ALLOW)
            .priority(DEFAULT_PRIORITY);
        return fwSubnet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FwSubnet createUpdatedEntity(EntityManager em) {
        FwSubnet fwSubnet = new FwSubnet()
            .ipPrefix(UPDATED_IP_PREFIX)
            .ipMask(UPDATED_IP_MASK)
            .allow(UPDATED_ALLOW)
            .priority(UPDATED_PRIORITY);
        return fwSubnet;
    }

    @BeforeEach
    public void initTest() {
        fwSubnet = createEntity(em);
    }

    @Test
    @Transactional
    public void createFwSubnet() throws Exception {
        int databaseSizeBeforeCreate = fwSubnetRepository.findAll().size();
        // Create the FwSubnet
        restFwSubnetMockMvc.perform(post("/api/fw-subnets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwSubnet)))
            .andExpect(status().isCreated());

        // Validate the FwSubnet in the database
        List<FwSubnet> fwSubnetList = fwSubnetRepository.findAll();
        assertThat(fwSubnetList).hasSize(databaseSizeBeforeCreate + 1);
        FwSubnet testFwSubnet = fwSubnetList.get(fwSubnetList.size() - 1);
        assertThat(testFwSubnet.getIpPrefix()).isEqualTo(DEFAULT_IP_PREFIX);
        assertThat(testFwSubnet.getIpMask()).isEqualTo(DEFAULT_IP_MASK);
        assertThat(testFwSubnet.isAllow()).isEqualTo(DEFAULT_ALLOW);
        assertThat(testFwSubnet.getPriority()).isEqualTo(DEFAULT_PRIORITY);
    }

    @Test
    @Transactional
    public void createFwSubnetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fwSubnetRepository.findAll().size();

        // Create the FwSubnet with an existing ID
        fwSubnet.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFwSubnetMockMvc.perform(post("/api/fw-subnets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwSubnet)))
            .andExpect(status().isBadRequest());

        // Validate the FwSubnet in the database
        List<FwSubnet> fwSubnetList = fwSubnetRepository.findAll();
        assertThat(fwSubnetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIpPrefixIsRequired() throws Exception {
        int databaseSizeBeforeTest = fwSubnetRepository.findAll().size();
        // set the field null
        fwSubnet.setIpPrefix(null);

        // Create the FwSubnet, which fails.


        restFwSubnetMockMvc.perform(post("/api/fw-subnets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwSubnet)))
            .andExpect(status().isBadRequest());

        List<FwSubnet> fwSubnetList = fwSubnetRepository.findAll();
        assertThat(fwSubnetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIpMaskIsRequired() throws Exception {
        int databaseSizeBeforeTest = fwSubnetRepository.findAll().size();
        // set the field null
        fwSubnet.setIpMask(null);

        // Create the FwSubnet, which fails.


        restFwSubnetMockMvc.perform(post("/api/fw-subnets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwSubnet)))
            .andExpect(status().isBadRequest());

        List<FwSubnet> fwSubnetList = fwSubnetRepository.findAll();
        assertThat(fwSubnetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAllowIsRequired() throws Exception {
        int databaseSizeBeforeTest = fwSubnetRepository.findAll().size();
        // set the field null
        fwSubnet.setAllow(null);

        // Create the FwSubnet, which fails.


        restFwSubnetMockMvc.perform(post("/api/fw-subnets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwSubnet)))
            .andExpect(status().isBadRequest());

        List<FwSubnet> fwSubnetList = fwSubnetRepository.findAll();
        assertThat(fwSubnetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriorityIsRequired() throws Exception {
        int databaseSizeBeforeTest = fwSubnetRepository.findAll().size();
        // set the field null
        fwSubnet.setPriority(null);

        // Create the FwSubnet, which fails.


        restFwSubnetMockMvc.perform(post("/api/fw-subnets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwSubnet)))
            .andExpect(status().isBadRequest());

        List<FwSubnet> fwSubnetList = fwSubnetRepository.findAll();
        assertThat(fwSubnetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFwSubnets() throws Exception {
        // Initialize the database
        fwSubnetRepository.saveAndFlush(fwSubnet);

        // Get all the fwSubnetList
        restFwSubnetMockMvc.perform(get("/api/fw-subnets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fwSubnet.getId().intValue())))
            .andExpect(jsonPath("$.[*].ipPrefix").value(hasItem(DEFAULT_IP_PREFIX)))
            .andExpect(jsonPath("$.[*].ipMask").value(hasItem(DEFAULT_IP_MASK)))
            .andExpect(jsonPath("$.[*].allow").value(hasItem(DEFAULT_ALLOW.booleanValue())))
            .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY)));
    }
    
    @Test
    @Transactional
    public void getFwSubnet() throws Exception {
        // Initialize the database
        fwSubnetRepository.saveAndFlush(fwSubnet);

        // Get the fwSubnet
        restFwSubnetMockMvc.perform(get("/api/fw-subnets/{id}", fwSubnet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fwSubnet.getId().intValue()))
            .andExpect(jsonPath("$.ipPrefix").value(DEFAULT_IP_PREFIX))
            .andExpect(jsonPath("$.ipMask").value(DEFAULT_IP_MASK))
            .andExpect(jsonPath("$.allow").value(DEFAULT_ALLOW.booleanValue()))
            .andExpect(jsonPath("$.priority").value(DEFAULT_PRIORITY));
    }
    @Test
    @Transactional
    public void getNonExistingFwSubnet() throws Exception {
        // Get the fwSubnet
        restFwSubnetMockMvc.perform(get("/api/fw-subnets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFwSubnet() throws Exception {
        // Initialize the database
        fwSubnetRepository.saveAndFlush(fwSubnet);

        int databaseSizeBeforeUpdate = fwSubnetRepository.findAll().size();

        // Update the fwSubnet
        FwSubnet updatedFwSubnet = fwSubnetRepository.findById(fwSubnet.getId()).get();
        // Disconnect from session so that the updates on updatedFwSubnet are not directly saved in db
        em.detach(updatedFwSubnet);
        updatedFwSubnet
            .ipPrefix(UPDATED_IP_PREFIX)
            .ipMask(UPDATED_IP_MASK)
            .allow(UPDATED_ALLOW)
            .priority(UPDATED_PRIORITY);

        restFwSubnetMockMvc.perform(put("/api/fw-subnets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFwSubnet)))
            .andExpect(status().isOk());

        // Validate the FwSubnet in the database
        List<FwSubnet> fwSubnetList = fwSubnetRepository.findAll();
        assertThat(fwSubnetList).hasSize(databaseSizeBeforeUpdate);
        FwSubnet testFwSubnet = fwSubnetList.get(fwSubnetList.size() - 1);
        assertThat(testFwSubnet.getIpPrefix()).isEqualTo(UPDATED_IP_PREFIX);
        assertThat(testFwSubnet.getIpMask()).isEqualTo(UPDATED_IP_MASK);
        assertThat(testFwSubnet.isAllow()).isEqualTo(UPDATED_ALLOW);
        assertThat(testFwSubnet.getPriority()).isEqualTo(UPDATED_PRIORITY);
    }

    @Test
    @Transactional
    public void updateNonExistingFwSubnet() throws Exception {
        int databaseSizeBeforeUpdate = fwSubnetRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFwSubnetMockMvc.perform(put("/api/fw-subnets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fwSubnet)))
            .andExpect(status().isBadRequest());

        // Validate the FwSubnet in the database
        List<FwSubnet> fwSubnetList = fwSubnetRepository.findAll();
        assertThat(fwSubnetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFwSubnet() throws Exception {
        // Initialize the database
        fwSubnetRepository.saveAndFlush(fwSubnet);

        int databaseSizeBeforeDelete = fwSubnetRepository.findAll().size();

        // Delete the fwSubnet
        restFwSubnetMockMvc.perform(delete("/api/fw-subnets/{id}", fwSubnet.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FwSubnet> fwSubnetList = fwSubnetRepository.findAll();
        assertThat(fwSubnetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
