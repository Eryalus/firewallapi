package com.adrian.web.rest;

import com.adrian.FirewallapiApp;
import com.adrian.domain.OnosController;
import com.adrian.repository.OnosControllerRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OnosControllerResource} REST controller.
 */
@SpringBootTest(classes = FirewallapiApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OnosControllerResourceIT {

    private static final String DEFAULT_HUMAN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_HUMAN_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_IP = "AAAAAAAAAA";
    private static final String UPDATED_IP = "BBBBBBBBBB";

    private static final Integer DEFAULT_PORT = 1;
    private static final Integer UPDATED_PORT = 2;

    private static final String DEFAULT_USER = "AAAAAAAAAA";
    private static final String UPDATED_USER = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    @Autowired
    private OnosControllerRepository onosControllerRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOnosControllerMockMvc;

    private OnosController onosController;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OnosController createEntity(EntityManager em) {
        OnosController onosController = new OnosController()
            .humanName(DEFAULT_HUMAN_NAME)
            .ip(DEFAULT_IP)
            .port(DEFAULT_PORT)
            .user(DEFAULT_USER)
            .password(DEFAULT_PASSWORD);
        return onosController;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OnosController createUpdatedEntity(EntityManager em) {
        OnosController onosController = new OnosController()
            .humanName(UPDATED_HUMAN_NAME)
            .ip(UPDATED_IP)
            .port(UPDATED_PORT)
            .user(UPDATED_USER)
            .password(UPDATED_PASSWORD);
        return onosController;
    }

    @BeforeEach
    public void initTest() {
        onosController = createEntity(em);
    }

    @Test
    @Transactional
    public void createOnosController() throws Exception {
        int databaseSizeBeforeCreate = onosControllerRepository.findAll().size();
        // Create the OnosController
        restOnosControllerMockMvc.perform(post("/api/onos-controllers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onosController)))
            .andExpect(status().isCreated());

        // Validate the OnosController in the database
        List<OnosController> onosControllerList = onosControllerRepository.findAll();
        assertThat(onosControllerList).hasSize(databaseSizeBeforeCreate + 1);
        OnosController testOnosController = onosControllerList.get(onosControllerList.size() - 1);
        assertThat(testOnosController.getHumanName()).isEqualTo(DEFAULT_HUMAN_NAME);
        assertThat(testOnosController.getIp()).isEqualTo(DEFAULT_IP);
        assertThat(testOnosController.getPort()).isEqualTo(DEFAULT_PORT);
        assertThat(testOnosController.getUser()).isEqualTo(DEFAULT_USER);
        assertThat(testOnosController.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void createOnosControllerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = onosControllerRepository.findAll().size();

        // Create the OnosController with an existing ID
        onosController.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOnosControllerMockMvc.perform(post("/api/onos-controllers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onosController)))
            .andExpect(status().isBadRequest());

        // Validate the OnosController in the database
        List<OnosController> onosControllerList = onosControllerRepository.findAll();
        assertThat(onosControllerList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOnosControllers() throws Exception {
        // Initialize the database
        onosControllerRepository.saveAndFlush(onosController);

        // Get all the onosControllerList
        restOnosControllerMockMvc.perform(get("/api/onos-controllers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(onosController.getId().intValue())))
            .andExpect(jsonPath("$.[*].humanName").value(hasItem(DEFAULT_HUMAN_NAME)))
            .andExpect(jsonPath("$.[*].ip").value(hasItem(DEFAULT_IP)))
            .andExpect(jsonPath("$.[*].port").value(hasItem(DEFAULT_PORT)))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)));
    }
    
    @Test
    @Transactional
    public void getOnosController() throws Exception {
        // Initialize the database
        onosControllerRepository.saveAndFlush(onosController);

        // Get the onosController
        restOnosControllerMockMvc.perform(get("/api/onos-controllers/{id}", onosController.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(onosController.getId().intValue()))
            .andExpect(jsonPath("$.humanName").value(DEFAULT_HUMAN_NAME))
            .andExpect(jsonPath("$.ip").value(DEFAULT_IP))
            .andExpect(jsonPath("$.port").value(DEFAULT_PORT))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD));
    }
    @Test
    @Transactional
    public void getNonExistingOnosController() throws Exception {
        // Get the onosController
        restOnosControllerMockMvc.perform(get("/api/onos-controllers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOnosController() throws Exception {
        // Initialize the database
        onosControllerRepository.saveAndFlush(onosController);

        int databaseSizeBeforeUpdate = onosControllerRepository.findAll().size();

        // Update the onosController
        OnosController updatedOnosController = onosControllerRepository.findById(onosController.getId()).get();
        // Disconnect from session so that the updates on updatedOnosController are not directly saved in db
        em.detach(updatedOnosController);
        updatedOnosController
            .humanName(UPDATED_HUMAN_NAME)
            .ip(UPDATED_IP)
            .port(UPDATED_PORT)
            .user(UPDATED_USER)
            .password(UPDATED_PASSWORD);

        restOnosControllerMockMvc.perform(put("/api/onos-controllers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOnosController)))
            .andExpect(status().isOk());

        // Validate the OnosController in the database
        List<OnosController> onosControllerList = onosControllerRepository.findAll();
        assertThat(onosControllerList).hasSize(databaseSizeBeforeUpdate);
        OnosController testOnosController = onosControllerList.get(onosControllerList.size() - 1);
        assertThat(testOnosController.getHumanName()).isEqualTo(UPDATED_HUMAN_NAME);
        assertThat(testOnosController.getIp()).isEqualTo(UPDATED_IP);
        assertThat(testOnosController.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testOnosController.getUser()).isEqualTo(UPDATED_USER);
        assertThat(testOnosController.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void updateNonExistingOnosController() throws Exception {
        int databaseSizeBeforeUpdate = onosControllerRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOnosControllerMockMvc.perform(put("/api/onos-controllers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onosController)))
            .andExpect(status().isBadRequest());

        // Validate the OnosController in the database
        List<OnosController> onosControllerList = onosControllerRepository.findAll();
        assertThat(onosControllerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOnosController() throws Exception {
        // Initialize the database
        onosControllerRepository.saveAndFlush(onosController);

        int databaseSizeBeforeDelete = onosControllerRepository.findAll().size();

        // Delete the onosController
        restOnosControllerMockMvc.perform(delete("/api/onos-controllers/{id}", onosController.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OnosController> onosControllerList = onosControllerRepository.findAll();
        assertThat(onosControllerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
