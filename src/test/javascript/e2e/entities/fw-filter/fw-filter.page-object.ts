import { element, by, ElementFinder } from 'protractor';

export class FwFilterComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-fw-filter div table .btn-danger'));
  title = element.all(by.css('jhi-fw-filter div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FwFilterUpdatePage {
  pageTitle = element(by.id('jhi-fw-filter-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  humanNameInput = element(by.id('field_humanName'));
  protocolInput = element(by.id('field_protocol'));
  protocolValueInput = element(by.id('field_protocolValue'));
  originInput = element(by.id('field_origin'));

  fwRuleSelect = element(by.id('field_fwRule'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setHumanNameInput(humanName: string): Promise<void> {
    await this.humanNameInput.sendKeys(humanName);
  }

  async getHumanNameInput(): Promise<string> {
    return await this.humanNameInput.getAttribute('value');
  }

  async setProtocolInput(protocol: string): Promise<void> {
    await this.protocolInput.sendKeys(protocol);
  }

  async getProtocolInput(): Promise<string> {
    return await this.protocolInput.getAttribute('value');
  }

  async setProtocolValueInput(protocolValue: string): Promise<void> {
    await this.protocolValueInput.sendKeys(protocolValue);
  }

  async getProtocolValueInput(): Promise<string> {
    return await this.protocolValueInput.getAttribute('value');
  }

  getOriginInput(): ElementFinder {
    return this.originInput;
  }

  async fwRuleSelectLastOption(): Promise<void> {
    await this.fwRuleSelect.all(by.tagName('option')).last().click();
  }

  async fwRuleSelectOption(option: string): Promise<void> {
    await this.fwRuleSelect.sendKeys(option);
  }

  getFwRuleSelect(): ElementFinder {
    return this.fwRuleSelect;
  }

  async getFwRuleSelectedOption(): Promise<string> {
    return await this.fwRuleSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FwFilterDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-fwFilter-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-fwFilter'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
