import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FwFilterComponentsPage, FwFilterDeleteDialog, FwFilterUpdatePage } from './fw-filter.page-object';

const expect = chai.expect;

describe('FwFilter e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fwFilterComponentsPage: FwFilterComponentsPage;
  let fwFilterUpdatePage: FwFilterUpdatePage;
  let fwFilterDeleteDialog: FwFilterDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FwFilters', async () => {
    await navBarPage.goToEntity('fw-filter');
    fwFilterComponentsPage = new FwFilterComponentsPage();
    await browser.wait(ec.visibilityOf(fwFilterComponentsPage.title), 5000);
    expect(await fwFilterComponentsPage.getTitle()).to.eq('firewallapiApp.fwFilter.home.title');
    await browser.wait(ec.or(ec.visibilityOf(fwFilterComponentsPage.entities), ec.visibilityOf(fwFilterComponentsPage.noResult)), 1000);
  });

  it('should load create FwFilter page', async () => {
    await fwFilterComponentsPage.clickOnCreateButton();
    fwFilterUpdatePage = new FwFilterUpdatePage();
    expect(await fwFilterUpdatePage.getPageTitle()).to.eq('firewallapiApp.fwFilter.home.createOrEditLabel');
    await fwFilterUpdatePage.cancel();
  });

  it('should create and save FwFilters', async () => {
    const nbButtonsBeforeCreate = await fwFilterComponentsPage.countDeleteButtons();

    await fwFilterComponentsPage.clickOnCreateButton();

    await promise.all([
      fwFilterUpdatePage.setHumanNameInput('humanName'),
      fwFilterUpdatePage.setProtocolInput('protocol'),
      fwFilterUpdatePage.setProtocolValueInput('protocolValue'),
      fwFilterUpdatePage.fwRuleSelectLastOption(),
    ]);

    expect(await fwFilterUpdatePage.getHumanNameInput()).to.eq('humanName', 'Expected HumanName value to be equals to humanName');
    expect(await fwFilterUpdatePage.getProtocolInput()).to.eq('protocol', 'Expected Protocol value to be equals to protocol');
    expect(await fwFilterUpdatePage.getProtocolValueInput()).to.eq(
      'protocolValue',
      'Expected ProtocolValue value to be equals to protocolValue'
    );
    const selectedOrigin = fwFilterUpdatePage.getOriginInput();
    if (await selectedOrigin.isSelected()) {
      await fwFilterUpdatePage.getOriginInput().click();
      expect(await fwFilterUpdatePage.getOriginInput().isSelected(), 'Expected origin not to be selected').to.be.false;
    } else {
      await fwFilterUpdatePage.getOriginInput().click();
      expect(await fwFilterUpdatePage.getOriginInput().isSelected(), 'Expected origin to be selected').to.be.true;
    }

    await fwFilterUpdatePage.save();
    expect(await fwFilterUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await fwFilterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last FwFilter', async () => {
    const nbButtonsBeforeDelete = await fwFilterComponentsPage.countDeleteButtons();
    await fwFilterComponentsPage.clickOnLastDeleteButton();

    fwFilterDeleteDialog = new FwFilterDeleteDialog();
    expect(await fwFilterDeleteDialog.getDialogTitle()).to.eq('firewallapiApp.fwFilter.delete.question');
    await fwFilterDeleteDialog.clickOnConfirmButton();

    expect(await fwFilterComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
