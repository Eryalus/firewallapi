import { element, by, ElementFinder } from 'protractor';

export class FwRuleGroupComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-fw-rule-group div table .btn-danger'));
  title = element.all(by.css('jhi-fw-rule-group div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FwRuleGroupUpdatePage {
  pageTitle = element(by.id('jhi-fw-rule-group-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  humanNameInput = element(by.id('field_humanName'));
  priorityInput = element(by.id('field_priority'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setHumanNameInput(humanName: string): Promise<void> {
    await this.humanNameInput.sendKeys(humanName);
  }

  async getHumanNameInput(): Promise<string> {
    return await this.humanNameInput.getAttribute('value');
  }

  async setPriorityInput(priority: string): Promise<void> {
    await this.priorityInput.sendKeys(priority);
  }

  async getPriorityInput(): Promise<string> {
    return await this.priorityInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FwRuleGroupDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-fwRuleGroup-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-fwRuleGroup'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
