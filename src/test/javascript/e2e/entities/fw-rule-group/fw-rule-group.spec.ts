import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FwRuleGroupComponentsPage, FwRuleGroupDeleteDialog, FwRuleGroupUpdatePage } from './fw-rule-group.page-object';

const expect = chai.expect;

describe('FwRuleGroup e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fwRuleGroupComponentsPage: FwRuleGroupComponentsPage;
  let fwRuleGroupUpdatePage: FwRuleGroupUpdatePage;
  let fwRuleGroupDeleteDialog: FwRuleGroupDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FwRuleGroups', async () => {
    await navBarPage.goToEntity('fw-rule-group');
    fwRuleGroupComponentsPage = new FwRuleGroupComponentsPage();
    await browser.wait(ec.visibilityOf(fwRuleGroupComponentsPage.title), 5000);
    expect(await fwRuleGroupComponentsPage.getTitle()).to.eq('firewallapiApp.fwRuleGroup.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(fwRuleGroupComponentsPage.entities), ec.visibilityOf(fwRuleGroupComponentsPage.noResult)),
      1000
    );
  });

  it('should load create FwRuleGroup page', async () => {
    await fwRuleGroupComponentsPage.clickOnCreateButton();
    fwRuleGroupUpdatePage = new FwRuleGroupUpdatePage();
    expect(await fwRuleGroupUpdatePage.getPageTitle()).to.eq('firewallapiApp.fwRuleGroup.home.createOrEditLabel');
    await fwRuleGroupUpdatePage.cancel();
  });

  it('should create and save FwRuleGroups', async () => {
    const nbButtonsBeforeCreate = await fwRuleGroupComponentsPage.countDeleteButtons();

    await fwRuleGroupComponentsPage.clickOnCreateButton();

    await promise.all([fwRuleGroupUpdatePage.setHumanNameInput('humanName'), fwRuleGroupUpdatePage.setPriorityInput('5')]);

    expect(await fwRuleGroupUpdatePage.getHumanNameInput()).to.eq('humanName', 'Expected HumanName value to be equals to humanName');
    expect(await fwRuleGroupUpdatePage.getPriorityInput()).to.eq('5', 'Expected priority value to be equals to 5');

    await fwRuleGroupUpdatePage.save();
    expect(await fwRuleGroupUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await fwRuleGroupComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last FwRuleGroup', async () => {
    const nbButtonsBeforeDelete = await fwRuleGroupComponentsPage.countDeleteButtons();
    await fwRuleGroupComponentsPage.clickOnLastDeleteButton();

    fwRuleGroupDeleteDialog = new FwRuleGroupDeleteDialog();
    expect(await fwRuleGroupDeleteDialog.getDialogTitle()).to.eq('firewallapiApp.fwRuleGroup.delete.question');
    await fwRuleGroupDeleteDialog.clickOnConfirmButton();

    expect(await fwRuleGroupComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
