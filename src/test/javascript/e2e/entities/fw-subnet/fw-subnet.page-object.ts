import { element, by, ElementFinder } from 'protractor';

export class FwSubnetComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-fw-subnet div table .btn-danger'));
  title = element.all(by.css('jhi-fw-subnet div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FwSubnetUpdatePage {
  pageTitle = element(by.id('jhi-fw-subnet-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  ipPrefixInput = element(by.id('field_ipPrefix'));
  ipMaskInput = element(by.id('field_ipMask'));
  allowInput = element(by.id('field_allow'));
  priorityInput = element(by.id('field_priority'));

  fwNetworkSelect = element(by.id('field_fwNetwork'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setIpPrefixInput(ipPrefix: string): Promise<void> {
    await this.ipPrefixInput.sendKeys(ipPrefix);
  }

  async getIpPrefixInput(): Promise<string> {
    return await this.ipPrefixInput.getAttribute('value');
  }

  async setIpMaskInput(ipMask: string): Promise<void> {
    await this.ipMaskInput.sendKeys(ipMask);
  }

  async getIpMaskInput(): Promise<string> {
    return await this.ipMaskInput.getAttribute('value');
  }

  getAllowInput(): ElementFinder {
    return this.allowInput;
  }

  async setPriorityInput(priority: string): Promise<void> {
    await this.priorityInput.sendKeys(priority);
  }

  async getPriorityInput(): Promise<string> {
    return await this.priorityInput.getAttribute('value');
  }

  async fwNetworkSelectLastOption(): Promise<void> {
    await this.fwNetworkSelect.all(by.tagName('option')).last().click();
  }

  async fwNetworkSelectOption(option: string): Promise<void> {
    await this.fwNetworkSelect.sendKeys(option);
  }

  getFwNetworkSelect(): ElementFinder {
    return this.fwNetworkSelect;
  }

  async getFwNetworkSelectedOption(): Promise<string> {
    return await this.fwNetworkSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FwSubnetDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-fwSubnet-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-fwSubnet'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
