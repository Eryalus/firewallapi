import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FwSubnetComponentsPage, FwSubnetDeleteDialog, FwSubnetUpdatePage } from './fw-subnet.page-object';

const expect = chai.expect;

describe('FwSubnet e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fwSubnetComponentsPage: FwSubnetComponentsPage;
  let fwSubnetUpdatePage: FwSubnetUpdatePage;
  let fwSubnetDeleteDialog: FwSubnetDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FwSubnets', async () => {
    await navBarPage.goToEntity('fw-subnet');
    fwSubnetComponentsPage = new FwSubnetComponentsPage();
    await browser.wait(ec.visibilityOf(fwSubnetComponentsPage.title), 5000);
    expect(await fwSubnetComponentsPage.getTitle()).to.eq('firewallapiApp.fwSubnet.home.title');
    await browser.wait(ec.or(ec.visibilityOf(fwSubnetComponentsPage.entities), ec.visibilityOf(fwSubnetComponentsPage.noResult)), 1000);
  });

  it('should load create FwSubnet page', async () => {
    await fwSubnetComponentsPage.clickOnCreateButton();
    fwSubnetUpdatePage = new FwSubnetUpdatePage();
    expect(await fwSubnetUpdatePage.getPageTitle()).to.eq('firewallapiApp.fwSubnet.home.createOrEditLabel');
    await fwSubnetUpdatePage.cancel();
  });

  it('should create and save FwSubnets', async () => {
    const nbButtonsBeforeCreate = await fwSubnetComponentsPage.countDeleteButtons();

    await fwSubnetComponentsPage.clickOnCreateButton();

    await promise.all([
      fwSubnetUpdatePage.setIpPrefixInput('ipPrefix'),
      fwSubnetUpdatePage.setIpMaskInput('ipMask'),
      fwSubnetUpdatePage.setPriorityInput('5'),
      fwSubnetUpdatePage.fwNetworkSelectLastOption(),
    ]);

    expect(await fwSubnetUpdatePage.getIpPrefixInput()).to.eq('ipPrefix', 'Expected IpPrefix value to be equals to ipPrefix');
    expect(await fwSubnetUpdatePage.getIpMaskInput()).to.eq('ipMask', 'Expected IpMask value to be equals to ipMask');
    const selectedAllow = fwSubnetUpdatePage.getAllowInput();
    if (await selectedAllow.isSelected()) {
      await fwSubnetUpdatePage.getAllowInput().click();
      expect(await fwSubnetUpdatePage.getAllowInput().isSelected(), 'Expected allow not to be selected').to.be.false;
    } else {
      await fwSubnetUpdatePage.getAllowInput().click();
      expect(await fwSubnetUpdatePage.getAllowInput().isSelected(), 'Expected allow to be selected').to.be.true;
    }
    expect(await fwSubnetUpdatePage.getPriorityInput()).to.eq('5', 'Expected priority value to be equals to 5');

    await fwSubnetUpdatePage.save();
    expect(await fwSubnetUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await fwSubnetComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last FwSubnet', async () => {
    const nbButtonsBeforeDelete = await fwSubnetComponentsPage.countDeleteButtons();
    await fwSubnetComponentsPage.clickOnLastDeleteButton();

    fwSubnetDeleteDialog = new FwSubnetDeleteDialog();
    expect(await fwSubnetDeleteDialog.getDialogTitle()).to.eq('firewallapiApp.fwSubnet.delete.question');
    await fwSubnetDeleteDialog.clickOnConfirmButton();

    expect(await fwSubnetComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
