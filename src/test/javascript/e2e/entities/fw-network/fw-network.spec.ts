import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FwNetworkComponentsPage, FwNetworkDeleteDialog, FwNetworkUpdatePage } from './fw-network.page-object';

const expect = chai.expect;

describe('FwNetwork e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fwNetworkComponentsPage: FwNetworkComponentsPage;
  let fwNetworkUpdatePage: FwNetworkUpdatePage;
  let fwNetworkDeleteDialog: FwNetworkDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FwNetworks', async () => {
    await navBarPage.goToEntity('fw-network');
    fwNetworkComponentsPage = new FwNetworkComponentsPage();
    await browser.wait(ec.visibilityOf(fwNetworkComponentsPage.title), 5000);
    expect(await fwNetworkComponentsPage.getTitle()).to.eq('firewallapiApp.fwNetwork.home.title');
    await browser.wait(ec.or(ec.visibilityOf(fwNetworkComponentsPage.entities), ec.visibilityOf(fwNetworkComponentsPage.noResult)), 1000);
  });

  it('should load create FwNetwork page', async () => {
    await fwNetworkComponentsPage.clickOnCreateButton();
    fwNetworkUpdatePage = new FwNetworkUpdatePage();
    expect(await fwNetworkUpdatePage.getPageTitle()).to.eq('firewallapiApp.fwNetwork.home.createOrEditLabel');
    await fwNetworkUpdatePage.cancel();
  });

  it('should create and save FwNetworks', async () => {
    const nbButtonsBeforeCreate = await fwNetworkComponentsPage.countDeleteButtons();

    await fwNetworkComponentsPage.clickOnCreateButton();

    await promise.all([fwNetworkUpdatePage.setHumanNameInput('humanName')]);

    expect(await fwNetworkUpdatePage.getHumanNameInput()).to.eq('humanName', 'Expected HumanName value to be equals to humanName');

    await fwNetworkUpdatePage.save();
    expect(await fwNetworkUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await fwNetworkComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last FwNetwork', async () => {
    const nbButtonsBeforeDelete = await fwNetworkComponentsPage.countDeleteButtons();
    await fwNetworkComponentsPage.clickOnLastDeleteButton();

    fwNetworkDeleteDialog = new FwNetworkDeleteDialog();
    expect(await fwNetworkDeleteDialog.getDialogTitle()).to.eq('firewallapiApp.fwNetwork.delete.question');
    await fwNetworkDeleteDialog.clickOnConfirmButton();

    expect(await fwNetworkComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
