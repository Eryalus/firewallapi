import { element, by, ElementFinder } from 'protractor';

export class FwNetworkComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-fw-network div table .btn-danger'));
  title = element.all(by.css('jhi-fw-network div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FwNetworkUpdatePage {
  pageTitle = element(by.id('jhi-fw-network-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  humanNameInput = element(by.id('field_humanName'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setHumanNameInput(humanName: string): Promise<void> {
    await this.humanNameInput.sendKeys(humanName);
  }

  async getHumanNameInput(): Promise<string> {
    return await this.humanNameInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FwNetworkDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-fwNetwork-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-fwNetwork'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
