import { element, by, ElementFinder } from 'protractor';

export class OnosControllerComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-onos-controller div table .btn-danger'));
  title = element.all(by.css('jhi-onos-controller div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class OnosControllerUpdatePage {
  pageTitle = element(by.id('jhi-onos-controller-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  humanNameInput = element(by.id('field_humanName'));
  ipInput = element(by.id('field_ip'));
  portInput = element(by.id('field_port'));
  userInput = element(by.id('field_user'));
  passwordInput = element(by.id('field_password'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setHumanNameInput(humanName: string): Promise<void> {
    await this.humanNameInput.sendKeys(humanName);
  }

  async getHumanNameInput(): Promise<string> {
    return await this.humanNameInput.getAttribute('value');
  }

  async setIpInput(ip: string): Promise<void> {
    await this.ipInput.sendKeys(ip);
  }

  async getIpInput(): Promise<string> {
    return await this.ipInput.getAttribute('value');
  }

  async setPortInput(port: string): Promise<void> {
    await this.portInput.sendKeys(port);
  }

  async getPortInput(): Promise<string> {
    return await this.portInput.getAttribute('value');
  }

  async setUserInput(user: string): Promise<void> {
    await this.userInput.sendKeys(user);
  }

  async getUserInput(): Promise<string> {
    return await this.userInput.getAttribute('value');
  }

  async setPasswordInput(password: string): Promise<void> {
    await this.passwordInput.sendKeys(password);
  }

  async getPasswordInput(): Promise<string> {
    return await this.passwordInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class OnosControllerDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-onosController-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-onosController'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
