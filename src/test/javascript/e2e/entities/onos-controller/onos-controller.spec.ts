import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OnosControllerComponentsPage, OnosControllerDeleteDialog, OnosControllerUpdatePage } from './onos-controller.page-object';

const expect = chai.expect;

describe('OnosController e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let onosControllerComponentsPage: OnosControllerComponentsPage;
  let onosControllerUpdatePage: OnosControllerUpdatePage;
  let onosControllerDeleteDialog: OnosControllerDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load OnosControllers', async () => {
    await navBarPage.goToEntity('onos-controller');
    onosControllerComponentsPage = new OnosControllerComponentsPage();
    await browser.wait(ec.visibilityOf(onosControllerComponentsPage.title), 5000);
    expect(await onosControllerComponentsPage.getTitle()).to.eq('firewallapiApp.onosController.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(onosControllerComponentsPage.entities), ec.visibilityOf(onosControllerComponentsPage.noResult)),
      1000
    );
  });

  it('should load create OnosController page', async () => {
    await onosControllerComponentsPage.clickOnCreateButton();
    onosControllerUpdatePage = new OnosControllerUpdatePage();
    expect(await onosControllerUpdatePage.getPageTitle()).to.eq('firewallapiApp.onosController.home.createOrEditLabel');
    await onosControllerUpdatePage.cancel();
  });

  it('should create and save OnosControllers', async () => {
    const nbButtonsBeforeCreate = await onosControllerComponentsPage.countDeleteButtons();

    await onosControllerComponentsPage.clickOnCreateButton();

    await promise.all([
      onosControllerUpdatePage.setHumanNameInput('humanName'),
      onosControllerUpdatePage.setIpInput('ip'),
      onosControllerUpdatePage.setPortInput('5'),
      onosControllerUpdatePage.setUserInput('user'),
      onosControllerUpdatePage.setPasswordInput('password'),
    ]);

    expect(await onosControllerUpdatePage.getHumanNameInput()).to.eq('humanName', 'Expected HumanName value to be equals to humanName');
    expect(await onosControllerUpdatePage.getIpInput()).to.eq('ip', 'Expected Ip value to be equals to ip');
    expect(await onosControllerUpdatePage.getPortInput()).to.eq('5', 'Expected port value to be equals to 5');
    expect(await onosControllerUpdatePage.getUserInput()).to.eq('user', 'Expected User value to be equals to user');
    expect(await onosControllerUpdatePage.getPasswordInput()).to.eq('password', 'Expected Password value to be equals to password');

    await onosControllerUpdatePage.save();
    expect(await onosControllerUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await onosControllerComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last OnosController', async () => {
    const nbButtonsBeforeDelete = await onosControllerComponentsPage.countDeleteButtons();
    await onosControllerComponentsPage.clickOnLastDeleteButton();

    onosControllerDeleteDialog = new OnosControllerDeleteDialog();
    expect(await onosControllerDeleteDialog.getDialogTitle()).to.eq('firewallapiApp.onosController.delete.question');
    await onosControllerDeleteDialog.clickOnConfirmButton();

    expect(await onosControllerComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
