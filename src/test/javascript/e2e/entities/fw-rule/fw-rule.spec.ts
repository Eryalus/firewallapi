import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FwRuleComponentsPage, FwRuleDeleteDialog, FwRuleUpdatePage } from './fw-rule.page-object';

const expect = chai.expect;

describe('FwRule e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fwRuleComponentsPage: FwRuleComponentsPage;
  let fwRuleUpdatePage: FwRuleUpdatePage;
  let fwRuleDeleteDialog: FwRuleDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FwRules', async () => {
    await navBarPage.goToEntity('fw-rule');
    fwRuleComponentsPage = new FwRuleComponentsPage();
    await browser.wait(ec.visibilityOf(fwRuleComponentsPage.title), 5000);
    expect(await fwRuleComponentsPage.getTitle()).to.eq('firewallapiApp.fwRule.home.title');
    await browser.wait(ec.or(ec.visibilityOf(fwRuleComponentsPage.entities), ec.visibilityOf(fwRuleComponentsPage.noResult)), 1000);
  });

  it('should load create FwRule page', async () => {
    await fwRuleComponentsPage.clickOnCreateButton();
    fwRuleUpdatePage = new FwRuleUpdatePage();
    expect(await fwRuleUpdatePage.getPageTitle()).to.eq('firewallapiApp.fwRule.home.createOrEditLabel');
    await fwRuleUpdatePage.cancel();
  });

  it('should create and save FwRules', async () => {
    const nbButtonsBeforeCreate = await fwRuleComponentsPage.countDeleteButtons();

    await fwRuleComponentsPage.clickOnCreateButton();

    await promise.all([
      fwRuleUpdatePage.setHumanNameInput('humanName'),
      fwRuleUpdatePage.setPriorityInput('5'),
      fwRuleUpdatePage.setActionInput('action'),
      fwRuleUpdatePage.fwRuleGroupSelectLastOption(),
    ]);

    expect(await fwRuleUpdatePage.getHumanNameInput()).to.eq('humanName', 'Expected HumanName value to be equals to humanName');
    expect(await fwRuleUpdatePage.getPriorityInput()).to.eq('5', 'Expected priority value to be equals to 5');
    expect(await fwRuleUpdatePage.getActionInput()).to.eq('action', 'Expected Action value to be equals to action');

    await fwRuleUpdatePage.save();
    expect(await fwRuleUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await fwRuleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last FwRule', async () => {
    const nbButtonsBeforeDelete = await fwRuleComponentsPage.countDeleteButtons();
    await fwRuleComponentsPage.clickOnLastDeleteButton();

    fwRuleDeleteDialog = new FwRuleDeleteDialog();
    expect(await fwRuleDeleteDialog.getDialogTitle()).to.eq('firewallapiApp.fwRule.delete.question');
    await fwRuleDeleteDialog.clickOnConfirmButton();

    expect(await fwRuleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
