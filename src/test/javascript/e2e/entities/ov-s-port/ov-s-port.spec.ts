import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OvSPortComponentsPage, OvSPortDeleteDialog, OvSPortUpdatePage } from './ov-s-port.page-object';

const expect = chai.expect;

describe('OvSPort e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let ovSPortComponentsPage: OvSPortComponentsPage;
  let ovSPortUpdatePage: OvSPortUpdatePage;
  let ovSPortDeleteDialog: OvSPortDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load OvSPorts', async () => {
    await navBarPage.goToEntity('ov-s-port');
    ovSPortComponentsPage = new OvSPortComponentsPage();
    await browser.wait(ec.visibilityOf(ovSPortComponentsPage.title), 5000);
    expect(await ovSPortComponentsPage.getTitle()).to.eq('firewallapiApp.ovSPort.home.title');
    await browser.wait(ec.or(ec.visibilityOf(ovSPortComponentsPage.entities), ec.visibilityOf(ovSPortComponentsPage.noResult)), 1000);
  });

  it('should load create OvSPort page', async () => {
    await ovSPortComponentsPage.clickOnCreateButton();
    ovSPortUpdatePage = new OvSPortUpdatePage();
    expect(await ovSPortUpdatePage.getPageTitle()).to.eq('firewallapiApp.ovSPort.home.createOrEditLabel');
    await ovSPortUpdatePage.cancel();
  });

  it('should create and save OvSPorts', async () => {
    const nbButtonsBeforeCreate = await ovSPortComponentsPage.countDeleteButtons();

    await ovSPortComponentsPage.clickOnCreateButton();

    await promise.all([
      ovSPortUpdatePage.setHumanNameInput('humanName'),
      ovSPortUpdatePage.setOnosPortNameInput('onosPortName'),
      // ovSPortUpdatePage.fwNetworkSelectLastOption(),
      ovSPortUpdatePage.ovSwitchSelectLastOption(),
    ]);

    expect(await ovSPortUpdatePage.getHumanNameInput()).to.eq('humanName', 'Expected HumanName value to be equals to humanName');
    expect(await ovSPortUpdatePage.getOnosPortNameInput()).to.eq(
      'onosPortName',
      'Expected OnosPortName value to be equals to onosPortName'
    );

    await ovSPortUpdatePage.save();
    expect(await ovSPortUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await ovSPortComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last OvSPort', async () => {
    const nbButtonsBeforeDelete = await ovSPortComponentsPage.countDeleteButtons();
    await ovSPortComponentsPage.clickOnLastDeleteButton();

    ovSPortDeleteDialog = new OvSPortDeleteDialog();
    expect(await ovSPortDeleteDialog.getDialogTitle()).to.eq('firewallapiApp.ovSPort.delete.question');
    await ovSPortDeleteDialog.clickOnConfirmButton();

    expect(await ovSPortComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
