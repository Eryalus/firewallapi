import { element, by, ElementFinder } from 'protractor';

export class OvSPortComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-ov-s-port div table .btn-danger'));
  title = element.all(by.css('jhi-ov-s-port div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class OvSPortUpdatePage {
  pageTitle = element(by.id('jhi-ov-s-port-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  humanNameInput = element(by.id('field_humanName'));
  onosPortNameInput = element(by.id('field_onosPortName'));

  fwNetworkSelect = element(by.id('field_fwNetwork'));
  ovSwitchSelect = element(by.id('field_ovSwitch'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setHumanNameInput(humanName: string): Promise<void> {
    await this.humanNameInput.sendKeys(humanName);
  }

  async getHumanNameInput(): Promise<string> {
    return await this.humanNameInput.getAttribute('value');
  }

  async setOnosPortNameInput(onosPortName: string): Promise<void> {
    await this.onosPortNameInput.sendKeys(onosPortName);
  }

  async getOnosPortNameInput(): Promise<string> {
    return await this.onosPortNameInput.getAttribute('value');
  }

  async fwNetworkSelectLastOption(): Promise<void> {
    await this.fwNetworkSelect.all(by.tagName('option')).last().click();
  }

  async fwNetworkSelectOption(option: string): Promise<void> {
    await this.fwNetworkSelect.sendKeys(option);
  }

  getFwNetworkSelect(): ElementFinder {
    return this.fwNetworkSelect;
  }

  async getFwNetworkSelectedOption(): Promise<string> {
    return await this.fwNetworkSelect.element(by.css('option:checked')).getText();
  }

  async ovSwitchSelectLastOption(): Promise<void> {
    await this.ovSwitchSelect.all(by.tagName('option')).last().click();
  }

  async ovSwitchSelectOption(option: string): Promise<void> {
    await this.ovSwitchSelect.sendKeys(option);
  }

  getOvSwitchSelect(): ElementFinder {
    return this.ovSwitchSelect;
  }

  async getOvSwitchSelectedOption(): Promise<string> {
    return await this.ovSwitchSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class OvSPortDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-ovSPort-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-ovSPort'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
