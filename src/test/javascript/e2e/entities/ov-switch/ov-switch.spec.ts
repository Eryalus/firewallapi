import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { OvSwitchComponentsPage, OvSwitchDeleteDialog, OvSwitchUpdatePage } from './ov-switch.page-object';

const expect = chai.expect;

describe('OvSwitch e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let ovSwitchComponentsPage: OvSwitchComponentsPage;
  let ovSwitchUpdatePage: OvSwitchUpdatePage;
  let ovSwitchDeleteDialog: OvSwitchDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load OvSwitches', async () => {
    await navBarPage.goToEntity('ov-switch');
    ovSwitchComponentsPage = new OvSwitchComponentsPage();
    await browser.wait(ec.visibilityOf(ovSwitchComponentsPage.title), 5000);
    expect(await ovSwitchComponentsPage.getTitle()).to.eq('firewallapiApp.ovSwitch.home.title');
    await browser.wait(ec.or(ec.visibilityOf(ovSwitchComponentsPage.entities), ec.visibilityOf(ovSwitchComponentsPage.noResult)), 1000);
  });

  it('should load create OvSwitch page', async () => {
    await ovSwitchComponentsPage.clickOnCreateButton();
    ovSwitchUpdatePage = new OvSwitchUpdatePage();
    expect(await ovSwitchUpdatePage.getPageTitle()).to.eq('firewallapiApp.ovSwitch.home.createOrEditLabel');
    await ovSwitchUpdatePage.cancel();
  });

  it('should create and save OvSwitches', async () => {
    const nbButtonsBeforeCreate = await ovSwitchComponentsPage.countDeleteButtons();

    await ovSwitchComponentsPage.clickOnCreateButton();

    await promise.all([ovSwitchUpdatePage.setHumanNameInput('humanName'), ovSwitchUpdatePage.setOnosDeviceIdInput('onosDeviceId')]);

    expect(await ovSwitchUpdatePage.getHumanNameInput()).to.eq('humanName', 'Expected HumanName value to be equals to humanName');
    expect(await ovSwitchUpdatePage.getOnosDeviceIdInput()).to.eq(
      'onosDeviceId',
      'Expected OnosDeviceId value to be equals to onosDeviceId'
    );

    await ovSwitchUpdatePage.save();
    expect(await ovSwitchUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await ovSwitchComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last OvSwitch', async () => {
    const nbButtonsBeforeDelete = await ovSwitchComponentsPage.countDeleteButtons();
    await ovSwitchComponentsPage.clickOnLastDeleteButton();

    ovSwitchDeleteDialog = new OvSwitchDeleteDialog();
    expect(await ovSwitchDeleteDialog.getDialogTitle()).to.eq('firewallapiApp.ovSwitch.delete.question');
    await ovSwitchDeleteDialog.clickOnConfirmButton();

    expect(await ovSwitchComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
