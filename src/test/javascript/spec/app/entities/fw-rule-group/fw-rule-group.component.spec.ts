import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FirewallapiTestModule } from '../../../test.module';
import { FwRuleGroupComponent } from 'app/entities/fw-rule-group/fw-rule-group.component';
import { FwRuleGroupService } from 'app/entities/fw-rule-group/fw-rule-group.service';
import { FwRuleGroup } from 'app/shared/model/fw-rule-group.model';

describe('Component Tests', () => {
  describe('FwRuleGroup Management Component', () => {
    let comp: FwRuleGroupComponent;
    let fixture: ComponentFixture<FwRuleGroupComponent>;
    let service: FwRuleGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwRuleGroupComponent],
      })
        .overrideTemplate(FwRuleGroupComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwRuleGroupComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwRuleGroupService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FwRuleGroup(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fwRuleGroups && comp.fwRuleGroups[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
