import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwRuleGroupUpdateComponent } from 'app/entities/fw-rule-group/fw-rule-group-update.component';
import { FwRuleGroupService } from 'app/entities/fw-rule-group/fw-rule-group.service';
import { FwRuleGroup } from 'app/shared/model/fw-rule-group.model';

describe('Component Tests', () => {
  describe('FwRuleGroup Management Update Component', () => {
    let comp: FwRuleGroupUpdateComponent;
    let fixture: ComponentFixture<FwRuleGroupUpdateComponent>;
    let service: FwRuleGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwRuleGroupUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FwRuleGroupUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwRuleGroupUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwRuleGroupService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwRuleGroup(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwRuleGroup();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
