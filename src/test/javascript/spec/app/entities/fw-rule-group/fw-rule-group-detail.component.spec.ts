import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwRuleGroupDetailComponent } from 'app/entities/fw-rule-group/fw-rule-group-detail.component';
import { FwRuleGroup } from 'app/shared/model/fw-rule-group.model';

describe('Component Tests', () => {
  describe('FwRuleGroup Management Detail Component', () => {
    let comp: FwRuleGroupDetailComponent;
    let fixture: ComponentFixture<FwRuleGroupDetailComponent>;
    const route = ({ data: of({ fwRuleGroup: new FwRuleGroup(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwRuleGroupDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FwRuleGroupDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FwRuleGroupDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fwRuleGroup on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fwRuleGroup).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
