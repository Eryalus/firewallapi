import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwFilterUpdateComponent } from 'app/entities/fw-filter/fw-filter-update.component';
import { FwFilterService } from 'app/entities/fw-filter/fw-filter.service';
import { FwFilter } from 'app/shared/model/fw-filter.model';

describe('Component Tests', () => {
  describe('FwFilter Management Update Component', () => {
    let comp: FwFilterUpdateComponent;
    let fixture: ComponentFixture<FwFilterUpdateComponent>;
    let service: FwFilterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwFilterUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FwFilterUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwFilterUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwFilterService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwFilter(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwFilter();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
