import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FirewallapiTestModule } from '../../../test.module';
import { FwFilterComponent } from 'app/entities/fw-filter/fw-filter.component';
import { FwFilterService } from 'app/entities/fw-filter/fw-filter.service';
import { FwFilter } from 'app/shared/model/fw-filter.model';

describe('Component Tests', () => {
  describe('FwFilter Management Component', () => {
    let comp: FwFilterComponent;
    let fixture: ComponentFixture<FwFilterComponent>;
    let service: FwFilterService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwFilterComponent],
      })
        .overrideTemplate(FwFilterComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwFilterComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwFilterService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FwFilter(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fwFilters && comp.fwFilters[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
