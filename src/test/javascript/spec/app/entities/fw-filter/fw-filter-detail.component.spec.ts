import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwFilterDetailComponent } from 'app/entities/fw-filter/fw-filter-detail.component';
import { FwFilter } from 'app/shared/model/fw-filter.model';

describe('Component Tests', () => {
  describe('FwFilter Management Detail Component', () => {
    let comp: FwFilterDetailComponent;
    let fixture: ComponentFixture<FwFilterDetailComponent>;
    const route = ({ data: of({ fwFilter: new FwFilter(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwFilterDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FwFilterDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FwFilterDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fwFilter on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fwFilter).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
