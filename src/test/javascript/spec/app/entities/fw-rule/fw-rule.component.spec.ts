import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FirewallapiTestModule } from '../../../test.module';
import { FwRuleComponent } from 'app/entities/fw-rule/fw-rule.component';
import { FwRuleService } from 'app/entities/fw-rule/fw-rule.service';
import { FwRule } from 'app/shared/model/fw-rule.model';

describe('Component Tests', () => {
  describe('FwRule Management Component', () => {
    let comp: FwRuleComponent;
    let fixture: ComponentFixture<FwRuleComponent>;
    let service: FwRuleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwRuleComponent],
      })
        .overrideTemplate(FwRuleComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwRuleComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwRuleService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FwRule(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fwRules && comp.fwRules[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
