import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwRuleUpdateComponent } from 'app/entities/fw-rule/fw-rule-update.component';
import { FwRuleService } from 'app/entities/fw-rule/fw-rule.service';
import { FwRule } from 'app/shared/model/fw-rule.model';

describe('Component Tests', () => {
  describe('FwRule Management Update Component', () => {
    let comp: FwRuleUpdateComponent;
    let fixture: ComponentFixture<FwRuleUpdateComponent>;
    let service: FwRuleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwRuleUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FwRuleUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwRuleUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwRuleService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwRule(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwRule();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
