import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwRuleDetailComponent } from 'app/entities/fw-rule/fw-rule-detail.component';
import { FwRule } from 'app/shared/model/fw-rule.model';

describe('Component Tests', () => {
  describe('FwRule Management Detail Component', () => {
    let comp: FwRuleDetailComponent;
    let fixture: ComponentFixture<FwRuleDetailComponent>;
    const route = ({ data: of({ fwRule: new FwRule(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwRuleDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FwRuleDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FwRuleDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fwRule on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fwRule).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
