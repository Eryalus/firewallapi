import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { OvSPortDetailComponent } from 'app/entities/ov-s-port/ov-s-port-detail.component';
import { OvSPort } from 'app/shared/model/ov-s-port.model';

describe('Component Tests', () => {
  describe('OvSPort Management Detail Component', () => {
    let comp: OvSPortDetailComponent;
    let fixture: ComponentFixture<OvSPortDetailComponent>;
    const route = ({ data: of({ ovSPort: new OvSPort(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [OvSPortDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(OvSPortDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OvSPortDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load ovSPort on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.ovSPort).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
