import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { OvSPortUpdateComponent } from 'app/entities/ov-s-port/ov-s-port-update.component';
import { OvSPortService } from 'app/entities/ov-s-port/ov-s-port.service';
import { OvSPort } from 'app/shared/model/ov-s-port.model';

describe('Component Tests', () => {
  describe('OvSPort Management Update Component', () => {
    let comp: OvSPortUpdateComponent;
    let fixture: ComponentFixture<OvSPortUpdateComponent>;
    let service: OvSPortService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [OvSPortUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(OvSPortUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OvSPortUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OvSPortService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OvSPort(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OvSPort();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
