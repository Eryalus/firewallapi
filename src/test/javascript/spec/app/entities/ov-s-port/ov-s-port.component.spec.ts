import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FirewallapiTestModule } from '../../../test.module';
import { OvSPortComponent } from 'app/entities/ov-s-port/ov-s-port.component';
import { OvSPortService } from 'app/entities/ov-s-port/ov-s-port.service';
import { OvSPort } from 'app/shared/model/ov-s-port.model';

describe('Component Tests', () => {
  describe('OvSPort Management Component', () => {
    let comp: OvSPortComponent;
    let fixture: ComponentFixture<OvSPortComponent>;
    let service: OvSPortService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [OvSPortComponent],
      })
        .overrideTemplate(OvSPortComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OvSPortComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OvSPortService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new OvSPort(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.ovSPorts && comp.ovSPorts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
