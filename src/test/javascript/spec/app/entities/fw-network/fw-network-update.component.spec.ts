import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwNetworkUpdateComponent } from 'app/entities/fw-network/fw-network-update.component';
import { FwNetworkService } from 'app/entities/fw-network/fw-network.service';
import { FwNetwork } from 'app/shared/model/fw-network.model';

describe('Component Tests', () => {
  describe('FwNetwork Management Update Component', () => {
    let comp: FwNetworkUpdateComponent;
    let fixture: ComponentFixture<FwNetworkUpdateComponent>;
    let service: FwNetworkService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwNetworkUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FwNetworkUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwNetworkUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwNetworkService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwNetwork(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwNetwork();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
