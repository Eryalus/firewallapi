import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FirewallapiTestModule } from '../../../test.module';
import { FwNetworkComponent } from 'app/entities/fw-network/fw-network.component';
import { FwNetworkService } from 'app/entities/fw-network/fw-network.service';
import { FwNetwork } from 'app/shared/model/fw-network.model';

describe('Component Tests', () => {
  describe('FwNetwork Management Component', () => {
    let comp: FwNetworkComponent;
    let fixture: ComponentFixture<FwNetworkComponent>;
    let service: FwNetworkService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwNetworkComponent],
      })
        .overrideTemplate(FwNetworkComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwNetworkComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwNetworkService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FwNetwork(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fwNetworks && comp.fwNetworks[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
