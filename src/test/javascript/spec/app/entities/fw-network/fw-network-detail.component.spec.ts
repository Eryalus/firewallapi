import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwNetworkDetailComponent } from 'app/entities/fw-network/fw-network-detail.component';
import { FwNetwork } from 'app/shared/model/fw-network.model';

describe('Component Tests', () => {
  describe('FwNetwork Management Detail Component', () => {
    let comp: FwNetworkDetailComponent;
    let fixture: ComponentFixture<FwNetworkDetailComponent>;
    const route = ({ data: of({ fwNetwork: new FwNetwork(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwNetworkDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FwNetworkDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FwNetworkDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fwNetwork on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fwNetwork).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
