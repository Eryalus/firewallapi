import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FirewallapiTestModule } from '../../../test.module';
import { OvSwitchComponent } from 'app/entities/ov-switch/ov-switch.component';
import { OvSwitchService } from 'app/entities/ov-switch/ov-switch.service';
import { OvSwitch } from 'app/shared/model/ov-switch.model';

describe('Component Tests', () => {
  describe('OvSwitch Management Component', () => {
    let comp: OvSwitchComponent;
    let fixture: ComponentFixture<OvSwitchComponent>;
    let service: OvSwitchService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [OvSwitchComponent],
      })
        .overrideTemplate(OvSwitchComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OvSwitchComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OvSwitchService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new OvSwitch(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.ovSwitches && comp.ovSwitches[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
