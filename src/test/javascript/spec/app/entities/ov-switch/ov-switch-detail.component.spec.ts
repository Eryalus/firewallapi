import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { OvSwitchDetailComponent } from 'app/entities/ov-switch/ov-switch-detail.component';
import { OvSwitch } from 'app/shared/model/ov-switch.model';

describe('Component Tests', () => {
  describe('OvSwitch Management Detail Component', () => {
    let comp: OvSwitchDetailComponent;
    let fixture: ComponentFixture<OvSwitchDetailComponent>;
    const route = ({ data: of({ ovSwitch: new OvSwitch(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [OvSwitchDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(OvSwitchDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OvSwitchDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load ovSwitch on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.ovSwitch).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
