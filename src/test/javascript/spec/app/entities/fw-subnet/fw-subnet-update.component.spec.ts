import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwSubnetUpdateComponent } from 'app/entities/fw-subnet/fw-subnet-update.component';
import { FwSubnetService } from 'app/entities/fw-subnet/fw-subnet.service';
import { FwSubnet } from 'app/shared/model/fw-subnet.model';

describe('Component Tests', () => {
  describe('FwSubnet Management Update Component', () => {
    let comp: FwSubnetUpdateComponent;
    let fixture: ComponentFixture<FwSubnetUpdateComponent>;
    let service: FwSubnetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwSubnetUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FwSubnetUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwSubnetUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwSubnetService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwSubnet(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FwSubnet();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
