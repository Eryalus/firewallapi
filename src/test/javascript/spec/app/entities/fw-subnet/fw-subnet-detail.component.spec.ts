import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { FwSubnetDetailComponent } from 'app/entities/fw-subnet/fw-subnet-detail.component';
import { FwSubnet } from 'app/shared/model/fw-subnet.model';

describe('Component Tests', () => {
  describe('FwSubnet Management Detail Component', () => {
    let comp: FwSubnetDetailComponent;
    let fixture: ComponentFixture<FwSubnetDetailComponent>;
    const route = ({ data: of({ fwSubnet: new FwSubnet(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwSubnetDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FwSubnetDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FwSubnetDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fwSubnet on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fwSubnet).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
