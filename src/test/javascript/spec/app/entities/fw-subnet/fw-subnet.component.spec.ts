import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FirewallapiTestModule } from '../../../test.module';
import { FwSubnetComponent } from 'app/entities/fw-subnet/fw-subnet.component';
import { FwSubnetService } from 'app/entities/fw-subnet/fw-subnet.service';
import { FwSubnet } from 'app/shared/model/fw-subnet.model';

describe('Component Tests', () => {
  describe('FwSubnet Management Component', () => {
    let comp: FwSubnetComponent;
    let fixture: ComponentFixture<FwSubnetComponent>;
    let service: FwSubnetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [FwSubnetComponent],
      })
        .overrideTemplate(FwSubnetComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FwSubnetComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FwSubnetService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FwSubnet(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fwSubnets && comp.fwSubnets[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
