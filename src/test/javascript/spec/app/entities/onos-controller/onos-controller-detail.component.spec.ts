import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { OnosControllerDetailComponent } from 'app/entities/onos-controller/onos-controller-detail.component';
import { OnosController } from 'app/shared/model/onos-controller.model';

describe('Component Tests', () => {
  describe('OnosController Management Detail Component', () => {
    let comp: OnosControllerDetailComponent;
    let fixture: ComponentFixture<OnosControllerDetailComponent>;
    const route = ({ data: of({ onosController: new OnosController(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [OnosControllerDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(OnosControllerDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OnosControllerDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load onosController on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.onosController).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
