import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FirewallapiTestModule } from '../../../test.module';
import { OnosControllerComponent } from 'app/entities/onos-controller/onos-controller.component';
import { OnosControllerService } from 'app/entities/onos-controller/onos-controller.service';
import { OnosController } from 'app/shared/model/onos-controller.model';

describe('Component Tests', () => {
  describe('OnosController Management Component', () => {
    let comp: OnosControllerComponent;
    let fixture: ComponentFixture<OnosControllerComponent>;
    let service: OnosControllerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [OnosControllerComponent],
      })
        .overrideTemplate(OnosControllerComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OnosControllerComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OnosControllerService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new OnosController(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.onosControllers && comp.onosControllers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
