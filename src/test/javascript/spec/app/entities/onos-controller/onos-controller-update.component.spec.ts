import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FirewallapiTestModule } from '../../../test.module';
import { OnosControllerUpdateComponent } from 'app/entities/onos-controller/onos-controller-update.component';
import { OnosControllerService } from 'app/entities/onos-controller/onos-controller.service';
import { OnosController } from 'app/shared/model/onos-controller.model';

describe('Component Tests', () => {
  describe('OnosController Management Update Component', () => {
    let comp: OnosControllerUpdateComponent;
    let fixture: ComponentFixture<OnosControllerUpdateComponent>;
    let service: OnosControllerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FirewallapiTestModule],
        declarations: [OnosControllerUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(OnosControllerUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OnosControllerUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OnosControllerService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OnosController(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OnosController();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
