package com.adrian.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A OvSPort.
 */
@Entity
@Table(name = "ov_s_port")
public class OvSPort implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "human_name")
    private String humanName;

    @Column(name = "onos_port_name")
    private String onosPortName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ov_s_port_fw_network",
               joinColumns = @JoinColumn(name = "ovsport_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "fw_network_id", referencedColumnName = "id"))
    private Set<FwNetwork> fwNetworks = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "ovSPorts", allowSetters = true)
    private OvSwitch ovSwitch;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public OvSPort humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public String getOnosPortName() {
        return onosPortName;
    }

    public OvSPort onosPortName(String onosPortName) {
        this.onosPortName = onosPortName;
        return this;
    }

    public void setOnosPortName(String onosPortName) {
        this.onosPortName = onosPortName;
    }

    public Set<FwNetwork> getFwNetworks() {
        return fwNetworks;
    }

    public OvSPort fwNetworks(Set<FwNetwork> fwNetworks) {
        this.fwNetworks = fwNetworks;
        return this;
    }

    public OvSPort addFwNetwork(FwNetwork fwNetwork) {
        this.fwNetworks.add(fwNetwork);
        fwNetwork.getOvSPorts().add(this);
        return this;
    }

    public OvSPort removeFwNetwork(FwNetwork fwNetwork) {
        this.fwNetworks.remove(fwNetwork);
        fwNetwork.getOvSPorts().remove(this);
        return this;
    }

    public void setFwNetworks(Set<FwNetwork> fwNetworks) {
        this.fwNetworks = fwNetworks;
    }

    public OvSwitch getOvSwitch() {
        return ovSwitch;
    }

    public OvSPort ovSwitch(OvSwitch ovSwitch) {
        this.ovSwitch = ovSwitch;
        return this;
    }

    public void setOvSwitch(OvSwitch ovSwitch) {
        this.ovSwitch = ovSwitch;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OvSPort)) {
            return false;
        }
        return id != null && id.equals(((OvSPort) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OvSPort{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            ", onosPortName='" + getOnosPortName() + "'" +
            "}";
    }
}
