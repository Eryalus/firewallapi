package com.adrian.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A FwFilter.
 */
@Entity
@Table(name = "fw_filter")
public class FwFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "human_name")
    private String humanName;

    @NotNull
    @Column(name = "protocol", nullable = false)
    private String protocol;

    @Column(name = "protocol_value")
    private String protocolValue;

    @NotNull
    @Column(name = "origin", nullable = false)
    private Boolean origin;

    @ManyToOne
    @JsonIgnoreProperties(value = "fwFilters", allowSetters = true)
    private FwRule fwRule;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public FwFilter humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public String getProtocol() {
        return protocol;
    }

    public FwFilter protocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocolValue() {
        return protocolValue;
    }

    public FwFilter protocolValue(String protocolValue) {
        this.protocolValue = protocolValue;
        return this;
    }

    public void setProtocolValue(String protocolValue) {
        this.protocolValue = protocolValue;
    }

    public Boolean isOrigin() {
        return origin;
    }

    public FwFilter origin(Boolean origin) {
        this.origin = origin;
        return this;
    }

    public void setOrigin(Boolean origin) {
        this.origin = origin;
    }

    public FwRule getFwRule() {
        return fwRule;
    }

    public FwFilter fwRule(FwRule fwRule) {
        this.fwRule = fwRule;
        return this;
    }

    public void setFwRule(FwRule fwRule) {
        this.fwRule = fwRule;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FwFilter)) {
            return false;
        }
        return id != null && id.equals(((FwFilter) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FwFilter{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            ", protocol='" + getProtocol() + "'" +
            ", protocolValue='" + getProtocolValue() + "'" +
            ", origin='" + isOrigin() + "'" +
            "}";
    }
}
