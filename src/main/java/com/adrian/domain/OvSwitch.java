package com.adrian.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A OvSwitch.
 */
@Entity
@Table(name = "ov_switch")
public class OvSwitch implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "human_name")
    private String humanName;

    @Column(name = "onos_device_id")
    private String onosDeviceId;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "ovSwitch")
    private Set<OvSPort> ovSPorts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public OvSwitch humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public String getOnosDeviceId() {
        return onosDeviceId;
    }

    public OvSwitch onosDeviceId(String onosDeviceId) {
        this.onosDeviceId = onosDeviceId;
        return this;
    }

    public void setOnosDeviceId(String onosDeviceId) {
        this.onosDeviceId = onosDeviceId;
    }

    public Set<OvSPort> getOvSPorts() {
        return ovSPorts;
    }

    public OvSwitch ovSPorts(Set<OvSPort> ovSPorts) {
        this.ovSPorts = ovSPorts;
        return this;
    }

    public OvSwitch addOvSPort(OvSPort ovSPort) {
        this.ovSPorts.add(ovSPort);
        ovSPort.setOvSwitch(this);
        return this;
    }

    public OvSwitch removeOvSPort(OvSPort ovSPort) {
        this.ovSPorts.remove(ovSPort);
        ovSPort.setOvSwitch(null);
        return this;
    }

    public void setOvSPorts(Set<OvSPort> ovSPorts) {
        this.ovSPorts = ovSPorts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OvSwitch)) {
            return false;
        }
        return id != null && id.equals(((OvSwitch) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OvSwitch{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            ", onosDeviceId='" + getOnosDeviceId() + "'" +
            "}";
    }
}
