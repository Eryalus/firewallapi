package com.adrian.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A FwRuleGroup.
 */
@Entity
@Table(name = "fw_rule_group")
public class FwRuleGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "human_name")
    private String humanName;

    @NotNull
    @Column(name = "priority", nullable = false)
    private Integer priority;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "fwRuleGroup")
    private Set<FwRule> fwRules = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public FwRuleGroup humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public Integer getPriority() {
        return priority;
    }

    public FwRuleGroup priority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Set<FwRule> getFwRules() {
        return fwRules;
    }

    public FwRuleGroup fwRules(Set<FwRule> fwRules) {
        this.fwRules = fwRules;
        return this;
    }

    public FwRuleGroup addFwRule(FwRule fwRule) {
        this.fwRules.add(fwRule);
        fwRule.setFwRuleGroup(this);
        return this;
    }

    public FwRuleGroup removeFwRule(FwRule fwRule) {
        this.fwRules.remove(fwRule);
        fwRule.setFwRuleGroup(null);
        return this;
    }

    public void setFwRules(Set<FwRule> fwRules) {
        this.fwRules = fwRules;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FwRuleGroup)) {
            return false;
        }
        return id != null && id.equals(((FwRuleGroup) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FwRuleGroup{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            ", priority=" + getPriority() +
            "}";
    }
}
