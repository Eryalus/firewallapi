package com.adrian.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A FwSubnet.
 */
@Entity
@Table(name = "fw_subnet")
public class FwSubnet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ip_prefix", nullable = false)
    private String ipPrefix;

    @NotNull
    @Column(name = "ip_mask", nullable = false)
    private String ipMask;

    @NotNull
    @Column(name = "allow", nullable = false)
    private Boolean allow;

    @NotNull
    @Column(name = "priority", nullable = false)
    private Integer priority;

    @ManyToOne
    @JsonIgnoreProperties(value = "fwSubnets", allowSetters = true)
    private FwNetwork fwNetwork;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpPrefix() {
        return ipPrefix;
    }

    public FwSubnet ipPrefix(String ipPrefix) {
        this.ipPrefix = ipPrefix;
        return this;
    }

    public void setIpPrefix(String ipPrefix) {
        this.ipPrefix = ipPrefix;
    }

    public String getIpMask() {
        return ipMask;
    }

    public FwSubnet ipMask(String ipMask) {
        this.ipMask = ipMask;
        return this;
    }

    public void setIpMask(String ipMask) {
        this.ipMask = ipMask;
    }

    public Boolean isAllow() {
        return allow;
    }

    public FwSubnet allow(Boolean allow) {
        this.allow = allow;
        return this;
    }

    public void setAllow(Boolean allow) {
        this.allow = allow;
    }

    public Integer getPriority() {
        return priority;
    }

    public FwSubnet priority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public FwNetwork getFwNetwork() {
        return fwNetwork;
    }

    public FwSubnet fwNetwork(FwNetwork fwNetwork) {
        this.fwNetwork = fwNetwork;
        return this;
    }

    public void setFwNetwork(FwNetwork fwNetwork) {
        this.fwNetwork = fwNetwork;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FwSubnet)) {
            return false;
        }
        return id != null && id.equals(((FwSubnet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FwSubnet{" +
            "id=" + getId() +
            ", ipPrefix='" + getIpPrefix() + "'" +
            ", ipMask='" + getIpMask() + "'" +
            ", allow='" + isAllow() + "'" +
            ", priority=" + getPriority() +
            "}";
    }
}
