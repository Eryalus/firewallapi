package com.adrian.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A FwNetwork.
 */
@Entity
@Table(name = "fw_network")
public class FwNetwork implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "human_name")
    private String humanName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "fwNetwork")
    private Set<FwSubnet> fwSubnets = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "fwNetworks")
    @JsonIgnore
    private Set<OvSPort> ovSPorts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public FwNetwork humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public Set<FwSubnet> getFwSubnets() {
        return fwSubnets;
    }

    public FwNetwork fwSubnets(Set<FwSubnet> fwSubnets) {
        this.fwSubnets = fwSubnets;
        return this;
    }

    public FwNetwork addFwSubnet(FwSubnet fwSubnet) {
        this.fwSubnets.add(fwSubnet);
        fwSubnet.setFwNetwork(this);
        return this;
    }

    public FwNetwork removeFwSubnet(FwSubnet fwSubnet) {
        this.fwSubnets.remove(fwSubnet);
        fwSubnet.setFwNetwork(null);
        return this;
    }

    public void setFwSubnets(Set<FwSubnet> fwSubnets) {
        this.fwSubnets = fwSubnets;
    }

    public Set<OvSPort> getOvSPorts() {
        return ovSPorts;
    }

    public FwNetwork ovSPorts(Set<OvSPort> ovSPorts) {
        this.ovSPorts = ovSPorts;
        return this;
    }

    public FwNetwork addOvSPort(OvSPort ovSPort) {
        this.ovSPorts.add(ovSPort);
        ovSPort.getFwNetworks().add(this);
        return this;
    }

    public FwNetwork removeOvSPort(OvSPort ovSPort) {
        this.ovSPorts.remove(ovSPort);
        ovSPort.getFwNetworks().remove(this);
        return this;
    }

    public void setOvSPorts(Set<OvSPort> ovSPorts) {
        this.ovSPorts = ovSPorts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FwNetwork)) {
            return false;
        }
        return id != null && id.equals(((FwNetwork) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FwNetwork{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            "}";
    }
}
