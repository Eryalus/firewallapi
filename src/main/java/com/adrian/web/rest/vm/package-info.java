/**
 * View Models used by Spring MVC REST controllers.
 */
package com.adrian.web.rest.vm;
