package com.adrian.web.rest;

import com.adrian.domain.OvSPort;
import com.adrian.repository.OvSPortRepository;
import com.adrian.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adrian.domain.OvSPort}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class OvSPortResource {

    private final Logger log = LoggerFactory.getLogger(OvSPortResource.class);

    private static final String ENTITY_NAME = "ovSPort";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OvSPortRepository ovSPortRepository;

    public OvSPortResource(OvSPortRepository ovSPortRepository) {
        this.ovSPortRepository = ovSPortRepository;
    }

    /**
     * {@code POST  /ov-s-ports} : Create a new ovSPort.
     *
     * @param ovSPort the ovSPort to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ovSPort, or with status {@code 400 (Bad Request)} if the ovSPort has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ov-s-ports")
    public ResponseEntity<OvSPort> createOvSPort(@RequestBody OvSPort ovSPort) throws URISyntaxException {
        log.debug("REST request to save OvSPort : {}", ovSPort);
        if (ovSPort.getId() != null) {
            throw new BadRequestAlertException("A new ovSPort cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OvSPort result = ovSPortRepository.save(ovSPort);
        return ResponseEntity.created(new URI("/api/ov-s-ports/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ov-s-ports} : Updates an existing ovSPort.
     *
     * @param ovSPort the ovSPort to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ovSPort,
     * or with status {@code 400 (Bad Request)} if the ovSPort is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ovSPort couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ov-s-ports")
    public ResponseEntity<OvSPort> updateOvSPort(@RequestBody OvSPort ovSPort) throws URISyntaxException {
        log.debug("REST request to update OvSPort : {}", ovSPort);
        if (ovSPort.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OvSPort result = ovSPortRepository.save(ovSPort);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ovSPort.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ov-s-ports} : get all the ovSPorts.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ovSPorts in body.
     */
    @GetMapping("/ov-s-ports")
    public List<OvSPort> getAllOvSPorts(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all OvSPorts");
        return ovSPortRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /ov-s-ports/:id} : get the "id" ovSPort.
     *
     * @param id the id of the ovSPort to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ovSPort, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ov-s-ports/{id}")
    public ResponseEntity<OvSPort> getOvSPort(@PathVariable Long id) {
        log.debug("REST request to get OvSPort : {}", id);
        Optional<OvSPort> ovSPort = ovSPortRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(ovSPort);
    }

    /**
     * {@code DELETE  /ov-s-ports/:id} : delete the "id" ovSPort.
     *
     * @param id the id of the ovSPort to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ov-s-ports/{id}")
    public ResponseEntity<Void> deleteOvSPort(@PathVariable Long id) {
        log.debug("REST request to delete OvSPort : {}", id);
        ovSPortRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
