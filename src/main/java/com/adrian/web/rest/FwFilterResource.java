package com.adrian.web.rest;

import com.adrian.domain.FwFilter;
import com.adrian.repository.FwFilterRepository;
import com.adrian.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adrian.domain.FwFilter}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FwFilterResource {

    private final Logger log = LoggerFactory.getLogger(FwFilterResource.class);

    private static final String ENTITY_NAME = "fwFilter";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FwFilterRepository fwFilterRepository;

    public FwFilterResource(FwFilterRepository fwFilterRepository) {
        this.fwFilterRepository = fwFilterRepository;
    }

    /**
     * {@code POST  /fw-filters} : Create a new fwFilter.
     *
     * @param fwFilter the fwFilter to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fwFilter, or with status {@code 400 (Bad Request)} if the fwFilter has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fw-filters")
    public ResponseEntity<FwFilter> createFwFilter(@Valid @RequestBody FwFilter fwFilter) throws URISyntaxException {
        log.debug("REST request to save FwFilter : {}", fwFilter);
        if (fwFilter.getId() != null) {
            throw new BadRequestAlertException("A new fwFilter cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FwFilter result = fwFilterRepository.save(fwFilter);
        return ResponseEntity.created(new URI("/api/fw-filters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fw-filters} : Updates an existing fwFilter.
     *
     * @param fwFilter the fwFilter to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fwFilter,
     * or with status {@code 400 (Bad Request)} if the fwFilter is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fwFilter couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fw-filters")
    public ResponseEntity<FwFilter> updateFwFilter(@Valid @RequestBody FwFilter fwFilter) throws URISyntaxException {
        log.debug("REST request to update FwFilter : {}", fwFilter);
        if (fwFilter.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FwFilter result = fwFilterRepository.save(fwFilter);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fwFilter.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fw-filters} : get all the fwFilters.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fwFilters in body.
     */
    @GetMapping("/fw-filters")
    public List<FwFilter> getAllFwFilters() {
        log.debug("REST request to get all FwFilters");
        return fwFilterRepository.findAll();
    }

    /**
     * {@code GET  /fw-filters/:id} : get the "id" fwFilter.
     *
     * @param id the id of the fwFilter to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fwFilter, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fw-filters/{id}")
    public ResponseEntity<FwFilter> getFwFilter(@PathVariable Long id) {
        log.debug("REST request to get FwFilter : {}", id);
        Optional<FwFilter> fwFilter = fwFilterRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fwFilter);
    }

    /**
     * {@code DELETE  /fw-filters/:id} : delete the "id" fwFilter.
     *
     * @param id the id of the fwFilter to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fw-filters/{id}")
    public ResponseEntity<Void> deleteFwFilter(@PathVariable Long id) {
        log.debug("REST request to delete FwFilter : {}", id);
        fwFilterRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
