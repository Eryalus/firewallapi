package com.adrian.web.rest;

import com.adrian.domain.FwFilter;
import com.adrian.domain.FwRule;
import com.adrian.domain.FwRuleGroup;
import com.adrian.repository.FwFilterRepository;
import com.adrian.repository.FwRuleGroupRepository;
import com.adrian.repository.FwRuleRepository;
import com.adrian.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adrian.domain.FwRule}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FwRuleResource {

    private final Logger log = LoggerFactory.getLogger(FwRuleResource.class);

    private static final String ENTITY_NAME = "fwRule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FwFilterRepository fwFilterRepository;
    private final FwRuleRepository fwRuleRepository;
    private final FwRuleGroupRepository fwRuleGroupRepository;

    public FwRuleResource(FwRuleRepository fwRuleRepository, FwRuleGroupRepository fwRuleGroupRepository, FwFilterRepository fwFilterRepository) {
        this.fwRuleRepository = fwRuleRepository;
        this.fwRuleGroupRepository = fwRuleGroupRepository;
        this.fwFilterRepository = fwFilterRepository;
    }

    /**
     * {@code POST  /fw-rules} : Create a new fwRule.
     *
     * @param fwRule the fwRule to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fwRule, or with status {@code 400 (Bad Request)} if the fwRule has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fw-rules")
    public ResponseEntity<FwRule> createFwRule(@Valid @RequestBody FwRule fwRule) throws URISyntaxException {
        log.debug("REST request to save FwRule : {}", fwRule);
        if (fwRule.getId() != null) {
            throw new BadRequestAlertException("A new fwRule cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FwRule result = fwRuleRepository.save(fwRule);
        return ResponseEntity.created(new URI("/api/fw-rules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fw-rules} : Updates an existing fwRule.
     *
     * @param fwRule the fwRule to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fwRule,
     * or with status {@code 400 (Bad Request)} if the fwRule is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fwRule couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fw-rules")
    public ResponseEntity<FwRule> updateFwRule(@Valid @RequestBody FwRule fwRule) throws URISyntaxException {
        log.debug("REST request to update FwRule : {}", fwRule);
        if (fwRule.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FwRule result = fwRuleRepository.save(fwRule);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fwRule.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fw-rules} : get all the fwRules.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fwRules in body.
     */
    @GetMapping("/fw-rules")
    public List<FwRule> getAllFwRules(@RequestParam(name = "group", required = false) Long groupID) {
        log.error(""+groupID);
        if(groupID!= null){
            Optional<FwRuleGroup> group = fwRuleGroupRepository.findById(groupID);
            List<FwRule> list = new ArrayList<>();
            FwRuleGroup fwRuleGroup = group.get();
            if(fwRuleGroup!=null){
                FwRule fwRule = new FwRule();
                fwRule.setFwRuleGroup(fwRuleGroup);
                list=fwRuleRepository.findAll(Example.of(fwRule), Sort.by(Sort.Direction.DESC, "priority"));
            }
            return list;
        }
        log.debug("REST request to get all FwRules");
        return fwRuleRepository.findAll(Sort.by(Sort.Direction.DESC, "priority"));
    }

    /**
     * {@code GET  /fw-rules/:id} : get the "id" fwRule.
     *
     * @param id the id of the fwRule to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fwRule, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fw-rules/{id}")
    public ResponseEntity<FwRule> getFwRule(@PathVariable Long id) {
        log.debug("REST request to get FwRule : {}", id);
        Optional<FwRule> fwRule = fwRuleRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fwRule);
    }

    /**
     * {@code DELETE  /fw-rules/:id} : delete the "id" fwRule.
     *
     * @param id the id of the fwRule to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fw-rules/{id}")
    public ResponseEntity<Void> deleteFwRule(@PathVariable Long id) {
        log.debug("REST request to delete FwRule : {}", id);
        Optional<FwRule> findById = fwRuleRepository.findById(id);
        FwRule fwRule = findById.get();
        if(fwRule!=null){
            for(FwFilter filter : fwRule.getFwFilters())
                fwFilterRepository.delete(filter);
        }
        fwRuleRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
