package com.adrian.web.rest;

import com.adrian.domain.OvSwitch;
import com.adrian.repository.OvSwitchRepository;
import com.adrian.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adrian.domain.OvSwitch}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class OvSwitchResource {

    private final Logger log = LoggerFactory.getLogger(OvSwitchResource.class);

    private static final String ENTITY_NAME = "ovSwitch";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OvSwitchRepository ovSwitchRepository;

    public OvSwitchResource(OvSwitchRepository ovSwitchRepository) {
        this.ovSwitchRepository = ovSwitchRepository;
    }

    /**
     * {@code POST  /ov-switches} : Create a new ovSwitch.
     *
     * @param ovSwitch the ovSwitch to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ovSwitch, or with status {@code 400 (Bad Request)} if the ovSwitch has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ov-switches")
    public ResponseEntity<OvSwitch> createOvSwitch(@RequestBody OvSwitch ovSwitch) throws URISyntaxException {
        log.debug("REST request to save OvSwitch : {}", ovSwitch);
        if (ovSwitch.getId() != null) {
            throw new BadRequestAlertException("A new ovSwitch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OvSwitch result = ovSwitchRepository.save(ovSwitch);
        return ResponseEntity.created(new URI("/api/ov-switches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ov-switches} : Updates an existing ovSwitch.
     *
     * @param ovSwitch the ovSwitch to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ovSwitch,
     * or with status {@code 400 (Bad Request)} if the ovSwitch is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ovSwitch couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ov-switches")
    public ResponseEntity<OvSwitch> updateOvSwitch(@RequestBody OvSwitch ovSwitch) throws URISyntaxException {
        log.debug("REST request to update OvSwitch : {}", ovSwitch);
        if (ovSwitch.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OvSwitch result = ovSwitchRepository.save(ovSwitch);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ovSwitch.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ov-switches} : get all the ovSwitches.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ovSwitches in body.
     */
    @GetMapping("/ov-switches")
    public List<OvSwitch> getAllOvSwitches() {
        log.debug("REST request to get all OvSwitches");
        return ovSwitchRepository.findAll();
    }

    /**
     * {@code GET  /ov-switches/:id} : get the "id" ovSwitch.
     *
     * @param id the id of the ovSwitch to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ovSwitch, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ov-switches/{id}")
    public ResponseEntity<OvSwitch> getOvSwitch(@PathVariable Long id) {
        log.debug("REST request to get OvSwitch : {}", id);
        Optional<OvSwitch> ovSwitch = ovSwitchRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(ovSwitch);
    }

    /**
     * {@code DELETE  /ov-switches/:id} : delete the "id" ovSwitch.
     *
     * @param id the id of the ovSwitch to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ov-switches/{id}")
    public ResponseEntity<Void> deleteOvSwitch(@PathVariable Long id) {
        log.debug("REST request to delete OvSwitch : {}", id);
        ovSwitchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
