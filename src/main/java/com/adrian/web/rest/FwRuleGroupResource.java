package com.adrian.web.rest;

import com.adrian.domain.FwRuleGroup;
import com.adrian.repository.FwRuleGroupRepository;
import com.adrian.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adrian.domain.FwRuleGroup}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FwRuleGroupResource {

    private final Logger log = LoggerFactory.getLogger(FwRuleGroupResource.class);

    private static final String ENTITY_NAME = "fwRuleGroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FwRuleGroupRepository fwRuleGroupRepository;

    public FwRuleGroupResource(FwRuleGroupRepository fwRuleGroupRepository) {
        this.fwRuleGroupRepository = fwRuleGroupRepository;
    }

    /**
     * {@code POST  /fw-rule-groups} : Create a new fwRuleGroup.
     *
     * @param fwRuleGroup the fwRuleGroup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fwRuleGroup, or with status {@code 400 (Bad Request)} if the fwRuleGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fw-rule-groups")
    public ResponseEntity<FwRuleGroup> createFwRuleGroup(@Valid @RequestBody FwRuleGroup fwRuleGroup) throws URISyntaxException {
        log.debug("REST request to save FwRuleGroup : {}", fwRuleGroup);
        if (fwRuleGroup.getId() != null) {
            throw new BadRequestAlertException("A new fwRuleGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FwRuleGroup result = fwRuleGroupRepository.save(fwRuleGroup);
        return ResponseEntity.created(new URI("/api/fw-rule-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fw-rule-groups} : Updates an existing fwRuleGroup.
     *
     * @param fwRuleGroup the fwRuleGroup to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fwRuleGroup,
     * or with status {@code 400 (Bad Request)} if the fwRuleGroup is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fwRuleGroup couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fw-rule-groups")
    public ResponseEntity<FwRuleGroup> updateFwRuleGroup(@Valid @RequestBody FwRuleGroup fwRuleGroup) throws URISyntaxException {
        log.debug("REST request to update FwRuleGroup : {}", fwRuleGroup);
        if (fwRuleGroup.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FwRuleGroup result = fwRuleGroupRepository.save(fwRuleGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fwRuleGroup.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fw-rule-groups} : get all the fwRuleGroups.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fwRuleGroups in body.
     */
    @GetMapping("/fw-rule-groups")
    public List<FwRuleGroup> getAllFwRuleGroups() {
        log.debug("REST request to get all FwRuleGroups");
        return fwRuleGroupRepository.findAll(Sort.by(Sort.Direction.DESC, "priority"));
    }

    /**
     * {@code GET  /fw-rule-groups/:id} : get the "id" fwRuleGroup.
     *
     * @param id the id of the fwRuleGroup to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fwRuleGroup, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fw-rule-groups/{id}")
    public ResponseEntity<FwRuleGroup> getFwRuleGroup(@PathVariable Long id) {
        log.debug("REST request to get FwRuleGroup : {}", id);
        Optional<FwRuleGroup> fwRuleGroup = fwRuleGroupRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fwRuleGroup);
    }

    /**
     * {@code DELETE  /fw-rule-groups/:id} : delete the "id" fwRuleGroup.
     *
     * @param id the id of the fwRuleGroup to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fw-rule-groups/{id}")
    public ResponseEntity<Void> deleteFwRuleGroup(@PathVariable Long id) {
        log.debug("REST request to delete FwRuleGroup : {}", id);
        fwRuleGroupRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
