package com.adrian.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.adrian.connector.onos.OnosConnector;
import com.adrian.connector.onos.entities.OnosFwNetwork;
import com.adrian.connector.onos.entities.OnosFwSubnet;
import com.adrian.connector.onos.entities.OnosOvSPort;
import com.adrian.connector.onos.entities.OnosOvSwitch;
import com.adrian.domain.FwFilter;
import com.adrian.domain.FwNetwork;
import com.adrian.domain.FwRule;
import com.adrian.domain.FwRuleGroup;
import com.adrian.domain.FwSubnet;
import com.adrian.domain.OnosController;
import com.adrian.domain.OvSPort;
import com.adrian.domain.OvSwitch;
import com.adrian.repository.FwNetworkRepository;
import com.adrian.repository.FwRuleGroupRepository;
import com.adrian.repository.FwSubnetRepository;
import com.adrian.repository.OnosControllerRepository;
import com.adrian.repository.OvSPortRepository;
import com.adrian.repository.OvSwitchRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;


/**
 * OnosActionsResource controller
 */
@RestController
@RequestMapping("/api/onos-actions")
public class OnosActionsResource {

    private final Logger log = LoggerFactory.getLogger(OnosActionsResource.class);

    @Autowired
    private FwRuleGroupRepository fwRuleGroupRepository;

    @Autowired
    private OvSwitchRepository ovSwitchRepository;

    @Autowired
    private OvSPortRepository ovSPortRepository;

    @Autowired
    private OnosControllerRepository onosControllerRepository;

    @Autowired
    private FwNetworkRepository fwNetworkRepository;

    @Autowired
    private FwSubnetRepository fwSubnetRepository;

    /**
     * POST reloadRules
     */
    @Secured("ROLE_ADMIN")
    @PostMapping("/push-rules")
    public String reloadRules() {
        List<OnosController> controllers = onosControllerRepository.findAll();
        List<FwRuleGroup> ruleGroups = fwRuleGroupRepository.findAll();
        String response = "";
        for (OnosController controller : controllers) {
            OnosConnector connector = new OnosConnector(controller.getIp(), controller.getPort(), false,
                    controller.getUser()+":"+controller.getPassword());
            try {
                response = connector.pushRules(ruleGroups);
                log.info("Sent to Controller "+controller.getHumanName());
            } catch (JsonProcessingException | RestClientException | URISyntaxException e) {
                // TODO Auto-generated catch block
                log.error(e.getMessage(), e);
                return e.getMessage();
            }
        }
        return response;
        // return connector.test();
    }

    @GetMapping("/get-rules")
    public String getRules() {
        List<OnosController> controllers = onosControllerRepository.findAll();
        OnosConnector connector = new OnosConnector(controllers.get(0).getIp(), controllers.get(0).getPort(), false,
                "onos:rocks");
        return connector.getRules();
    }

    @GetMapping("/import-topology")
    @Secured("ROLE_ADMIN")
    public String importTopology() {
        List<OnosController> controllers = onosControllerRepository.findAll();
        if(controllers.size()==0){
            return null; // TODO handle
        }
        OnosConnector connector = new OnosConnector(controllers.get(0).getIp(), controllers.get(0).getPort(), false,
        controllers.get(0).getUser()+":"+controllers.get(0).getPassword());
        String importTopology = connector.importTopology();
        ovSPortRepository.deleteAll();
        fwSubnetRepository.deleteAll();
        fwNetworkRepository.deleteAll();
        ovSwitchRepository.deleteAll();
        try {
            onosTopology readValue = new ObjectMapper().readValue(importTopology, onosTopology.class);
            List<OnosOvSwitch> switches = new ObjectMapper().readValue(readValue.topology, new TypeReference<List<OnosOvSwitch>>(){});
            HashMap<Long,OvSPort> allPorts = new HashMap<>(), processedPorts = new HashMap<>();
            for(OnosOvSwitch switchO : switches){
                for(OnosOvSPort portO : switchO.getOvSPorts()){
                    OvSPort port = new OvSPort();
                    for(OnosFwNetwork netO : portO.getFwNetworks()){
                        FwNetwork net = new FwNetwork();
                        net.setHumanName(netO.getHumanName());
                        fwNetworkRepository.save(net);
                        for(OnosFwSubnet subnetO : netO.getFwSubnets()){
                            FwSubnet subnet = new FwSubnet();
                            subnet.setFwNetwork(net);
                            subnet.setAllow(subnetO.isAllow());
                            subnet.setIpMask(subnetO.getIpMask());
                            subnet.setIpPrefix(subnetO.getIpPrefix());
                            subnet.setPriority(subnetO.getPriority());
                            fwSubnetRepository.save(subnet);
                        }
                        port.addFwNetwork(net);
                    }
                    port.setHumanName(portO.getHumanName());
                    port.setOnosPortName(portO.getOnosPortName());
                    allPorts.put(portO.getId(), port);
                }
            }
            for(OnosOvSwitch switchO : switches){ 
                OvSwitch switch1 = new OvSwitch();
                switch1.setHumanName(switchO.getHumanName());
                switch1.setOnosDeviceId(switchO.getOnosDeviceId());
                ovSwitchRepository.save(switch1);
                for(OnosOvSPort portO : switchO.getOvSPorts()){
                    OvSPort port = allPorts.get(portO.getId());
                    port.setOvSwitch(switch1);
                    OvSPort dest = allPorts.get(portO.getOvSPort());
                    if(dest!=null && !processedPorts.containsKey(portO.getId()) && !processedPorts.containsKey(portO.getOvSPort())){
                        FwNetwork net = new FwNetwork();
                        net.setHumanName("p2p "+port.getHumanName()+"<->"+dest.getHumanName());
                        fwNetworkRepository.save(net);
                        dest.addFwNetwork(net);
                        ovSPortRepository.save(dest);
                        port.addFwNetwork(net);
                        processedPorts.put(portO.getId(), port);
                        processedPorts.put(portO.getOvSPort(), dest);
                    }
                    ovSPortRepository.save(port);
                }
            }
            // ovSwitchRepository.flush();
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
           log.error(e.getMessage(), e);
        } 
        return importTopology;
    }

    public static class onosTopology{
        private String topology;

        public String getTopology() {
            return topology;
        }

        public void setTopology(String topology) {
            this.topology = topology;
        }

        
    }

}
