package com.adrian.web.rest;

import com.adrian.domain.OnosController;
import com.adrian.repository.OnosControllerRepository;
import com.adrian.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adrian.domain.OnosController}.
 */
@RestController
@RequestMapping("/api")
@Transactional
@Secured("ROLE_ADMIN")
public class OnosControllerResource {

    private final Logger log = LoggerFactory.getLogger(OnosControllerResource.class);

    private static final String ENTITY_NAME = "onosController";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OnosControllerRepository onosControllerRepository;

    public OnosControllerResource(OnosControllerRepository onosControllerRepository) {
        this.onosControllerRepository = onosControllerRepository;
    }

    /**
     * {@code POST  /onos-controllers} : Create a new onosController.
     *
     * @param onosController the onosController to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new onosController, or with status {@code 400 (Bad Request)} if the onosController has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/onos-controllers")
    public ResponseEntity<OnosController> createOnosController(@RequestBody OnosController onosController) throws URISyntaxException {
        log.debug("REST request to save OnosController : {}", onosController);
        if (onosController.getId() != null) {
            throw new BadRequestAlertException("A new onosController cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OnosController result = onosControllerRepository.save(onosController);
        return ResponseEntity.created(new URI("/api/onos-controllers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /onos-controllers} : Updates an existing onosController.
     *
     * @param onosController the onosController to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated onosController,
     * or with status {@code 400 (Bad Request)} if the onosController is not valid,
     * or with status {@code 500 (Internal Server Error)} if the onosController couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/onos-controllers")
    public ResponseEntity<OnosController> updateOnosController(@RequestBody OnosController onosController) throws URISyntaxException {
        log.debug("REST request to update OnosController : {}", onosController);
        if (onosController.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OnosController result = onosControllerRepository.save(onosController);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, onosController.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /onos-controllers} : get all the onosControllers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of onosControllers in body.
     */
    @GetMapping("/onos-controllers")
    public List<OnosController> getAllOnosControllers() {
        log.debug("REST request to get all OnosControllers");
        return onosControllerRepository.findAll();
    }

    /**
     * {@code GET  /onos-controllers/:id} : get the "id" onosController.
     *
     * @param id the id of the onosController to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the onosController, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/onos-controllers/{id}")
    public ResponseEntity<OnosController> getOnosController(@PathVariable Long id) {
        log.debug("REST request to get OnosController : {}", id);
        Optional<OnosController> onosController = onosControllerRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(onosController);
    }

    /**
     * {@code DELETE  /onos-controllers/:id} : delete the "id" onosController.
     *
     * @param id the id of the onosController to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/onos-controllers/{id}")
    public ResponseEntity<Void> deleteOnosController(@PathVariable Long id) {
        log.debug("REST request to delete OnosController : {}", id);
        onosControllerRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
