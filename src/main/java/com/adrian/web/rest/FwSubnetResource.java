package com.adrian.web.rest;

import com.adrian.domain.FwSubnet;
import com.adrian.repository.FwSubnetRepository;
import com.adrian.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adrian.domain.FwSubnet}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FwSubnetResource {

    private final Logger log = LoggerFactory.getLogger(FwSubnetResource.class);

    private static final String ENTITY_NAME = "fwSubnet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FwSubnetRepository fwSubnetRepository;

    public FwSubnetResource(FwSubnetRepository fwSubnetRepository) {
        this.fwSubnetRepository = fwSubnetRepository;
    }

    /**
     * {@code POST  /fw-subnets} : Create a new fwSubnet.
     *
     * @param fwSubnet the fwSubnet to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fwSubnet, or with status {@code 400 (Bad Request)} if the fwSubnet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fw-subnets")
    public ResponseEntity<FwSubnet> createFwSubnet(@Valid @RequestBody FwSubnet fwSubnet) throws URISyntaxException {
        log.debug("REST request to save FwSubnet : {}", fwSubnet);
        if (fwSubnet.getId() != null) {
            throw new BadRequestAlertException("A new fwSubnet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FwSubnet result = fwSubnetRepository.save(fwSubnet);
        return ResponseEntity.created(new URI("/api/fw-subnets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fw-subnets} : Updates an existing fwSubnet.
     *
     * @param fwSubnet the fwSubnet to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fwSubnet,
     * or with status {@code 400 (Bad Request)} if the fwSubnet is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fwSubnet couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fw-subnets")
    public ResponseEntity<FwSubnet> updateFwSubnet(@Valid @RequestBody FwSubnet fwSubnet) throws URISyntaxException {
        log.debug("REST request to update FwSubnet : {}", fwSubnet);
        if (fwSubnet.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FwSubnet result = fwSubnetRepository.save(fwSubnet);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fwSubnet.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fw-subnets} : get all the fwSubnets.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fwSubnets in body.
     */
    @GetMapping("/fw-subnets")
    public List<FwSubnet> getAllFwSubnets() {
        log.debug("REST request to get all FwSubnets");
        return fwSubnetRepository.findAll();
    }

    /**
     * {@code GET  /fw-subnets/:id} : get the "id" fwSubnet.
     *
     * @param id the id of the fwSubnet to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fwSubnet, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fw-subnets/{id}")
    public ResponseEntity<FwSubnet> getFwSubnet(@PathVariable Long id) {
        log.debug("REST request to get FwSubnet : {}", id);
        Optional<FwSubnet> fwSubnet = fwSubnetRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fwSubnet);
    }

    /**
     * {@code DELETE  /fw-subnets/:id} : delete the "id" fwSubnet.
     *
     * @param id the id of the fwSubnet to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fw-subnets/{id}")
    public ResponseEntity<Void> deleteFwSubnet(@PathVariable Long id) {
        log.debug("REST request to delete FwSubnet : {}", id);
        fwSubnetRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
