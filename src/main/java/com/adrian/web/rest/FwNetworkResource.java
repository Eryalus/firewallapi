package com.adrian.web.rest;

import com.adrian.domain.FwNetwork;
import com.adrian.repository.FwNetworkRepository;
import com.adrian.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.adrian.domain.FwNetwork}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FwNetworkResource {

    private final Logger log = LoggerFactory.getLogger(FwNetworkResource.class);

    private static final String ENTITY_NAME = "fwNetwork";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FwNetworkRepository fwNetworkRepository;

    public FwNetworkResource(FwNetworkRepository fwNetworkRepository) {
        this.fwNetworkRepository = fwNetworkRepository;
    }

    /**
     * {@code POST  /fw-networks} : Create a new fwNetwork.
     *
     * @param fwNetwork the fwNetwork to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fwNetwork, or with status {@code 400 (Bad Request)} if the fwNetwork has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fw-networks")
    public ResponseEntity<FwNetwork> createFwNetwork(@RequestBody FwNetwork fwNetwork) throws URISyntaxException {
        log.debug("REST request to save FwNetwork : {}", fwNetwork);
        if (fwNetwork.getId() != null) {
            throw new BadRequestAlertException("A new fwNetwork cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FwNetwork result = fwNetworkRepository.save(fwNetwork);
        return ResponseEntity.created(new URI("/api/fw-networks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fw-networks} : Updates an existing fwNetwork.
     *
     * @param fwNetwork the fwNetwork to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fwNetwork,
     * or with status {@code 400 (Bad Request)} if the fwNetwork is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fwNetwork couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fw-networks")
    public ResponseEntity<FwNetwork> updateFwNetwork(@RequestBody FwNetwork fwNetwork) throws URISyntaxException {
        log.debug("REST request to update FwNetwork : {}", fwNetwork);
        if (fwNetwork.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FwNetwork result = fwNetworkRepository.save(fwNetwork);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fwNetwork.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fw-networks} : get all the fwNetworks.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fwNetworks in body.
     */
    @GetMapping("/fw-networks")
    public List<FwNetwork> getAllFwNetworks() {
        log.debug("REST request to get all FwNetworks");
        return fwNetworkRepository.findAll();
    }

    /**
     * {@code GET  /fw-networks/:id} : get the "id" fwNetwork.
     *
     * @param id the id of the fwNetwork to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fwNetwork, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fw-networks/{id}")
    public ResponseEntity<FwNetwork> getFwNetwork(@PathVariable Long id) {
        log.debug("REST request to get FwNetwork : {}", id);
        Optional<FwNetwork> fwNetwork = fwNetworkRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fwNetwork);
    }

    /**
     * {@code DELETE  /fw-networks/:id} : delete the "id" fwNetwork.
     *
     * @param id the id of the fwNetwork to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fw-networks/{id}")
    public ResponseEntity<Void> deleteFwNetwork(@PathVariable Long id) {
        log.debug("REST request to delete FwNetwork : {}", id);
        fwNetworkRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
