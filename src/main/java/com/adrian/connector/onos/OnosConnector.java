package com.adrian.connector.onos;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.List;

import com.adrian.connector.Connector;
import com.adrian.domain.FwRuleGroup;
import com.adrian.domain.OvSwitch;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class OnosConnector extends Connector {

    protected static final String BASE_WS_NAME = "/onos/onosapi/v1";
    protected static final String RULES = "/rules";
    protected static final String TOPOLOGY = "/topology";

    /**
     * Creates ONOS Controller connector
     * 
     * @param ip          Controller ip
     * @param port        Controller port
     * @param https       true for https and false for http
     * @param credentials Onos controller credentials (user:password)
     */
    public OnosConnector(String ip, Integer port, boolean https, String credentials) {
        super((https ? "https" : "http") + "://" + ip + ":" + port,
                "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes()));
    }

    /**
     * Updates onos rules
     */
    public String pushRules(List<FwRuleGroup> ruleGroups)
            throws JsonProcessingException, RestClientException, URISyntaxException {
        String postForObject = super.postRequestTo(super.CONNECT_URL + BASE_WS_NAME + RULES, ruleGroups, String.class);
        return postForObject;
    }

    public String getRules() {
        ResponseEntity<String> response = super.getRequestTo(super.CONNECT_URL + BASE_WS_NAME + RULES,
                String.class);
        return response.getBody();
    }

    public String pushTopology(List<OvSwitch> switches)
            throws JsonProcessingException, RestClientException, URISyntaxException {
        String postForObject = super.postRequestTo(super.CONNECT_URL + BASE_WS_NAME + TOPOLOGY, switches, String.class);
        return postForObject;
    }

    protected String getTopology() {
        ResponseEntity<String> response = super.getRequestTo(super.CONNECT_URL + BASE_WS_NAME + TOPOLOGY,
                String.class);
        return response.getBody();
    }

    public String importTopology() {
        String body = getTopology();
        return body;
    }

    @Override
    public void connect() {
        // Not used, stateless
    }

    @Override
    public void disconnect() {
        // Not used, stateless
    }

}
