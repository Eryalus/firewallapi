package com.adrian.connector.onos.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A OvSwitch.
 */
public class OnosOvSwitch implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String humanName;

    private String onosDeviceId;

    private Set<OnosOvSPort> ovSPorts = new HashSet<>();


    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public OnosOvSwitch humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public String getOnosDeviceId() {
        return onosDeviceId;
    }

    public OnosOvSwitch onosDeviceId(String onosDeviceId) {
        this.onosDeviceId = onosDeviceId;
        return this;
    }

    public void setOnosDeviceId(String onosDeviceId) {
        this.onosDeviceId = onosDeviceId;
    }

    public Set<OnosOvSPort> getOvSPorts() {
        return ovSPorts;
    }

    public OnosOvSwitch ovSPorts(Set<OnosOvSPort> ovSPorts) {
        this.ovSPorts = ovSPorts;
        return this;
    }

    public OnosOvSwitch addOvSPort(OnosOvSPort ovSPort) {
        this.ovSPorts.add(ovSPort);
        ovSPort.setOvSwitch(this);
        return this;
    }

    public OnosOvSwitch removeOvSPort(OnosOvSPort ovSPort) {
        this.ovSPorts.remove(ovSPort);
        ovSPort.setOvSwitch(null);
        return this;
    }

    public void setOvSPorts(Set<OnosOvSPort> ovSPorts) {
        this.ovSPorts = ovSPorts;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OnosOvSwitch)) {
            return false;
        }
        return id != null && id.equals(((OnosOvSwitch) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OvSwitch{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            ", onosDeviceId='" + getOnosDeviceId() + "'" +
            "}";
    }
}
