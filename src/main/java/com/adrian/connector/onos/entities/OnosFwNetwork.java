package com.adrian.connector.onos.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A FwNetwork.
 */
public class OnosFwNetwork implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String humanName;

    private Set<OnosFwSubnet> fwSubnets = new HashSet<>();

    @JsonIgnore
    private Set<OnosOvSPort> ovSPorts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public OnosFwNetwork humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public Set<OnosFwSubnet> getFwSubnets() {
        return fwSubnets;
    }

    public OnosFwNetwork fwSubnets(Set<OnosFwSubnet> fwSubnets) {
        this.fwSubnets = fwSubnets;
        return this;
    }

    public OnosFwNetwork addFwSubnet(OnosFwSubnet fwSubnet) {
        this.fwSubnets.add(fwSubnet);
        fwSubnet.setFwNetwork(this);
        return this;
    }

    public OnosFwNetwork removeFwSubnet(OnosFwSubnet fwSubnet) {
        this.fwSubnets.remove(fwSubnet);
        fwSubnet.setFwNetwork(null);
        return this;
    }

    public void setFwSubnets(Set<OnosFwSubnet> fwSubnets) {
        this.fwSubnets = fwSubnets;
    }

    public Set<OnosOvSPort> getOvSPorts() {
        return ovSPorts;
    }

    public OnosFwNetwork ovSPorts(Set<OnosOvSPort> ovSPorts) {
        this.ovSPorts = ovSPorts;
        return this;
    }

    public OnosFwNetwork addOvSPort(OnosOvSPort ovSPort) {
        this.ovSPorts.add(ovSPort);
        ovSPort.getFwNetworks().add(this);
        return this;
    }

    public OnosFwNetwork removeOvSPort(OnosOvSPort ovSPort) {
        this.ovSPorts.remove(ovSPort);
        ovSPort.getFwNetworks().remove(this);
        return this;
    }

    public void setOvSPorts(Set<OnosOvSPort> ovSPorts) {
        this.ovSPorts = ovSPorts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OnosFwNetwork)) {
            return false;
        }
        return id != null && id.equals(((OnosFwNetwork) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FwNetwork{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            "}";
    }
}
