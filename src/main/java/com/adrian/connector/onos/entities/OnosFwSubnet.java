package com.adrian.connector.onos.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * A FwSubnet.
 */
public class OnosFwSubnet implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String ipPrefix;

    private String ipMask;

    private Boolean allow;

    private Integer priority;

    @JsonIgnoreProperties(value = "fwSubnets", allowSetters = true)
    private OnosFwNetwork fwNetwork;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpPrefix() {
        return ipPrefix;
    }

    public OnosFwSubnet ipPrefix(String ipPrefix) {
        this.ipPrefix = ipPrefix;
        return this;
    }

    public void setIpPrefix(String ipPrefix) {
        this.ipPrefix = ipPrefix;
    }

    public String getIpMask() {
        return ipMask;
    }

    public OnosFwSubnet ipMask(String ipMask) {
        this.ipMask = ipMask;
        return this;
    }

    public void setIpMask(String ipMask) {
        this.ipMask = ipMask;
    }

    public Boolean isAllow() {
        return allow;
    }

    public OnosFwSubnet allow(Boolean allow) {
        this.allow = allow;
        return this;
    }

    public void setAllow(Boolean allow) {
        this.allow = allow;
    }

    public Integer getPriority() {
        return priority;
    }

    public OnosFwSubnet priority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public OnosFwNetwork getFwNetwork() {
        return fwNetwork;
    }

    public OnosFwSubnet fwNetwork(OnosFwNetwork fwNetwork) {
        this.fwNetwork = fwNetwork;
        return this;
    }

    public void setFwNetwork(OnosFwNetwork fwNetwork) {
        this.fwNetwork = fwNetwork;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OnosFwSubnet)) {
            return false;
        }
        return id != null && id.equals(((OnosFwSubnet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FwSubnet{" +
            "id=" + getId() +
            ", ipPrefix='" + getIpPrefix() + "'" +
            ", ipMask='" + getIpMask() + "'" +
            ", allow='" + isAllow() + "'" +
            ", priority=" + getPriority() +
            "}";
    }
}
