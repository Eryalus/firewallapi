package com.adrian.connector.onos.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A OvSPort.
 */
public class OnosOvSPort implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String humanName;

    private String onosPortName;

    private Set<OnosFwNetwork> fwNetworks = new HashSet<>();

    @JsonIgnoreProperties(value = "ovSPorts", allowSetters = true)
    private OnosOvSwitch ovSwitch;

    private Long ovSPort;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHumanName() {
        return humanName;
    }

    public OnosOvSPort humanName(String humanName) {
        this.humanName = humanName;
        return this;
    }

    public void setHumanName(String humanName) {
        this.humanName = humanName;
    }

    public String getOnosPortName() {
        return onosPortName;
    }

    public OnosOvSPort onosPortName(String onosPortName) {
        this.onosPortName = onosPortName;
        return this;
    }

    public void setOnosPortName(String onosPortName) {
        this.onosPortName = onosPortName;
    }

    public Set<OnosFwNetwork> getFwNetworks() {
        return fwNetworks;
    }

    public OnosOvSPort fwNetworks(Set<OnosFwNetwork> fwNetworks) {
        this.fwNetworks = fwNetworks;
        return this;
    }

    public OnosOvSPort addFwNetwork(OnosFwNetwork fwNetwork) {
        this.fwNetworks.add(fwNetwork);
        fwNetwork.getOvSPorts().add(this);
        return this;
    }

    public OnosOvSPort removeFwNetwork(OnosFwNetwork fwNetwork) {
        this.fwNetworks.remove(fwNetwork);
        fwNetwork.getOvSPorts().remove(this);
        return this;
    }

    public void setFwNetworks(Set<OnosFwNetwork> fwNetworks) {
        this.fwNetworks = fwNetworks;
    }

    public OnosOvSwitch getOvSwitch() {
        return ovSwitch;
    }

    public OnosOvSPort ovSwitch(OnosOvSwitch ovSwitch) {
        this.ovSwitch = ovSwitch;
        return this;
    }

    public void setOvSwitch(OnosOvSwitch ovSwitch) {
        this.ovSwitch = ovSwitch;
    }

    public Long getOvSPort() {
        return ovSPort;
    }

    public void setOvSPort(Long ovSPort) {
        this.ovSPort = ovSPort;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OnosOvSPort)) {
            return false;
        }
        return id != null && id.equals(((OnosOvSPort) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OvSPort{" +
            "id=" + getId() +
            ", humanName='" + getHumanName() + "'" +
            ", onosPortName='" + getOnosPortName() + "'" +
            "}";
    }
}
