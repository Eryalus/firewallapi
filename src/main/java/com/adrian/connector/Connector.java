package com.adrian.connector;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

public abstract class Connector {

    protected String CONNECT_URL;
    protected String authorizationHeader;

    public Connector(String connectUrl, String authorizationHeader) {
        this.CONNECT_URL = connectUrl;
        this.authorizationHeader = authorizationHeader;
    }

    public abstract void connect();

    public abstract void disconnect();

    protected <T> ResponseEntity<T> getRequestTo(String url, Class<T> responseType) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", this.authorizationHeader);
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.GET, entity, responseType);
        return response;
    }

    protected <T> T postRequestTo(String url, Object body, Class<T> responseType){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", this.authorizationHeader);
        HttpEntity<Object> request = new HttpEntity<Object>(body, headers);
        return restTemplate.postForObject(url, request, responseType);
    }

}
