package com.adrian.repository;

import com.adrian.domain.FwSubnet;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FwSubnet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FwSubnetRepository extends JpaRepository<FwSubnet, Long> {
}
