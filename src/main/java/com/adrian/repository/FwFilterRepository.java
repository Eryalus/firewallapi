package com.adrian.repository;

import com.adrian.domain.FwFilter;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FwFilter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FwFilterRepository extends JpaRepository<FwFilter, Long> {
}
