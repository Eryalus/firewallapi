package com.adrian.repository;

import com.adrian.domain.OvSPort;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the OvSPort entity.
 */
@Repository
public interface OvSPortRepository extends JpaRepository<OvSPort, Long> {

    @Query(value = "select distinct ovSPort from OvSPort ovSPort left join fetch ovSPort.fwNetworks",
        countQuery = "select count(distinct ovSPort) from OvSPort ovSPort")
    Page<OvSPort> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct ovSPort from OvSPort ovSPort left join fetch ovSPort.fwNetworks")
    List<OvSPort> findAllWithEagerRelationships();

    @Query("select ovSPort from OvSPort ovSPort left join fetch ovSPort.fwNetworks where ovSPort.id =:id")
    Optional<OvSPort> findOneWithEagerRelationships(@Param("id") Long id);
}
