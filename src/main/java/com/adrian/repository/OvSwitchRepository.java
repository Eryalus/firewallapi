package com.adrian.repository;

import com.adrian.domain.OvSwitch;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the OvSwitch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OvSwitchRepository extends JpaRepository<OvSwitch, Long> {
}
