package com.adrian.repository;

import java.util.List;

import com.adrian.domain.FwRule;
import com.adrian.domain.FwRuleGroup;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FwRule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FwRuleRepository extends JpaRepository<FwRule, Long> {
}
