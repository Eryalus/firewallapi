package com.adrian.repository;

import com.adrian.domain.FwNetwork;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FwNetwork entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FwNetworkRepository extends JpaRepository<FwNetwork, Long> {
}
