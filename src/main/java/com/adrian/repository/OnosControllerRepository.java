package com.adrian.repository;

import com.adrian.domain.OnosController;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the OnosController entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OnosControllerRepository extends JpaRepository<OnosController, Long> {
}
