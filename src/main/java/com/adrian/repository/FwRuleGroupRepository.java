package com.adrian.repository;

import com.adrian.domain.FwRuleGroup;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FwRuleGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FwRuleGroupRepository extends JpaRepository<FwRuleGroup, Long> {
}
