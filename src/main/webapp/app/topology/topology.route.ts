import { Route } from '@angular/router';

import { TopologyComponent } from './topology.component';

export const TOPOLOGY_ROUTE: Route = {
  path: 'topology',
  component: TopologyComponent,
  data: {
    authorities: [],
    pageTitle: 'global.menu.topology',
  },
};
