import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';

import { FwRuleGroupService } from '../entities/fw-rule-group/fw-rule-group.service';
import { HttpResponse } from '@angular/common/http';
import { IFwRuleGroup } from 'app/shared/model/fw-rule-group.model';
import { IOnosController } from 'app/shared/model/onos-controller.model';
import { OnosControllerService } from '../entities/onos-controller/onos-controller.service';
import { IOvSwitch } from 'app/shared/model/ov-switch.model';
import { OvSwitchService } from '../entities/ov-switch/ov-switch.service';
import { OnosActionsService } from '../home/onos-actions.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'jhi-topology',
  templateUrl: './topology.component.html',
  styleUrls: ['topology.scss'],
})
export class TopologyComponent implements OnInit, OnDestroy {
  fwRuleGroups?: IFwRuleGroup[];
  onosControllers?: IOnosController[];
  switches?: IOvSwitch[];
  account: Account | null = null;
  authSubscription?: Subscription;
  graph = {
    nodes: [{
      id: 'empty',
      label: 'empty'
    }], links: [{
      source: '',
      target: ''
    }]
  };
  graphController = {
    color: "red",
    width: "120",
    height: "20",
    href: "onos-controller/"
  }
  graphSwitch = {
    color: "blue",
    width: "180",
    height: "20",
    href: "ov-switch/"
  }
  graphNet = {
    color: "green",
    width: "150",
    height: "20",
    href: "fw-network/"
  }

  constructor(private translate: TranslateService, private accountService: AccountService, private loginModalService: LoginModalService, protected fwRuleGroupService: FwRuleGroupService, protected onosControllerService: OnosControllerService, private ovSwitchService: OvSwitchService, private onosActionsService: OnosActionsService) { }

  trackId(index: number, item: IFwRuleGroup): number {
    return item.id!;
  }


  loadControllers(): void {
    this.onosControllerService.query().subscribe((res: HttpResponse<IOnosController[]>) => {
      this.onosControllers = res.body || [];
      this.ovSwitchService.query().subscribe((re: HttpResponse<IOvSwitch[]>) => {
        this.switches = re.body || [];
        this.loadTopology();
      });
    });
  }

  loadTopology(): void {
    const nodes = [{
      id: 'empty',
      label: 'empty',
      height: '20',
      width: '70',
      color: 'red',
      href: ''
    }];
    const links = [{
      source: '',
      target: ''
    }]
    this.onosControllers?.forEach(controller => {
      nodes.push({
        id: 'controller#' + controller.id,
        label: (controller.humanName ? controller.humanName : ''),
        height: this.graphController.height,
        width: this.graphController.width,
        color: this.graphController.color,
        href: this.graphController.href + controller.id + '/view'
      });
    });
    const addedNets: number[] = [-1];
    this.switches?.forEach(sw => {
      nodes.push({
        id: 'switch#' + sw.id,
        label: (sw.onosDeviceId ? sw.onosDeviceId : ''),
        height: this.graphSwitch.height,
        width: this.graphSwitch.width,
        color: this.graphSwitch.color,
        href: this.graphSwitch.href + sw.id + '/view'
      });
      sw.ovSPorts?.forEach(port => {
        port.fwNetworks?.forEach(net => {
          links.push({
            source: 'switch#' + sw.id,
            target: 'network#' + net.id
          });
          if (!addedNets.includes(net.id ? net.id : -1)) {
            nodes.push({
              id: 'network#' + net.id,
              label: (net.humanName ? net.humanName : ''),
              height: this.graphNet.height,
              width: this.graphNet.width,
              color: this.graphNet.color,
              href: this.graphNet.href + net.id + '/view'
            });
            addedNets.push(net.id ? net.id : -1);
          }
        })
      })
    });
    // this.networks?.forEach(network => {
    //   if(network.fwSubnets?.length === 0 && network.humanName?.startsWith('p2p')){
    //     // links.push({
    //     //   source: 'switch#'+network.ovSPorts?.entries[0].ovSwitch?.id,
    //     //   target: 'switch#'+network.ovSPorts?.entries[1].ovSwitch?.id,
    //     // });
    //     alert('switch#'+network.id);
    //   }
    // });

    nodes.splice(0, 1);
    links.splice(0, 1);
    this.graph.nodes = nodes;
    this.graph.links = links;
  }

  ngOnInit(): void {
    this.loadControllers();
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  importTopo(): void{
    this.onosActionsService.importTopo().subscribe( _ => {_;alert(this.translate.instant('global.menu.import-topo-success'));window.location.reload();}, err => {err; alert(this.translate.instant('global.menu.import-topo-err')) });
  }
}
