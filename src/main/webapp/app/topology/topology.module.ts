import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { TOPOLOGY_ROUTE } from './topology.route';
import { TopologyComponent } from './topology.component';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  imports: [FirewallapiSharedModule, RouterModule.forChild([TOPOLOGY_ROUTE]), NgxGraphModule, NgxChartsModule],
  declarations: [TopologyComponent],
})
export class FirewallapiTopologyModule {}
