import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';

import { FwRuleGroupService } from '../entities/fw-rule-group/fw-rule-group.service';
import { HttpResponse } from '@angular/common/http';
import { IFwRuleGroup } from 'app/shared/model/fw-rule-group.model';
import { IOnosController } from 'app/shared/model/onos-controller.model';
import { OnosControllerService } from '../entities/onos-controller/onos-controller.service';
import { IOvSwitch } from 'app/shared/model/ov-switch.model';
import { OvSwitchService } from '../entities/ov-switch/ov-switch.service';
import { OnosActionsService } from './onos-actions.service';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  fwRuleGroups?: IFwRuleGroup[];
  onosControllers?: IOnosController[];
  switches?: IOvSwitch[];
  account: Account | null = null;
  authSubscription?: Subscription;

  constructor(private translate: TranslateService, private accountService: AccountService, private loginModalService: LoginModalService,  protected fwRuleGroupService: FwRuleGroupService, protected onosControllerService: OnosControllerService, private ovSwitchService: OvSwitchService, private onosActionsService: OnosActionsService) {}
  
  trackId(index: number, item: IFwRuleGroup): number {
    return item.id!;
  }

  loadGroups(): void {
    this.fwRuleGroupService.query().subscribe((res: HttpResponse<IFwRuleGroup[]>) => (this.fwRuleGroups = res.body || []));
  }

  loadControllers(): void {
    this.onosControllerService.query().subscribe((res: HttpResponse<IOnosController[]>) => {this.onosControllers = res.body || [];});
  }

  ngOnInit(): void {
    this.loadGroups();
    this.loadControllers();
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  applyRules(): void{    
    this.onosActionsService.push().subscribe( _ => {_;alert(this.translate.instant('home.applied_rules'))}, err => {err; alert(this.translate.instant('home.error_apply_rules')) });
  }
}
