import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';

type EntityResponseType = HttpResponse<any>;

@Injectable({ providedIn: 'root' })
export class OnosActionsService {
  public resourceUrl = SERVER_API_URL + 'api/onos-actions';

  constructor(protected http: HttpClient) {}

  push(): Observable<EntityResponseType> {
    return this.http.post<any>(this.resourceUrl+'/push-rules', {}, { observe: 'response' });
  }

  importTopo(): Observable<EntityResponseType> {
    return this.http.get<any>(this.resourceUrl+'/import-topology', { observe: 'response' });
  }
}
