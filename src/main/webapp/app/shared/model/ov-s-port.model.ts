import { IFwNetwork } from 'app/shared/model/fw-network.model';
import { IOvSwitch } from 'app/shared/model/ov-switch.model';

export interface IOvSPort {
  id?: number;
  humanName?: string;
  onosPortName?: string;
  fwNetworks?: IFwNetwork[];
  ovSwitch?: IOvSwitch;
}

export class OvSPort implements IOvSPort {
  constructor(
    public id?: number,
    public humanName?: string,
    public onosPortName?: string,
    public fwNetworks?: IFwNetwork[],
    public ovSwitch?: IOvSwitch
  ) {}
}
