import { IOvSPort } from 'app/shared/model/ov-s-port.model';

export interface IOvSwitch {
  id?: number;
  humanName?: string;
  onosDeviceId?: string;
  ovSPorts?: IOvSPort[];
}

export class OvSwitch implements IOvSwitch {
  constructor(public id?: number, public humanName?: string, public onosDeviceId?: string, public ovSPorts?: IOvSPort[]) {}
}
