import { IFwNetwork } from 'app/shared/model/fw-network.model';

export interface IFwSubnet {
  id?: number;
  ipPrefix?: string;
  ipMask?: string;
  allow?: boolean;
  priority?: number;
  fwNetwork?: IFwNetwork;
}

export class FwSubnet implements IFwSubnet {
  constructor(
    public id?: number,
    public ipPrefix?: string,
    public ipMask?: string,
    public allow?: boolean,
    public priority?: number,
    public fwNetwork?: IFwNetwork
  ) {
    this.allow = this.allow || false;
  }
}
