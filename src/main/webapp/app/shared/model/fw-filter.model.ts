import { IFwRule } from 'app/shared/model/fw-rule.model';

export interface IFwFilter {
  id?: number;
  humanName?: string;
  protocol?: string;
  protocolValue?: string;
  origin?: boolean;
  fwRule?: IFwRule;
}

export class FwFilter implements IFwFilter {
  constructor(
    public id?: number,
    public humanName?: string,
    public protocol?: string,
    public protocolValue?: string,
    public origin?: boolean,
    public fwRule?: IFwRule
  ) {
    this.origin = this.origin || false;
  }
}
