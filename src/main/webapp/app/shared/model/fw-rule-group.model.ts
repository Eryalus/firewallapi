import { IFwRule } from 'app/shared/model/fw-rule.model';

export interface IFwRuleGroup {
  id?: number;
  humanName?: string;
  priority?: number;
  fwRules?: IFwRule[];
}

export class FwRuleGroup implements IFwRuleGroup {
  constructor(public id?: number, public humanName?: string, public priority?: number, public fwRules?: IFwRule[]) {}
}
