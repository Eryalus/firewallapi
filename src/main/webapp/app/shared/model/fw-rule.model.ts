import { IFwFilter } from 'app/shared/model/fw-filter.model';
import { IFwRuleGroup } from 'app/shared/model/fw-rule-group.model';

export interface IFwRule {
  id?: number;
  humanName?: string;
  priority?: number;
  action?: string;
  fwFilters?: IFwFilter[];
  fwRuleGroup?: IFwRuleGroup;
}

export class FwRule implements IFwRule {
  constructor(
    public id?: number,
    public humanName?: string,
    public priority?: number,
    public action?: string,
    public fwFilters?: IFwFilter[],
    public fwRuleGroup?: IFwRuleGroup
  ) {}
}
