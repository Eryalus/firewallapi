import { IFwSubnet } from 'app/shared/model/fw-subnet.model';
import { IOvSPort } from 'app/shared/model/ov-s-port.model';

export interface IFwNetwork {
  id?: number;
  humanName?: string;
  fwSubnets?: IFwSubnet[];
  ovSPorts?: IOvSPort[];
}

export class FwNetwork implements IFwNetwork {
  constructor(public id?: number, public humanName?: string, public fwSubnets?: IFwSubnet[], public ovSPorts?: IOvSPort[]) {}
}
