export interface IOnosController {
  id?: number;
  humanName?: string;
  ip?: string;
  port?: number;
  user?: string;
  password?: string;
}

export class OnosController implements IOnosController {
  constructor(
    public id?: number,
    public humanName?: string,
    public ip?: string,
    public port?: number,
    public user?: string,
    public password?: string
  ) {}
}
