import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFwRule } from 'app/shared/model/fw-rule.model';
import { FwRuleService } from './fw-rule.service';

@Component({
  templateUrl: './fw-rule-delete-dialog.component.html',
})
export class FwRuleDeleteDialogComponent {
  fwRule?: IFwRule;

  constructor(protected fwRuleService: FwRuleService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fwRuleService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fwRuleListModification');
      this.activeModal.close();
    });
  }
}
