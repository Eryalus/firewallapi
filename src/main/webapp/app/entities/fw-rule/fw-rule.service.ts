import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFwRule } from 'app/shared/model/fw-rule.model';

type EntityResponseType = HttpResponse<IFwRule>;
type EntityArrayResponseType = HttpResponse<IFwRule[]>;

@Injectable({ providedIn: 'root' })
export class FwRuleService {
  public resourceUrl = SERVER_API_URL + 'api/fw-rules';

  constructor(protected http: HttpClient) {}

  create(fwRule: IFwRule): Observable<EntityResponseType> {
    return this.http.post<IFwRule>(this.resourceUrl, fwRule, { observe: 'response' });
  }

  update(fwRule: IFwRule): Observable<EntityResponseType> {
    return this.http.put<IFwRule>(this.resourceUrl, fwRule, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFwRule>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFwRule[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
