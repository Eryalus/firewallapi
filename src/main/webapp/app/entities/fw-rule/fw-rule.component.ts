import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFwRule } from 'app/shared/model/fw-rule.model';
import { FwRuleService } from './fw-rule.service';
import { FwRuleDeleteDialogComponent } from './fw-rule-delete-dialog.component';

@Component({
  selector: 'jhi-fw-rule',
  templateUrl: './fw-rule.component.html',
})
export class FwRuleComponent implements OnInit, OnDestroy {
  fwRules?: IFwRule[];
  eventSubscriber?: Subscription;

  constructor(protected fwRuleService: FwRuleService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.fwRuleService.query().subscribe((res: HttpResponse<IFwRule[]>) => (this.fwRules = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFwRules();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFwRule): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFwRules(): void {
    this.eventSubscriber = this.eventManager.subscribe('fwRuleListModification', () => this.loadAll());
  }

  delete(fwRule: IFwRule): void {
    const modalRef = this.modalService.open(FwRuleDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fwRule = fwRule;
  }
}
