import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFwRule, FwRule } from 'app/shared/model/fw-rule.model';
import { FwRuleService } from './fw-rule.service';
import { IFwRuleGroup } from 'app/shared/model/fw-rule-group.model';
import { FwRuleGroupService } from 'app/entities/fw-rule-group/fw-rule-group.service';

@Component({
  selector: 'jhi-fw-rule-update',
  templateUrl: './fw-rule-update.component.html',
})
export class FwRuleUpdateComponent implements OnInit {
  isSaving = false;
  fwrulegroups: IFwRuleGroup[] = [];
  groupId: number | null = null;
  group: IFwRuleGroup | undefined = undefined;

  editForm = this.fb.group({
    id: [],
    humanName: [],
    priority: [null, [Validators.required]],
    action: [],
    fwRuleGroup: [],
  });

  constructor(
    protected fwRuleService: FwRuleService,
    protected fwRuleGroupService: FwRuleGroupService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.groupId = Number(params['group']);
      this.activatedRoute.data.subscribe(({ fwRule }) => {
        this.fwRuleGroupService.query().subscribe((res: HttpResponse<IFwRuleGroup[]>) => {
          this.fwrulegroups = res.body || [];
          this.fwrulegroups.forEach(group => {
            if (group.id === this.groupId) {
              this.group = group;
            }
          });
        });
        this.updateForm(fwRule);
      });
    });
  }

  updateForm(fwRule: IFwRule): void {
    this.editForm.patchValue({
      id: fwRule.id,
      humanName: fwRule.humanName,
      priority: fwRule.priority,
      action: fwRule.action,
      fwRuleGroup: fwRule.fwRuleGroup,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fwRule = this.createFromForm();
    if (fwRule.id !== undefined) {
      this.subscribeToSaveResponse(this.fwRuleService.update(fwRule));
    } else {
      this.subscribeToSaveResponse(this.fwRuleService.create(fwRule));
    }
  }

  private createFromForm(): IFwRule {
    return {
      ...new FwRule(),
      id: this.editForm.get(['id'])!.value,
      humanName: this.editForm.get(['humanName'])!.value,
      priority: this.editForm.get(['priority'])!.value,
      action: this.editForm.get(['action'])!.value,
      fwRuleGroup: this.editForm.get(['fwRuleGroup'])!.value ?? this.group,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFwRule>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IFwRuleGroup): any {
    return item.id;
  }
}
