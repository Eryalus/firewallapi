import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IFwFilter } from 'app/shared/model/fw-filter.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFwRule } from 'app/shared/model/fw-rule.model';
import { FwFilterDeleteDialogComponent } from '../fw-filter/fw-filter-delete-dialog.component';

@Component({
  selector: 'jhi-fw-rule-detail',
  templateUrl: './fw-rule-detail.component.html',
})
export class FwRuleDetailComponent implements OnInit {
  fwRule: IFwRule | null = null;

  constructor(protected activatedRoute: ActivatedRoute, protected modalService: NgbModal) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fwRule }) => (this.fwRule = fwRule));
  }

  previousState(): void {
    window.history.back();
  }

  deleteFilter(fwFilter: IFwFilter): void {
    const modalRef = this.modalService.open(FwFilterDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fwFilter = fwFilter;
    modalRef.result.then(() => {
      window.location.reload();
    }, () => {
      // on dismiss
    });
  }
}
