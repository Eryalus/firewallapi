import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { FwRuleComponent } from './fw-rule.component';
import { FwRuleDetailComponent } from './fw-rule-detail.component';
import { FwRuleUpdateComponent } from './fw-rule-update.component';
import { FwRuleDeleteDialogComponent } from './fw-rule-delete-dialog.component';
import { fwRuleRoute } from './fw-rule.route';

@NgModule({
  imports: [FirewallapiSharedModule, RouterModule.forChild(fwRuleRoute)],
  declarations: [FwRuleComponent, FwRuleDetailComponent, FwRuleUpdateComponent, FwRuleDeleteDialogComponent],
  entryComponents: [FwRuleDeleteDialogComponent],
})
export class FirewallapiFwRuleModule {}
