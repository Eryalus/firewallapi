import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFwRule, FwRule } from 'app/shared/model/fw-rule.model';
import { FwRuleService } from './fw-rule.service';
import { FwRuleComponent } from './fw-rule.component';
import { FwRuleDetailComponent } from './fw-rule-detail.component';
import { FwRuleUpdateComponent } from './fw-rule-update.component';

@Injectable({ providedIn: 'root' })
export class FwRuleResolve implements Resolve<IFwRule> {
  constructor(private service: FwRuleService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFwRule> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fwRule: HttpResponse<FwRule>) => {
          if (fwRule.body) {
            return of(fwRule.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FwRule());
  }
}

export const fwRuleRoute: Routes = [
  {
    path: '',
    component: FwRuleComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwRule.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FwRuleDetailComponent,
    resolve: {
      fwRule: FwRuleResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwRule.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FwRuleUpdateComponent,
    resolve: {
      fwRule: FwRuleResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwRule.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FwRuleUpdateComponent,
    resolve: {
      fwRule: FwRuleResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwRule.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
