import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOvSPort } from 'app/shared/model/ov-s-port.model';

@Component({
  selector: 'jhi-ov-s-port-detail',
  templateUrl: './ov-s-port-detail.component.html',
})
export class OvSPortDetailComponent implements OnInit {
  ovSPort: IOvSPort | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ovSPort }) => (this.ovSPort = ovSPort));
  }

  previousState(): void {
    window.history.back();
  }
}
