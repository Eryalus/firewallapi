import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOvSPort } from 'app/shared/model/ov-s-port.model';
import { OvSPortService } from './ov-s-port.service';

@Component({
  templateUrl: './ov-s-port-delete-dialog.component.html',
})
export class OvSPortDeleteDialogComponent {
  ovSPort?: IOvSPort;

  constructor(protected ovSPortService: OvSPortService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.ovSPortService.delete(id).subscribe(() => {
      this.eventManager.broadcast('ovSPortListModification');
      this.activeModal.close();
    });
  }
}
