import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOvSPort, OvSPort } from 'app/shared/model/ov-s-port.model';
import { OvSPortService } from './ov-s-port.service';
import { OvSPortComponent } from './ov-s-port.component';
import { OvSPortDetailComponent } from './ov-s-port-detail.component';
import { OvSPortUpdateComponent } from './ov-s-port-update.component';

@Injectable({ providedIn: 'root' })
export class OvSPortResolve implements Resolve<IOvSPort> {
  constructor(private service: OvSPortService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOvSPort> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((ovSPort: HttpResponse<OvSPort>) => {
          if (ovSPort.body) {
            return of(ovSPort.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OvSPort());
  }
}

export const ovSPortRoute: Routes = [
  {
    path: '',
    component: OvSPortComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.ovSPort.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OvSPortDetailComponent,
    resolve: {
      ovSPort: OvSPortResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.ovSPort.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OvSPortUpdateComponent,
    resolve: {
      ovSPort: OvSPortResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.ovSPort.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OvSPortUpdateComponent,
    resolve: {
      ovSPort: OvSPortResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.ovSPort.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
