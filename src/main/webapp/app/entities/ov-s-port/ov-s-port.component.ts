import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOvSPort } from 'app/shared/model/ov-s-port.model';
import { OvSPortService } from './ov-s-port.service';
import { OvSPortDeleteDialogComponent } from './ov-s-port-delete-dialog.component';

@Component({
  selector: 'jhi-ov-s-port',
  templateUrl: './ov-s-port.component.html',
})
export class OvSPortComponent implements OnInit, OnDestroy {
  ovSPorts?: IOvSPort[];
  eventSubscriber?: Subscription;

  constructor(protected ovSPortService: OvSPortService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.ovSPortService.query().subscribe((res: HttpResponse<IOvSPort[]>) => (this.ovSPorts = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInOvSPorts();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOvSPort): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOvSPorts(): void {
    this.eventSubscriber = this.eventManager.subscribe('ovSPortListModification', () => this.loadAll());
  }

  delete(ovSPort: IOvSPort): void {
    const modalRef = this.modalService.open(OvSPortDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.ovSPort = ovSPort;
  }
}
