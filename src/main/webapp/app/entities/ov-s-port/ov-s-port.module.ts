import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { OvSPortComponent } from './ov-s-port.component';
import { OvSPortDetailComponent } from './ov-s-port-detail.component';
import { OvSPortUpdateComponent } from './ov-s-port-update.component';
import { OvSPortDeleteDialogComponent } from './ov-s-port-delete-dialog.component';
import { ovSPortRoute } from './ov-s-port.route';

@NgModule({
  imports: [FirewallapiSharedModule, RouterModule.forChild(ovSPortRoute)],
  declarations: [OvSPortComponent, OvSPortDetailComponent, OvSPortUpdateComponent, OvSPortDeleteDialogComponent],
  entryComponents: [OvSPortDeleteDialogComponent],
})
export class FirewallapiOvSPortModule {}
