import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOvSPort } from 'app/shared/model/ov-s-port.model';

type EntityResponseType = HttpResponse<IOvSPort>;
type EntityArrayResponseType = HttpResponse<IOvSPort[]>;

@Injectable({ providedIn: 'root' })
export class OvSPortService {
  public resourceUrl = SERVER_API_URL + 'api/ov-s-ports';

  constructor(protected http: HttpClient) {}

  create(ovSPort: IOvSPort): Observable<EntityResponseType> {
    return this.http.post<IOvSPort>(this.resourceUrl, ovSPort, { observe: 'response' });
  }

  update(ovSPort: IOvSPort): Observable<EntityResponseType> {
    return this.http.put<IOvSPort>(this.resourceUrl, ovSPort, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOvSPort>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOvSPort[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
