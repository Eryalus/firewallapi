import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOvSPort, OvSPort } from 'app/shared/model/ov-s-port.model';
import { OvSPortService } from './ov-s-port.service';
import { IFwNetwork } from 'app/shared/model/fw-network.model';
import { FwNetworkService } from 'app/entities/fw-network/fw-network.service';
import { IOvSwitch } from 'app/shared/model/ov-switch.model';
import { OvSwitchService } from 'app/entities/ov-switch/ov-switch.service';

type SelectableEntity = IFwNetwork | IOvSwitch;

@Component({
  selector: 'jhi-ov-s-port-update',
  templateUrl: './ov-s-port-update.component.html',
})
export class OvSPortUpdateComponent implements OnInit {
  isSaving = false;
  fwnetworks: IFwNetwork[] = [];
  ovswitches: IOvSwitch[] = [];

  editForm = this.fb.group({
    id: [],
    humanName: [],
    onosPortName: [],
    fwNetworks: [],
    ovSwitch: [],
  });

  constructor(
    protected ovSPortService: OvSPortService,
    protected fwNetworkService: FwNetworkService,
    protected ovSwitchService: OvSwitchService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ovSPort }) => {
      this.updateForm(ovSPort);

      this.fwNetworkService.query().subscribe((res: HttpResponse<IFwNetwork[]>) => (this.fwnetworks = res.body || []));

      this.ovSwitchService.query().subscribe((res: HttpResponse<IOvSwitch[]>) => (this.ovswitches = res.body || []));
    });
  }

  updateForm(ovSPort: IOvSPort): void {
    this.editForm.patchValue({
      id: ovSPort.id,
      humanName: ovSPort.humanName,
      onosPortName: ovSPort.onosPortName,
      fwNetworks: ovSPort.fwNetworks,
      ovSwitch: ovSPort.ovSwitch,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ovSPort = this.createFromForm();
    if (ovSPort.id !== undefined) {
      this.subscribeToSaveResponse(this.ovSPortService.update(ovSPort));
    } else {
      this.subscribeToSaveResponse(this.ovSPortService.create(ovSPort));
    }
  }

  private createFromForm(): IOvSPort {
    return {
      ...new OvSPort(),
      id: this.editForm.get(['id'])!.value,
      humanName: this.editForm.get(['humanName'])!.value,
      onosPortName: this.editForm.get(['onosPortName'])!.value,
      fwNetworks: this.editForm.get(['fwNetworks'])!.value,
      ovSwitch: this.editForm.get(['ovSwitch'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOvSPort>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IFwNetwork[], option: IFwNetwork): IFwNetwork {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
