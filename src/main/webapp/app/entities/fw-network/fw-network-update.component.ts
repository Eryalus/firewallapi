import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFwNetwork, FwNetwork } from 'app/shared/model/fw-network.model';
import { FwNetworkService } from './fw-network.service';

@Component({
  selector: 'jhi-fw-network-update',
  templateUrl: './fw-network-update.component.html',
})
export class FwNetworkUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    humanName: [],
  });

  constructor(protected fwNetworkService: FwNetworkService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fwNetwork }) => {
      this.updateForm(fwNetwork);
    });
  }

  updateForm(fwNetwork: IFwNetwork): void {
    this.editForm.patchValue({
      id: fwNetwork.id,
      humanName: fwNetwork.humanName,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fwNetwork = this.createFromForm();
    if (fwNetwork.id !== undefined) {
      this.subscribeToSaveResponse(this.fwNetworkService.update(fwNetwork));
    } else {
      this.subscribeToSaveResponse(this.fwNetworkService.create(fwNetwork));
    }
  }

  private createFromForm(): IFwNetwork {
    return {
      ...new FwNetwork(),
      id: this.editForm.get(['id'])!.value,
      humanName: this.editForm.get(['humanName'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFwNetwork>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
