import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFwNetwork } from 'app/shared/model/fw-network.model';

type EntityResponseType = HttpResponse<IFwNetwork>;
type EntityArrayResponseType = HttpResponse<IFwNetwork[]>;

@Injectable({ providedIn: 'root' })
export class FwNetworkService {
  public resourceUrl = SERVER_API_URL + 'api/fw-networks';

  constructor(protected http: HttpClient) {}

  create(fwNetwork: IFwNetwork): Observable<EntityResponseType> {
    return this.http.post<IFwNetwork>(this.resourceUrl, fwNetwork, { observe: 'response' });
  }

  update(fwNetwork: IFwNetwork): Observable<EntityResponseType> {
    return this.http.put<IFwNetwork>(this.resourceUrl, fwNetwork, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFwNetwork>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFwNetwork[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
