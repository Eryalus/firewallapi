import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFwNetwork } from 'app/shared/model/fw-network.model';
import { FwNetworkService } from './fw-network.service';

@Component({
  templateUrl: './fw-network-delete-dialog.component.html',
})
export class FwNetworkDeleteDialogComponent {
  fwNetwork?: IFwNetwork;

  constructor(protected fwNetworkService: FwNetworkService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fwNetworkService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fwNetworkListModification');
      this.activeModal.close();
    });
  }
}
