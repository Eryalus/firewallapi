import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { FwNetworkComponent } from './fw-network.component';
import { FwNetworkDetailComponent } from './fw-network-detail.component';
import { FwNetworkUpdateComponent } from './fw-network-update.component';
import { FwNetworkDeleteDialogComponent } from './fw-network-delete-dialog.component';
import { fwNetworkRoute } from './fw-network.route';

@NgModule({
  imports: [FirewallapiSharedModule, RouterModule.forChild(fwNetworkRoute)],
  declarations: [FwNetworkComponent, FwNetworkDetailComponent, FwNetworkUpdateComponent, FwNetworkDeleteDialogComponent],
  entryComponents: [FwNetworkDeleteDialogComponent],
})
export class FirewallapiFwNetworkModule {}
