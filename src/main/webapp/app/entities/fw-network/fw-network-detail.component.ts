import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFwNetwork } from 'app/shared/model/fw-network.model';

@Component({
  selector: 'jhi-fw-network-detail',
  templateUrl: './fw-network-detail.component.html',
})
export class FwNetworkDetailComponent implements OnInit {
  fwNetwork: IFwNetwork | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fwNetwork }) => (this.fwNetwork = fwNetwork));
  }

  previousState(): void {
    window.history.back();
  }
}
