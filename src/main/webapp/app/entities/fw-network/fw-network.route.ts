import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFwNetwork, FwNetwork } from 'app/shared/model/fw-network.model';
import { FwNetworkService } from './fw-network.service';
import { FwNetworkComponent } from './fw-network.component';
import { FwNetworkDetailComponent } from './fw-network-detail.component';
import { FwNetworkUpdateComponent } from './fw-network-update.component';

@Injectable({ providedIn: 'root' })
export class FwNetworkResolve implements Resolve<IFwNetwork> {
  constructor(private service: FwNetworkService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFwNetwork> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fwNetwork: HttpResponse<FwNetwork>) => {
          if (fwNetwork.body) {
            return of(fwNetwork.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FwNetwork());
  }
}

export const fwNetworkRoute: Routes = [
  {
    path: '',
    component: FwNetworkComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwNetwork.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FwNetworkDetailComponent,
    resolve: {
      fwNetwork: FwNetworkResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwNetwork.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FwNetworkUpdateComponent,
    resolve: {
      fwNetwork: FwNetworkResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwNetwork.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FwNetworkUpdateComponent,
    resolve: {
      fwNetwork: FwNetworkResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwNetwork.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
