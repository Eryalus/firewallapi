import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFwNetwork } from 'app/shared/model/fw-network.model';
import { FwNetworkService } from './fw-network.service';
import { FwNetworkDeleteDialogComponent } from './fw-network-delete-dialog.component';

@Component({
  selector: 'jhi-fw-network',
  templateUrl: './fw-network.component.html',
})
export class FwNetworkComponent implements OnInit, OnDestroy {
  fwNetworks?: IFwNetwork[];
  eventSubscriber?: Subscription;

  constructor(protected fwNetworkService: FwNetworkService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.fwNetworkService.query().subscribe((res: HttpResponse<IFwNetwork[]>) => (this.fwNetworks = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFwNetworks();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFwNetwork): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFwNetworks(): void {
    this.eventSubscriber = this.eventManager.subscribe('fwNetworkListModification', () => this.loadAll());
  }

  delete(fwNetwork: IFwNetwork): void {
    const modalRef = this.modalService.open(FwNetworkDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fwNetwork = fwNetwork;
  }
}
