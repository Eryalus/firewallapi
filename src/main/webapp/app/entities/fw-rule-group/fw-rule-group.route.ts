import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFwRuleGroup, FwRuleGroup } from 'app/shared/model/fw-rule-group.model';
import { FwRuleGroupService } from './fw-rule-group.service';
import { FwRuleGroupComponent } from './fw-rule-group.component';
import { FwRuleGroupDetailComponent } from './fw-rule-group-detail.component';
import { FwRuleGroupUpdateComponent } from './fw-rule-group-update.component';

@Injectable({ providedIn: 'root' })
export class FwRuleGroupResolve implements Resolve<IFwRuleGroup> {
  constructor(private service: FwRuleGroupService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFwRuleGroup> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fwRuleGroup: HttpResponse<FwRuleGroup>) => {
          if (fwRuleGroup.body) {
            return of(fwRuleGroup.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FwRuleGroup());
  }
}

export const fwRuleGroupRoute: Routes = [
  {
    path: '',
    component: FwRuleGroupComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwRuleGroup.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FwRuleGroupDetailComponent,
    resolve: {
      fwRuleGroup: FwRuleGroupResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwRuleGroup.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FwRuleGroupUpdateComponent,
    resolve: {
      fwRuleGroup: FwRuleGroupResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwRuleGroup.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FwRuleGroupUpdateComponent,
    resolve: {
      fwRuleGroup: FwRuleGroupResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwRuleGroup.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
