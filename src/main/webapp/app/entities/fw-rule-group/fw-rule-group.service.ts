import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFwRuleGroup } from 'app/shared/model/fw-rule-group.model';

type EntityResponseType = HttpResponse<IFwRuleGroup>;
type EntityArrayResponseType = HttpResponse<IFwRuleGroup[]>;

@Injectable({ providedIn: 'root' })
export class FwRuleGroupService {
  public resourceUrl = SERVER_API_URL + 'api/fw-rule-groups';

  constructor(protected http: HttpClient) {}

  create(fwRuleGroup: IFwRuleGroup): Observable<EntityResponseType> {
    return this.http.post<IFwRuleGroup>(this.resourceUrl, fwRuleGroup, { observe: 'response' });
  }

  update(fwRuleGroup: IFwRuleGroup): Observable<EntityResponseType> {
    return this.http.put<IFwRuleGroup>(this.resourceUrl, fwRuleGroup, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFwRuleGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFwRuleGroup[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
