import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFwRuleGroup, FwRuleGroup } from 'app/shared/model/fw-rule-group.model';
import { FwRuleGroupService } from './fw-rule-group.service';

@Component({
  selector: 'jhi-fw-rule-group-update',
  templateUrl: './fw-rule-group-update.component.html',
})
export class FwRuleGroupUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    humanName: [],
    priority: [null, [Validators.required]],
  });

  constructor(protected fwRuleGroupService: FwRuleGroupService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fwRuleGroup }) => {
      this.updateForm(fwRuleGroup);
    });
  }

  updateForm(fwRuleGroup: IFwRuleGroup): void {
    this.editForm.patchValue({
      id: fwRuleGroup.id,
      humanName: fwRuleGroup.humanName,
      priority: fwRuleGroup.priority,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fwRuleGroup = this.createFromForm();
    if (fwRuleGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.fwRuleGroupService.update(fwRuleGroup));
    } else {
      this.subscribeToSaveResponse(this.fwRuleGroupService.create(fwRuleGroup));
    }
  }

  private createFromForm(): IFwRuleGroup {
    return {
      ...new FwRuleGroup(),
      id: this.editForm.get(['id'])!.value,
      humanName: this.editForm.get(['humanName'])!.value,
      priority: this.editForm.get(['priority'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFwRuleGroup>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
