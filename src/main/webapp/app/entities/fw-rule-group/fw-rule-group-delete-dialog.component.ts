import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFwRuleGroup } from 'app/shared/model/fw-rule-group.model';
import { FwRuleGroupService } from './fw-rule-group.service';

@Component({
  templateUrl: './fw-rule-group-delete-dialog.component.html',
})
export class FwRuleGroupDeleteDialogComponent {
  fwRuleGroup?: IFwRuleGroup;

  constructor(
    protected fwRuleGroupService: FwRuleGroupService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fwRuleGroupService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fwRuleGroupListModification');
      this.activeModal.close();
    });
  }
}
