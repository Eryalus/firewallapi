import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

import { IFwRuleGroup } from 'app/shared/model/fw-rule-group.model';
import { IFwRule } from 'app/shared/model/fw-rule.model';
import { FwRuleService } from '../fw-rule/fw-rule.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwRuleDeleteDialogComponent } from '../fw-rule/fw-rule-delete-dialog.component';
@Component({
  selector: 'jhi-fw-rule-group-detail',
  templateUrl: './fw-rule-group-detail.component.html',
})
export class FwRuleGroupDetailComponent implements OnInit {
  fwRules?: IFwRule[];
  fwRuleGroup: IFwRuleGroup | null = null;

  constructor(protected activatedRoute: ActivatedRoute, protected fwRuleService: FwRuleService, protected modalService: NgbModal) {}

  loadRules(): void {
    this.fwRuleService.query({group:this.fwRuleGroup?.id}).subscribe((res: HttpResponse<IFwRule[]>) => (this.fwRules = res.body || []));
  }
  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fwRuleGroup }) => (this.fwRuleGroup = fwRuleGroup));
    this.loadRules();
  }

  previousState(): void {
    window.history.back();
  }

  deleteRule(fwRule: IFwRule): void {
    const modalRef = this.modalService.open(FwRuleDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fwRule = fwRule;
    modalRef.result.then(() => {
      window.location.reload();
    }, () => {
      // on dismiss
    });
  }
}
