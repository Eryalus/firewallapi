import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFwRuleGroup } from 'app/shared/model/fw-rule-group.model';
import { FwRuleGroupService } from './fw-rule-group.service';
import { FwRuleGroupDeleteDialogComponent } from './fw-rule-group-delete-dialog.component';

@Component({
  selector: 'jhi-fw-rule-group',
  templateUrl: './fw-rule-group.component.html',
})
export class FwRuleGroupComponent implements OnInit, OnDestroy {
  fwRuleGroups?: IFwRuleGroup[];
  eventSubscriber?: Subscription;

  constructor(
    protected fwRuleGroupService: FwRuleGroupService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.fwRuleGroupService.query().subscribe((res: HttpResponse<IFwRuleGroup[]>) => (this.fwRuleGroups = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFwRuleGroups();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFwRuleGroup): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFwRuleGroups(): void {
    this.eventSubscriber = this.eventManager.subscribe('fwRuleGroupListModification', () => this.loadAll());
  }

  delete(fwRuleGroup: IFwRuleGroup): void {
    const modalRef = this.modalService.open(FwRuleGroupDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fwRuleGroup = fwRuleGroup;
  }
}
