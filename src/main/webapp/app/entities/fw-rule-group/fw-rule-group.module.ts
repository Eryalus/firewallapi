import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { FwRuleGroupComponent } from './fw-rule-group.component';
import { FwRuleGroupDetailComponent } from './fw-rule-group-detail.component';
import { FwRuleGroupUpdateComponent } from './fw-rule-group-update.component';
import { FwRuleGroupDeleteDialogComponent } from './fw-rule-group-delete-dialog.component';
import { fwRuleGroupRoute } from './fw-rule-group.route';

@NgModule({
  imports: [FirewallapiSharedModule, RouterModule.forChild(fwRuleGroupRoute)],
  declarations: [FwRuleGroupComponent, FwRuleGroupDetailComponent, FwRuleGroupUpdateComponent, FwRuleGroupDeleteDialogComponent],
  entryComponents: [FwRuleGroupDeleteDialogComponent],
})
export class FirewallapiFwRuleGroupModule {}
