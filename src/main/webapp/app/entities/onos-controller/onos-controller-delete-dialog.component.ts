import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOnosController } from 'app/shared/model/onos-controller.model';
import { OnosControllerService } from './onos-controller.service';

@Component({
  templateUrl: './onos-controller-delete-dialog.component.html',
})
export class OnosControllerDeleteDialogComponent {
  onosController?: IOnosController;

  constructor(
    protected onosControllerService: OnosControllerService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.onosControllerService.delete(id).subscribe(() => {
      this.eventManager.broadcast('onosControllerListModification');
      this.activeModal.close();
    });
  }
}
