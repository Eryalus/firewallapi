import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { OnosControllerComponent } from './onos-controller.component';
import { OnosControllerDetailComponent } from './onos-controller-detail.component';
import { OnosControllerUpdateComponent } from './onos-controller-update.component';
import { OnosControllerDeleteDialogComponent } from './onos-controller-delete-dialog.component';
import { onosControllerRoute } from './onos-controller.route';

@NgModule({
  imports: [FirewallapiSharedModule, RouterModule.forChild(onosControllerRoute)],
  declarations: [
    OnosControllerComponent,
    OnosControllerDetailComponent,
    OnosControllerUpdateComponent,
    OnosControllerDeleteDialogComponent,
  ],
  entryComponents: [OnosControllerDeleteDialogComponent],
})
export class FirewallapiOnosControllerModule {}
