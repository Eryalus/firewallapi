import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOnosController } from 'app/shared/model/onos-controller.model';

type EntityResponseType = HttpResponse<IOnosController>;
type EntityArrayResponseType = HttpResponse<IOnosController[]>;

@Injectable({ providedIn: 'root' })
export class OnosControllerService {
  public resourceUrl = SERVER_API_URL + 'api/onos-controllers';

  constructor(protected http: HttpClient) {}

  create(onosController: IOnosController): Observable<EntityResponseType> {
    return this.http.post<IOnosController>(this.resourceUrl, onosController, { observe: 'response' });
  }

  update(onosController: IOnosController): Observable<EntityResponseType> {
    return this.http.put<IOnosController>(this.resourceUrl, onosController, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOnosController>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOnosController[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
