import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOnosController } from 'app/shared/model/onos-controller.model';

@Component({
  selector: 'jhi-onos-controller-detail',
  templateUrl: './onos-controller-detail.component.html',
})
export class OnosControllerDetailComponent implements OnInit {
  onosController: IOnosController | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ onosController }) => (this.onosController = onosController));
  }

  previousState(): void {
    window.history.back();
  }
}
