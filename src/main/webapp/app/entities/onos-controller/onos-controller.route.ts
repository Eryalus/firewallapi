import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOnosController, OnosController } from 'app/shared/model/onos-controller.model';
import { OnosControllerService } from './onos-controller.service';
import { OnosControllerComponent } from './onos-controller.component';
import { OnosControllerDetailComponent } from './onos-controller-detail.component';
import { OnosControllerUpdateComponent } from './onos-controller-update.component';

@Injectable({ providedIn: 'root' })
export class OnosControllerResolve implements Resolve<IOnosController> {
  constructor(private service: OnosControllerService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOnosController> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((onosController: HttpResponse<OnosController>) => {
          if (onosController.body) {
            return of(onosController.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OnosController());
  }
}

export const onosControllerRoute: Routes = [
  {
    path: '',
    component: OnosControllerComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.onosController.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OnosControllerDetailComponent,
    resolve: {
      onosController: OnosControllerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.onosController.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OnosControllerUpdateComponent,
    resolve: {
      onosController: OnosControllerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.onosController.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OnosControllerUpdateComponent,
    resolve: {
      onosController: OnosControllerResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.onosController.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
