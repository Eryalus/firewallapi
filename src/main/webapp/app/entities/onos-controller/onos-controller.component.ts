import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOnosController } from 'app/shared/model/onos-controller.model';
import { OnosControllerService } from './onos-controller.service';
import { OnosControllerDeleteDialogComponent } from './onos-controller-delete-dialog.component';

@Component({
  selector: 'jhi-onos-controller',
  templateUrl: './onos-controller.component.html',
})
export class OnosControllerComponent implements OnInit, OnDestroy {
  onosControllers?: IOnosController[];
  eventSubscriber?: Subscription;

  constructor(
    protected onosControllerService: OnosControllerService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.onosControllerService.query().subscribe((res: HttpResponse<IOnosController[]>) => (this.onosControllers = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInOnosControllers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOnosController): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOnosControllers(): void {
    this.eventSubscriber = this.eventManager.subscribe('onosControllerListModification', () => this.loadAll());
  }

  delete(onosController: IOnosController): void {
    const modalRef = this.modalService.open(OnosControllerDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.onosController = onosController;
  }
}
