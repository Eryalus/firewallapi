import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOnosController, OnosController } from 'app/shared/model/onos-controller.model';
import { OnosControllerService } from './onos-controller.service';

@Component({
  selector: 'jhi-onos-controller-update',
  templateUrl: './onos-controller-update.component.html',
})
export class OnosControllerUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    humanName: [],
    ip: [],
    port: [],
    user: [],
    password: [],
  });

  constructor(protected onosControllerService: OnosControllerService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ onosController }) => {
      this.updateForm(onosController);
    });
  }

  updateForm(onosController: IOnosController): void {
    this.editForm.patchValue({
      id: onosController.id,
      humanName: onosController.humanName,
      ip: onosController.ip,
      port: onosController.port,
      user: onosController.user,
      password: onosController.password,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const onosController = this.createFromForm();
    if (onosController.id !== undefined) {
      this.subscribeToSaveResponse(this.onosControllerService.update(onosController));
    } else {
      this.subscribeToSaveResponse(this.onosControllerService.create(onosController));
    }
  }

  private createFromForm(): IOnosController {
    return {
      ...new OnosController(),
      id: this.editForm.get(['id'])!.value,
      humanName: this.editForm.get(['humanName'])!.value,
      ip: this.editForm.get(['ip'])!.value,
      port: this.editForm.get(['port'])!.value,
      user: this.editForm.get(['user'])!.value,
      password: this.editForm.get(['password'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOnosController>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
