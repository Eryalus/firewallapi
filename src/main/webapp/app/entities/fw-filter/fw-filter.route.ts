import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFwFilter, FwFilter } from 'app/shared/model/fw-filter.model';
import { FwFilterService } from './fw-filter.service';
import { FwFilterComponent } from './fw-filter.component';
import { FwFilterDetailComponent } from './fw-filter-detail.component';
import { FwFilterUpdateComponent } from './fw-filter-update.component';

@Injectable({ providedIn: 'root' })
export class FwFilterResolve implements Resolve<IFwFilter> {
  constructor(private service: FwFilterService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFwFilter> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fwFilter: HttpResponse<FwFilter>) => {
          if (fwFilter.body) {
            return of(fwFilter.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FwFilter());
  }
}

export const fwFilterRoute: Routes = [
  {
    path: '',
    component: FwFilterComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwFilter.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FwFilterDetailComponent,
    resolve: {
      fwFilter: FwFilterResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwFilter.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FwFilterUpdateComponent,
    resolve: {
      fwFilter: FwFilterResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwFilter.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FwFilterUpdateComponent,
    resolve: {
      fwFilter: FwFilterResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwFilter.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
