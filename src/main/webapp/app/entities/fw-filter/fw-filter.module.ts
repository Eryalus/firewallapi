import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { FwFilterComponent } from './fw-filter.component';
import { FwFilterDetailComponent } from './fw-filter-detail.component';
import { FwFilterUpdateComponent } from './fw-filter-update.component';
import { FwFilterDeleteDialogComponent } from './fw-filter-delete-dialog.component';
import { fwFilterRoute } from './fw-filter.route';

@NgModule({
  imports: [FirewallapiSharedModule, RouterModule.forChild(fwFilterRoute)],
  declarations: [FwFilterComponent, FwFilterDetailComponent, FwFilterUpdateComponent, FwFilterDeleteDialogComponent],
  entryComponents: [FwFilterDeleteDialogComponent],
})
export class FirewallapiFwFilterModule {}
