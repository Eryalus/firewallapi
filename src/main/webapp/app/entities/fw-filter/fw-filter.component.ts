import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFwFilter } from 'app/shared/model/fw-filter.model';
import { FwFilterService } from './fw-filter.service';
import { FwFilterDeleteDialogComponent } from './fw-filter-delete-dialog.component';

@Component({
  selector: 'jhi-fw-filter',
  templateUrl: './fw-filter.component.html',
})
export class FwFilterComponent implements OnInit, OnDestroy {
  fwFilters?: IFwFilter[];
  eventSubscriber?: Subscription;

  constructor(protected fwFilterService: FwFilterService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.fwFilterService.query().subscribe((res: HttpResponse<IFwFilter[]>) => (this.fwFilters = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFwFilters();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFwFilter): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFwFilters(): void {
    this.eventSubscriber = this.eventManager.subscribe('fwFilterListModification', () => this.loadAll());
  }

  delete(fwFilter: IFwFilter): void {
    const modalRef = this.modalService.open(FwFilterDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fwFilter = fwFilter;
  }
}
