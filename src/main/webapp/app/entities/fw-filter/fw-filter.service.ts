import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFwFilter } from 'app/shared/model/fw-filter.model';

type EntityResponseType = HttpResponse<IFwFilter>;
type EntityArrayResponseType = HttpResponse<IFwFilter[]>;

@Injectable({ providedIn: 'root' })
export class FwFilterService {
  public resourceUrl = SERVER_API_URL + 'api/fw-filters';

  constructor(protected http: HttpClient) {}

  create(fwFilter: IFwFilter): Observable<EntityResponseType> {
    return this.http.post<IFwFilter>(this.resourceUrl, fwFilter, { observe: 'response' });
  }

  update(fwFilter: IFwFilter): Observable<EntityResponseType> {
    return this.http.put<IFwFilter>(this.resourceUrl, fwFilter, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFwFilter>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFwFilter[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
