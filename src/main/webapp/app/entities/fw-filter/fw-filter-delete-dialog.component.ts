import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFwFilter } from 'app/shared/model/fw-filter.model';
import { FwFilterService } from './fw-filter.service';

@Component({
  templateUrl: './fw-filter-delete-dialog.component.html',
})
export class FwFilterDeleteDialogComponent {
  fwFilter?: IFwFilter;

  constructor(protected fwFilterService: FwFilterService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fwFilterService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fwFilterListModification');
      this.activeModal.close();
    });
  }
}
