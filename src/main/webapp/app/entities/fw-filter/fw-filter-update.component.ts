import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFwFilter, FwFilter } from 'app/shared/model/fw-filter.model';
import { FwFilterService } from './fw-filter.service';
import { IFwRule } from 'app/shared/model/fw-rule.model';
import { FwRuleService } from 'app/entities/fw-rule/fw-rule.service';

@Component({
  selector: 'jhi-fw-filter-update',
  templateUrl: './fw-filter-update.component.html',
})
export class FwFilterUpdateComponent implements OnInit {
  isSaving = false;
  fwrules: IFwRule[] = [];
  ruleID: number | null = null;
  rule: IFwRule | undefined = undefined;
  editForm = this.fb.group({
    id: [],
    humanName: [],
    protocol: [null, [Validators.required]],
    protocolValue: [],
    origin: [null, [Validators.required]],
    fwRule: [],
  });

  constructor(
    protected fwFilterService: FwFilterService,
    protected fwRuleService: FwRuleService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.ruleID = Number(params['rule']);
      this.activatedRoute.data.subscribe(({ fwFilter }) => {
        this.fwRuleService.query().subscribe((res: HttpResponse<IFwRule[]>) => {
          this.fwrules = res.body || [];
          this.fwrules.forEach(rule => {
            if (rule.id === this.ruleID) {
              this.rule = rule;
            }
          });
        });
        this.updateForm(fwFilter);
      });
    });
  }

  updateForm(fwFilter: IFwFilter): void {
    this.editForm.patchValue({
      id: fwFilter.id,
      humanName: fwFilter.humanName,
      protocol: fwFilter.protocol,
      protocolValue: fwFilter.protocolValue,
      origin: fwFilter.origin,
      fwRule: fwFilter.fwRule,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fwFilter = this.createFromForm();
    if (fwFilter.id !== undefined) {
      this.subscribeToSaveResponse(this.fwFilterService.update(fwFilter));
    } else {
      this.subscribeToSaveResponse(this.fwFilterService.create(fwFilter));
    }
  }

  private createFromForm(): IFwFilter {
    return {
      ...new FwFilter(),
      id: this.editForm.get(['id'])!.value,
      humanName: this.editForm.get(['humanName'])!.value,
      protocol: this.editForm.get(['protocol'])!.value,
      protocolValue: this.editForm.get(['protocolValue'])!.value,
      origin: this.editForm.get(['origin'])!.value,
      fwRule: this.editForm.get(['fwRule'])!.value ?? this.rule,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFwFilter>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IFwRule): any {
    return item.id;
  }
}
