import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFwFilter } from 'app/shared/model/fw-filter.model';

@Component({
  selector: 'jhi-fw-filter-detail',
  templateUrl: './fw-filter-detail.component.html',
})
export class FwFilterDetailComponent implements OnInit {
  fwFilter: IFwFilter | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fwFilter }) => (this.fwFilter = fwFilter));
  }

  previousState(): void {
    window.history.back();
  }
}
