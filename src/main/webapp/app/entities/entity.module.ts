import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'ov-switch',
        loadChildren: () => import('./ov-switch/ov-switch.module').then(m => m.FirewallapiOvSwitchModule),
      },
      {
        path: 'ov-s-port',
        loadChildren: () => import('./ov-s-port/ov-s-port.module').then(m => m.FirewallapiOvSPortModule),
      },
      {
        path: 'fw-network',
        loadChildren: () => import('./fw-network/fw-network.module').then(m => m.FirewallapiFwNetworkModule),
      },
      {
        path: 'fw-subnet',
        loadChildren: () => import('./fw-subnet/fw-subnet.module').then(m => m.FirewallapiFwSubnetModule),
      },
      {
        path: 'fw-rule',
        loadChildren: () => import('./fw-rule/fw-rule.module').then(m => m.FirewallapiFwRuleModule),
      },
      {
        path: 'fw-filter',
        loadChildren: () => import('./fw-filter/fw-filter.module').then(m => m.FirewallapiFwFilterModule),
      },
      {
        path: 'onos-controller',
        loadChildren: () => import('./onos-controller/onos-controller.module').then(m => m.FirewallapiOnosControllerModule),
      },
      {
        path: 'fw-rule-group',
        loadChildren: () => import('./fw-rule-group/fw-rule-group.module').then(m => m.FirewallapiFwRuleGroupModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class FirewallapiEntityModule {}
