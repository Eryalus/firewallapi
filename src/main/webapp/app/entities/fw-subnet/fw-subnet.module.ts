import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { FwSubnetComponent } from './fw-subnet.component';
import { FwSubnetDetailComponent } from './fw-subnet-detail.component';
import { FwSubnetUpdateComponent } from './fw-subnet-update.component';
import { FwSubnetDeleteDialogComponent } from './fw-subnet-delete-dialog.component';
import { fwSubnetRoute } from './fw-subnet.route';

@NgModule({
  imports: [FirewallapiSharedModule, RouterModule.forChild(fwSubnetRoute)],
  declarations: [FwSubnetComponent, FwSubnetDetailComponent, FwSubnetUpdateComponent, FwSubnetDeleteDialogComponent],
  entryComponents: [FwSubnetDeleteDialogComponent],
})
export class FirewallapiFwSubnetModule {}
