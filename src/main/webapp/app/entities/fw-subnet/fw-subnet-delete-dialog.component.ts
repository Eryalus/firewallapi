import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFwSubnet } from 'app/shared/model/fw-subnet.model';
import { FwSubnetService } from './fw-subnet.service';

@Component({
  templateUrl: './fw-subnet-delete-dialog.component.html',
})
export class FwSubnetDeleteDialogComponent {
  fwSubnet?: IFwSubnet;

  constructor(protected fwSubnetService: FwSubnetService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fwSubnetService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fwSubnetListModification');
      this.activeModal.close();
    });
  }
}
