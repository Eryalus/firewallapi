import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFwSubnet, FwSubnet } from 'app/shared/model/fw-subnet.model';
import { FwSubnetService } from './fw-subnet.service';
import { IFwNetwork } from 'app/shared/model/fw-network.model';
import { FwNetworkService } from 'app/entities/fw-network/fw-network.service';

@Component({
  selector: 'jhi-fw-subnet-update',
  templateUrl: './fw-subnet-update.component.html',
})
export class FwSubnetUpdateComponent implements OnInit {
  isSaving = false;
  fwnetworks: IFwNetwork[] = [];

  editForm = this.fb.group({
    id: [],
    ipPrefix: [null, [Validators.required]],
    ipMask: [null, [Validators.required]],
    allow: [null, [Validators.required]],
    priority: [null, [Validators.required]],
    fwNetwork: [],
  });

  constructor(
    protected fwSubnetService: FwSubnetService,
    protected fwNetworkService: FwNetworkService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fwSubnet }) => {
      this.updateForm(fwSubnet);

      this.fwNetworkService.query().subscribe((res: HttpResponse<IFwNetwork[]>) => (this.fwnetworks = res.body || []));
    });
  }

  updateForm(fwSubnet: IFwSubnet): void {
    this.editForm.patchValue({
      id: fwSubnet.id,
      ipPrefix: fwSubnet.ipPrefix,
      ipMask: fwSubnet.ipMask,
      allow: fwSubnet.allow,
      priority: fwSubnet.priority,
      fwNetwork: fwSubnet.fwNetwork,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fwSubnet = this.createFromForm();
    if (fwSubnet.id !== undefined) {
      this.subscribeToSaveResponse(this.fwSubnetService.update(fwSubnet));
    } else {
      this.subscribeToSaveResponse(this.fwSubnetService.create(fwSubnet));
    }
  }

  private createFromForm(): IFwSubnet {
    return {
      ...new FwSubnet(),
      id: this.editForm.get(['id'])!.value,
      ipPrefix: this.editForm.get(['ipPrefix'])!.value,
      ipMask: this.editForm.get(['ipMask'])!.value,
      allow: this.editForm.get(['allow'])!.value,
      priority: this.editForm.get(['priority'])!.value,
      fwNetwork: this.editForm.get(['fwNetwork'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFwSubnet>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IFwNetwork): any {
    return item.id;
  }
}
