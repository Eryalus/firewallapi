import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFwSubnet } from 'app/shared/model/fw-subnet.model';
import { FwSubnetService } from './fw-subnet.service';
import { FwSubnetDeleteDialogComponent } from './fw-subnet-delete-dialog.component';

@Component({
  selector: 'jhi-fw-subnet',
  templateUrl: './fw-subnet.component.html',
})
export class FwSubnetComponent implements OnInit, OnDestroy {
  fwSubnets?: IFwSubnet[];
  eventSubscriber?: Subscription;

  constructor(protected fwSubnetService: FwSubnetService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.fwSubnetService.query().subscribe((res: HttpResponse<IFwSubnet[]>) => (this.fwSubnets = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFwSubnets();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFwSubnet): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFwSubnets(): void {
    this.eventSubscriber = this.eventManager.subscribe('fwSubnetListModification', () => this.loadAll());
  }

  delete(fwSubnet: IFwSubnet): void {
    const modalRef = this.modalService.open(FwSubnetDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fwSubnet = fwSubnet;
  }
}
