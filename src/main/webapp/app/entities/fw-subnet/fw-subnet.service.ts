import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFwSubnet } from 'app/shared/model/fw-subnet.model';

type EntityResponseType = HttpResponse<IFwSubnet>;
type EntityArrayResponseType = HttpResponse<IFwSubnet[]>;

@Injectable({ providedIn: 'root' })
export class FwSubnetService {
  public resourceUrl = SERVER_API_URL + 'api/fw-subnets';

  constructor(protected http: HttpClient) {}

  create(fwSubnet: IFwSubnet): Observable<EntityResponseType> {
    return this.http.post<IFwSubnet>(this.resourceUrl, fwSubnet, { observe: 'response' });
  }

  update(fwSubnet: IFwSubnet): Observable<EntityResponseType> {
    return this.http.put<IFwSubnet>(this.resourceUrl, fwSubnet, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFwSubnet>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFwSubnet[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
