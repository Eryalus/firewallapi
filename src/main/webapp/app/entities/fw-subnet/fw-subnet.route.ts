import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFwSubnet, FwSubnet } from 'app/shared/model/fw-subnet.model';
import { FwSubnetService } from './fw-subnet.service';
import { FwSubnetComponent } from './fw-subnet.component';
import { FwSubnetDetailComponent } from './fw-subnet-detail.component';
import { FwSubnetUpdateComponent } from './fw-subnet-update.component';

@Injectable({ providedIn: 'root' })
export class FwSubnetResolve implements Resolve<IFwSubnet> {
  constructor(private service: FwSubnetService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFwSubnet> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fwSubnet: HttpResponse<FwSubnet>) => {
          if (fwSubnet.body) {
            return of(fwSubnet.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FwSubnet());
  }
}

export const fwSubnetRoute: Routes = [
  {
    path: '',
    component: FwSubnetComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwSubnet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FwSubnetDetailComponent,
    resolve: {
      fwSubnet: FwSubnetResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwSubnet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FwSubnetUpdateComponent,
    resolve: {
      fwSubnet: FwSubnetResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwSubnet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FwSubnetUpdateComponent,
    resolve: {
      fwSubnet: FwSubnetResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.fwSubnet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
