import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFwSubnet } from 'app/shared/model/fw-subnet.model';

@Component({
  selector: 'jhi-fw-subnet-detail',
  templateUrl: './fw-subnet-detail.component.html',
})
export class FwSubnetDetailComponent implements OnInit {
  fwSubnet: IFwSubnet | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fwSubnet }) => (this.fwSubnet = fwSubnet));
  }

  previousState(): void {
    window.history.back();
  }
}
