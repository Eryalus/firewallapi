import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOvSwitch } from 'app/shared/model/ov-switch.model';

type EntityResponseType = HttpResponse<IOvSwitch>;
type EntityArrayResponseType = HttpResponse<IOvSwitch[]>;

@Injectable({ providedIn: 'root' })
export class OvSwitchService {
  public resourceUrl = SERVER_API_URL + 'api/ov-switches';

  constructor(protected http: HttpClient) {}

  create(ovSwitch: IOvSwitch): Observable<EntityResponseType> {
    return this.http.post<IOvSwitch>(this.resourceUrl, ovSwitch, { observe: 'response' });
  }

  update(ovSwitch: IOvSwitch): Observable<EntityResponseType> {
    return this.http.put<IOvSwitch>(this.resourceUrl, ovSwitch, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOvSwitch>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOvSwitch[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
