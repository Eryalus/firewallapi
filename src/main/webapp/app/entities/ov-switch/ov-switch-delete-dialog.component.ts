import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOvSwitch } from 'app/shared/model/ov-switch.model';
import { OvSwitchService } from './ov-switch.service';

@Component({
  templateUrl: './ov-switch-delete-dialog.component.html',
})
export class OvSwitchDeleteDialogComponent {
  ovSwitch?: IOvSwitch;

  constructor(protected ovSwitchService: OvSwitchService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.ovSwitchService.delete(id).subscribe(() => {
      this.eventManager.broadcast('ovSwitchListModification');
      this.activeModal.close();
    });
  }
}
