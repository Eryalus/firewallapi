import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOvSwitch, OvSwitch } from 'app/shared/model/ov-switch.model';
import { OvSwitchService } from './ov-switch.service';

@Component({
  selector: 'jhi-ov-switch-update',
  templateUrl: './ov-switch-update.component.html',
})
export class OvSwitchUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    humanName: [],
    onosDeviceId: [],
  });

  constructor(protected ovSwitchService: OvSwitchService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ovSwitch }) => {
      this.updateForm(ovSwitch);
    });
  }

  updateForm(ovSwitch: IOvSwitch): void {
    this.editForm.patchValue({
      id: ovSwitch.id,
      humanName: ovSwitch.humanName,
      onosDeviceId: ovSwitch.onosDeviceId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ovSwitch = this.createFromForm();
    if (ovSwitch.id !== undefined) {
      this.subscribeToSaveResponse(this.ovSwitchService.update(ovSwitch));
    } else {
      this.subscribeToSaveResponse(this.ovSwitchService.create(ovSwitch));
    }
  }

  private createFromForm(): IOvSwitch {
    return {
      ...new OvSwitch(),
      id: this.editForm.get(['id'])!.value,
      humanName: this.editForm.get(['humanName'])!.value,
      onosDeviceId: this.editForm.get(['onosDeviceId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOvSwitch>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
