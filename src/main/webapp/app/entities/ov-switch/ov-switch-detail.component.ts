import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOvSwitch } from 'app/shared/model/ov-switch.model';

@Component({
  selector: 'jhi-ov-switch-detail',
  templateUrl: './ov-switch-detail.component.html',
})
export class OvSwitchDetailComponent implements OnInit {
  ovSwitch: IOvSwitch | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ovSwitch }) => (this.ovSwitch = ovSwitch));
  }

  previousState(): void {
    window.history.back();
  }
}
