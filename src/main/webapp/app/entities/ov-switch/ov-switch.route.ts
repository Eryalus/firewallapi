import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOvSwitch, OvSwitch } from 'app/shared/model/ov-switch.model';
import { OvSwitchService } from './ov-switch.service';
import { OvSwitchComponent } from './ov-switch.component';
import { OvSwitchDetailComponent } from './ov-switch-detail.component';
import { OvSwitchUpdateComponent } from './ov-switch-update.component';

@Injectable({ providedIn: 'root' })
export class OvSwitchResolve implements Resolve<IOvSwitch> {
  constructor(private service: OvSwitchService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOvSwitch> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((ovSwitch: HttpResponse<OvSwitch>) => {
          if (ovSwitch.body) {
            return of(ovSwitch.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OvSwitch());
  }
}

export const ovSwitchRoute: Routes = [
  {
    path: '',
    component: OvSwitchComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.ovSwitch.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OvSwitchDetailComponent,
    resolve: {
      ovSwitch: OvSwitchResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.ovSwitch.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OvSwitchUpdateComponent,
    resolve: {
      ovSwitch: OvSwitchResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.ovSwitch.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OvSwitchUpdateComponent,
    resolve: {
      ovSwitch: OvSwitchResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'firewallapiApp.ovSwitch.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
