import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { OvSwitchComponent } from './ov-switch.component';
import { OvSwitchDetailComponent } from './ov-switch-detail.component';
import { OvSwitchUpdateComponent } from './ov-switch-update.component';
import { OvSwitchDeleteDialogComponent } from './ov-switch-delete-dialog.component';
import { ovSwitchRoute } from './ov-switch.route';

@NgModule({
  imports: [FirewallapiSharedModule, RouterModule.forChild(ovSwitchRoute)],
  declarations: [OvSwitchComponent, OvSwitchDetailComponent, OvSwitchUpdateComponent, OvSwitchDeleteDialogComponent],
  entryComponents: [OvSwitchDeleteDialogComponent],
})
export class FirewallapiOvSwitchModule {}
