import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOvSwitch } from 'app/shared/model/ov-switch.model';
import { OvSwitchService } from './ov-switch.service';
import { OvSwitchDeleteDialogComponent } from './ov-switch-delete-dialog.component';

@Component({
  selector: 'jhi-ov-switch',
  templateUrl: './ov-switch.component.html',
})
export class OvSwitchComponent implements OnInit, OnDestroy {
  ovSwitches?: IOvSwitch[];
  eventSubscriber?: Subscription;

  constructor(protected ovSwitchService: OvSwitchService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.ovSwitchService.query().subscribe((res: HttpResponse<IOvSwitch[]>) => (this.ovSwitches = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInOvSwitches();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOvSwitch): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOvSwitches(): void {
    this.eventSubscriber = this.eventManager.subscribe('ovSwitchListModification', () => this.loadAll());
  }

  delete(ovSwitch: IOvSwitch): void {
    const modalRef = this.modalService.open(OvSwitchDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.ovSwitch = ovSwitch;
  }
}
