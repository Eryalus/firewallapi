import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { FirewallapiSharedModule } from 'app/shared/shared.module';
import { FirewallapiCoreModule } from 'app/core/core.module';
import { FirewallapiAppRoutingModule } from './app-routing.module';
import { FirewallapiHomeModule } from './home/home.module';
import { FirewallapiEntityModule } from './entities/entity.module';
import { FirewallapiTopologyModule } from './topology/topology.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    FirewallapiSharedModule,
    FirewallapiCoreModule,
    FirewallapiHomeModule,
    FirewallapiTopologyModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    FirewallapiEntityModule,
    FirewallapiAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class FirewallapiAppModule {}
